Step by step guide to create a solid App Engine project

# Creating the Project #

A good starting point for an appengine project is the [appengine boilerplate](https://github.com/metachris/appengine-boilerplate).
This builds on the great [html5 boilerplate](https://github.com/h5bp/html5-boilerplate) includes configuration defaults, build scripts and some other neat stuff.

Clone the boilerplate and copy it to the project folder.
    git clone https://github.com/metachris/appengine-boilerplate.git
    mkdir myproject
    cp -r <appengine-boilerplate directory>/* myproject/

I will use other components for the build chain so I can just delete the build tool included in the boilerplate.
    rm -r app/static_dev/build
    rm -r upload_to_appengine.sh

Lets also delete some of the extra stuff??
On the `myproject/app/templates/base.html` we will replace a couple of lines:
    <link rel="stylesheet" href="/css/style.css">
    <script src="/js/libs/modernizr-2.0.6.min.js"></script>

Lets create a repository for the project, it is a bit early but it will let you configure as you go allong.
My choice is git, so in the `myproject` directory do:
    git init .


# Grunt #

Time to install the build tool, for this project we will use grunt, for this you will need `nodejs` installed.
Lets create a `package.json` file to easily install the components we need.

`package.json`
    {
    "name": "app",
    "version": "0.1.0",
    "devDependencies": {
        "grunt": "~0.4.0",
        "grunt-cli": "~0.1.6",
        "grunt-contrib-watch": "~0.2.0",
        "grunt-contrib-jshint": "~0.2.0",
        "grunt-contrib-nodeunit": "~0.1.2",
        "grunt-contrib-compass": "~0.1.3",
        "grunt-dojo": "~0.2.0"
        }
    }


TODO
`npm install`

There are some dependencies here for some tools we will talk about later.
On the project root lets create an empty Gruntfile.
Gruntfile.js
    module.exports = function(grunt) {

        // Project configuration.
        grunt.initConfig({
            pkg: grunt.file.readJSON('package.json'),
        });

    };


# Dojo #
I personally like using the [Dojo framework](http://dojotoolkit.org/) it comes with libraries for most of your needs and tools like the [Closure compiler](https://developers.google.com/closure/compiler/).
Again we will start from a boilerplate get the [Dojo boilerplate](https://github.com/csnover/dojo-boilerplate).
Download the boilerplate and replace the contents of `myproject/app/static_dev/js`.
At this point we will delete the Dojo libraries and replace them with git submodules.
    rm -rf app/static_dev/js/src/dojo
    rm -rf app/static_dev/js/src/dijit
    rm -rf app/static_dev/js/src/dojox
    rm -rf app/static_dev/js/src/util
    git submodule add https://github.com/dojo/dojo.git app/static_dev/src/dojo
    git submodule add https://github.com/dojo/dijit.git app/static_dev/src/dijit
    git submodule add https://github.com/dojo/dojox.git app/static_dev/src/dojox
    git submodule add https://github.com/dojo/util.git app/static_dev/src/util

On the `myproject/app/templates/base.html` we will add couple of lines:
    <link type="text/css" rel="stylesheet" href="/js/app/resources/app.css" type="text/css">
    <script data-dojo-config="async: 1, {% if conf.DEBUG %}isDebug: true, {% endif %} deps:['app/run']" src="/js/dojo/dojo.js"></script>

Since we will use Grunt to build ao project we won't need the build script included in the boilerplate.
    rm app/static_dev/js/build.sh
    rm app/static_dev/js/index.html
In the build profile lets add the the release dir, add the following line to the `myproject/app/static_dev/js/profiles/app.profile.js` file.
    releaseDir: '../dir'

Lets add dojo build to our Gruntfile:
    module.exports = function(grunt) {

        // Project configuration.
        grunt.initConfig({
            pkg: grunt.file.readJSON('package.json'),

            dojo: {
                dist: {
                    options: {
                        dojo: 'src/static_dev/js/src/dojo/dojo.js',
                        load: 'build',
                        profile: 'src/static_dev/js/profiles/app.profile.js' // Profile for build
                    }
                }
            }
        });

        grunt.registerTask('build', ['dojo:dist']);
    };

Now you can run `grunt build` to build Dojo.

We will now have to add our build directory `myproject/app/static_dev/js/dist/` to out `.gitignore` file.
    src/static_dev/js/dist
If you don't have the next two lines you should also add them.
    *.pyc
    *.swp



# Compass #
TODO links
To ease our css pains we will use Compass wich includes Blueprint, both are great.
    compass create app/static_dev/
Now delete config.rb and stylesheets we don't need them.

Finally we add compass to Grunt.
grunt.initConfig
    compass: {
      dist: {
        options: {
          sassDir: 'src/static_dev/sass/src',
          cssDir: 'src/static_dev/sass/dist',
          environment: 'production'
        }
      },
      dev: {
        options: {
          sassDir: 'src/static_dev/sass/src',
          cssDir: 'src/static_dev/sass/dev'
        }
      }
    },




# App Engine configuration #

I keep two sets of configuration files in `myproject/config`, `app_dev.yaml` and `app_dist.yaml`.
They have diferent expiration
    mkdir config
    mv app/app.yaml config/
TODO app.yaml
add to gitignore



# Setting up Grunt #

When developing localy I like to have the option of working from source or from a build, I also like having warnings as soon as I save changes on a file.
So I'm going to add a bunch of stuff to the Gruntfile.

    module.exports = function(grunt) {

        grunt.initConfig({
            pkg: grunt.file.readJSON('package.json'),

            watch: {
                files: ['src/static_dev/sass/**/*.scss',
                        'Gruntfile.js',
                        'src/static_dev/js/src/app/**/*.js',
                        'src/static_dev/js/src/profiles/**/*.js'],
                tasks: ['default']
            },

            jshint: {
                all: ['Gruntfile.js',
                    'src/static_dev/js/src/app/**/*.js',
                    'src/static_dev/js/src/profiles/**/*.js']
            },

            compass: {
                dist: {
                    options: {
                        sassDir: 'src/static_dev/sass/src',
                        cssDir: 'src/static_dev/sass/dist',
                        environment: 'production'
                    }
                },
                dev: {
                    options: {
                        sassDir: 'src/static_dev/sass/src',
                        cssDir: 'src/static_dev/sass/dev'
                    }
                }
            },

            dojo: {
                dist: {
                    options: {
                        dojo: 'src/static_dev/js/src/dojo/dojo.js',
                        load: 'build',
                        profile: 'src/static_dev/js/profiles/app.profile.js' // Profile for build
                    }
                }
            }
        });

        grunt.registerTask('copyConf', 'Copy configs.', function(env) {
            grunt.file.copy('config/app_' + env  + '.yaml', 'src/app.yaml');
            grunt.file.copy('config/conf_' + env  + '.py', 'src/conf.py');
        });

        grunt.registerTask('linkDist', function() {
            var done = this.async();
            grunt.util.spawn({
                cmd: 'ln',
                args: ['-sf', 'src/static_dev/sass/dist', 'src/static/css'],
                opts: {'stdio': 'inherit'}
            }, function() {
                grunt.util.spawn({
                cmd: 'ln',
                args: ['-sf', 'src/static_dev/js/dist', 'src/static/js'],
                opts: {'stdio': 'inherit'}
                }, function () {
                done();
                });
            });
        });

        grunt.registerTask('linkDev', 'Clean the whole build directory.', function() {
            grunt.file.mkdir('src/static/js');


            var done = this.async();
            grunt.util.spawn({
                cmd: 'ln',
                args: ['-sf', 'src/static_dev/sass/dev', 'src/static/css'],
                opts: {'stdio': 'inherit'}
            }, function () {
                grunt.util.spawn({
                cmd: 'ln',
                args: ['-sf', 'src/static_dev/js/src/dojo', 'src/static/js/dojo'],
                opts: {'stdio': 'inherit'}
                }, function() {
                grunt.util.spawn({
                    cmd: 'ln',
                    args: ['-sf', 'src/static_dev/js/src/dijit', 'src/static/js/dijit'],
                    opts: {'stdio': 'inherit'}
                }, function() {
                    grunt.util.spawn({
                    cmd: 'ln',
                    args: ['-sf', 'src/static_dev/js/src/dojo', 'src/static/js/dojo'],
                    opts: {'stdio': 'inherit'}
                    }, function() {
                    grunt.util.spawn({
                        cmd: 'ln',
                        args: ['-sf', 'src/static_dev/js/src/app', 'src/static/js/app'],
                        opts: {'stdio': 'inherit'}
                    }, function() {
                        grunt.util.spawn({
                        cmd: 'ln',
                        args: ['-sf', 'src/static_dev/js/src/jquery.masonry.min.js', 'src/static/js/'],
                        opts: {'stdio': 'inherit'}
                        }, function() {
                        done();
                        });
                    });
                    });
                });
                });
            });

        });

        grunt.registerTask('cleanLinks', function() {
            var done = this.async();
            grunt.util.spawn({
                cmd: 'rm',
                args: ['-rdf', 'src/static/js', 'src/static/css'],
                opts: {'stdio': 'inherit'}
            },
            function() {
                done();
            });
        });

        grunt.registerTask('cleanDev', 'Clean the whole build directory.', function() {
            var done = this.async();
            grunt.util.spawn({
                cmd: 'rm',
                args: ['-rdf', 'src/static_dev/sass/dev'],
                opts: {'stdio': 'inherit'}
            },
            function() {
                done();
            });
        });

        grunt.registerTask('cleanDist', 'Clean the whole build directory.', function() {
            var done = this.async();
            grunt.util.spawn({
                cmd: 'rm',
                args: ['-rdf', 'src/static_dev/js/dist', 'src/static_dev/sass/dist'],
                opts: {'stdio': 'inherit'}
            },
            function() {
                done();
            });
        });

        grunt.registerTask('cleanBuild', 'Clean the whole build directory.', function() {
            var done = this.async();
            grunt.util.spawn({
                cmd: 'find',
                args: ['src/static_dev/js/dist',
                '-o', '-name', '"\\*.uncompressed.js"',
                '-o', '-name', '"\\*.consoleStripped.js"',
                '-o', '-name', '"\\*.less"',
                '-o', '-name', '"\\*README"',
                '-o', '-name', '"\\*LICENCE"',
                '-type', 'f',
                '-delete'],
                opts: {'stdio': 'inherit'}
            },
            function() {
                done();
            });
        });

        grunt.registerTask('appengineUpdate', 'Upload to App Engine.', function() {
            grunt.log.subhead('Uploading to App Engine');

            var done = this.async();
            grunt.util.spawn({
                cmd: 'appcfg.py',
                args: ['update', 'src/'],
                opts: {'stdio': 'inherit'}
            },
            function() {
                grunt.log.success('Done');
                done();
            });
        });

        grunt.registerTask('run', 'Run app server.', function() {
            grunt.log.subhead('Starting App Server');

            var done = this.async();
            grunt.util.spawn({
                cmd: 'dev_appserver.py',
                args: ['update', 'src/'],
                opts: {'stdio': 'inherit'}
            },
            function() {
                grunt.log.writeln('Done');
                done();
            });

        });

        //grunt.loadNpmTasks('grunt-contrib-uglify');
        grunt.loadNpmTasks('grunt-contrib-watch');
        grunt.loadNpmTasks('grunt-contrib-compass');
        grunt.loadNpmTasks('grunt-contrib-jshint');
        grunt.loadNpmTasks('grunt-dojo');

        grunt.registerTask('setDev', ['cleanLinks', 'linkDev']);
        grunt.registerTask('setDist', ['cleanLinks', 'linkDev']);

        grunt.registerTask('watchDev', ['jshint', 'compass:dev']);
        grunt.registerTask('buildDist', ['compass:dist', 'dojo:dist']);

        grunt.registerTask('runLocal', [
            'cleanDev',
            'jshint',
            'compass:dev',
            'setDev',
            'copyConf:dev',
            'run']);
        grunt.registerTask('runLocalDist', [
            'cleanDist',
            'compass:dist',
            'dojo:dist',
            'setDist',
            'copyConf:dev',
            'run']);
        grunt.registerTask('updateDist', [
            'cleanDist',
            'compass:dist',
            'dojo:dist',
            'copyConf:dist',
            'setDist',
            'appengineUpdate',
            'copyConf:local']);
    };

Now this is a lot of stuff all of a sudden, lets do a quick overview of the tasks.
* run -- Run App Engine locally
* appengineUpdate -- Update App Engine
* cleanDist/cleanDev -- Delete dist/dev build directories (just making sure we have a clean build)
* setDist/setDev -- Make links in static that point to the correct environment
* copyConf -- Copy correct configuration files to App Engine source
* watchDev -- TODO
* buildDist -- TODO


When developing I split my terminal using tmux and on the botton I run watchDev
TODO screen grab.


We have one more step remaining 

# Packaging #

If you tryed to intall the same toolchain I did you discovered that basically each tool is writen in a diferent programing language.
Compass uses ruby and Grunt uses node.js and for the server side we are using python.
This can quickly become an unmanageable mess of diferent version numbers and a pain for a new colaborator to start developing.
It's time to start thinking abou how we will package our project and creating an environment for it to run.

I'm of the opinion that it should be painless for a new contributor to start developing.
Assuming that he some base tools installed on the system he/she should be able to have a working come in as few steps as necessary.
For me the ideal would be three:
* Checkout a copy of the project
* Run a script that builds the environment and installs dependencies
* Copy a file with the required keys and passwords to run the development environment

These are the tools I will assume are allready installed in the system:
* `virtualenv` and `virtualenvwrapper` //TODO links
* [nvm](https://rvm.io/)
* node.js
node env TODO??

For the tool chain I described here are the files/scripts I use.

mkevn.sh
startenv.sh
stopenv.sh

package.json
Gemfile
requirements.txt

# Setup #

# Version Controll #

Delete dojo, dijit, dojox, util
git init
git add submodule
git add
git commit
