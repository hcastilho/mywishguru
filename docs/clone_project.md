

git clone <repo>
cd <repo>
mkvirtualenv -p /usr/bin/python2 -r requirements.txt <name>
deactivate
workon <name>
nodeenv -v -p -r node-requirements.txt
deactivate
workon <name>
npm install

# Install local grunt required because of reasons...
npm install grunt



# add npm
npm install -g <name>

# npm install; installs package.json
