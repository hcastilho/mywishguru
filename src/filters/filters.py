import json
#import logging
#from django.core.serializers import serialize
#from django.utils import simplejson
from google.appengine.ext import webapp
#from django.template.defaultfilters import register
#from django.template import Library
#register = Library()

register = webapp.template.create_template_register()

@register.filter(name='jsonify')
def jsonify(object):
    obj=json.dumps(object)#[1:-1] #.replace('"',"'")
    return obj

@register.filter(name='jsonify_dojo_props')
def jsonify_dojo_props(object):
    obj=json.dumps(object)[1:-1]#.replace('"',"'")
    return obj
