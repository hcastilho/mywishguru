require([
  'dojo/dom',
  'dojo/ready',
  'mwg/widget/SlideShow',
  'mwg/widget/TakeTour'
  ],
function(
  dom,
  ready,
  SlideShow,
  TakeTour
  ) {

  ready(function() {
    new SlideShow().placeAt(dom.byId('slideshow'));
    new TakeTour().placeAt(dom.byId('takeatour'));
  });
});
