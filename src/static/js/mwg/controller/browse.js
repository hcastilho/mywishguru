require([
  'dojo/_base/Deferred',
  'dojo/_base/event',
  'dojo/_base/lang',
  'dojo/cookie',
  'dojo/io/iframe',
  'dojo/on',
  'dojo/dom',
  'dojo/dom-construct',
  'dojo/dom-class',
  'dojo/query',
  'dojo/io-query',
  'dojo/string',
  'dijit/registry',
  'dijit/Dialog',
  'dijit/form/Select',
  'mwg/main',
  'mwg/InfiniteScroll',
  'mwg/controller/boards',
  'mwg/widget/WishModel',
  'mwg/widget/WishCard',
  'mwg/widget/FilterDialog',
  'mwg/widget/PagesDialog',
  'dojo/ready'],
function(
  Deferred,
  event,
  lang,
  cookie,
  iframe,
  on,
  dom,
  domConstruct,
  domClass,
  query,
  ioQuery,
  string,
  registry,
  Dialog,
  Select,
  main,
  InfiniteScroll,
  boards,
  WishModel,
  WishCard,
  FilterDialog,
  PagesDialog,
  ready
  ) {
    var user = null;

    var queryArgs = {},
    setQueryArgs = function() {
      if (user !== null) {
        queryArgs = {user: user};
        return;
      }

      var node = registry.byId('filter');
      if (node.value == 'friends') {
        queryArgs = {type: 'friends'};
      }
      else if (node.value == 'everyone' || node.value == 'dummy') {
        if (window.mwg.account) {
          queryArgs = {type: 'public'};
        }
        else {
          queryArgs = {type: 'everyone'};
        }
      }
      else if (node.value == 'favorites') {
        queryArgs = {type: 'favorites'};
      }
      else if (node.value == 'public') {
        queryArgs = {type: 'public'};
      }
      else if (node.value == 'pages') {
        queryArgs = {type: 'pages'};
      }

      node.set('value', 'dummy');
    },

    wishQuery = function() {
      main.wishStore.query(queryArgs, {count: mwg.config.pageSize}).then(function (response) {
        boards.place(response);
      });
    },

    setShowFilter = function() {
      var showFilter = dom.byId('showFilter');
      if ('type' in queryArgs) {
        if (queryArgs.type == 'friends') {
          showFilter.innerHTML = 'My Friends';
        }
        else if (queryArgs.type == 'everyone') {
          showFilter.innerHTML = 'Everyone';
        }
        else if (queryArgs.type == 'favorites') {
          showFilter.innerHTML = 'Favorites';
        }
        else if (queryArgs.type == 'public') {
          showFilter.innerHTML = 'Public';
        }
        else if (queryArgs.type == 'pages') {
          showFilter.innerHTML = 'Pages';
        }
      }
      else if ('user' in queryArgs) {
        if ((window.mwg.account &&
              (window.mwg.account.id == queryArgs.user ||
               window.mwg.account.username == queryArgs.user)) ||
            (!window.mwg.account &&
              (window.mwg.config.userId == queryArgs.user ||
              window.mwg.config.username == queryArgs.user))) {
          showFilter.innerHTML = 'My Wishboard';
        }
        else {
          main.userStore.get(queryArgs.user).then(function(response) {
            showFilter.innerHTML = [
              '<a title="Go to Facebook wall"',
              ' href="http://www.facebook.com/' + response.user_id + '"',
              ' target="_blank">',
              '<img class="mask" src="/images/fb_mask_v1.png"/>',
              '<img src="' + response.picture + '"/>',
              '</a>',
              '<span>' + response.name + "'s Wishboard",
              '</span>'].join('');
            //if ('wishboard_cover' in response && response.wishboard_cover) {
            //  var htmlfrag= [
            //    '<div id="wishboardCover" class="masonrySeparator wishboardCover">',
            //    '<img id="coverImage" src="' + response.wishboard_cover + '">',
            //    '</img>',
            //    '</div>'
            //  ].join('');
            //  var node = domConstruct.place(htmlfrag, dom.byId('masonryContainer'), 'first');
            //  boards.append(node);
            //}
          });
        }
      }
    },

    init = function() {
      var browse_user;
      var pathname = window.location.pathname;
      if (pathname.indexOf('/wishboard') !== -1) {
        if (window.mwg.account) {
          if (window.mwg.account.username)
            user = window.mwg.account.username;
          else
            user = window.mwg.account.userId;
        }
        else {
          browse_user = window.mwg.config.userId;
          if (window.mwg.config.username)
            user = window.mwg.config.username;
          else
            user = window.mwg.config.userId;
        }
      }
      else if (pathname.indexOf('/browse') !== -1) {
        pathname = pathname.substring('/browse/'.length, pathname.length);
        if (pathname.length > 1) {
          user = pathname;
          browse_user = window.mwg.config.userId;
        }
      }
      else if (pathname.length > 2) {
        // /<username>
        user = pathname.slice(1, pathname.length);
      }

      if (('username' in window.mwg.active_user & window.mwg.active_user.username == user) ||
          ('id' in window.mwg.active_user & window.mwg.active_user.id == user)) {
        var node;
        node = dom.byId('browse_button');
        if (node) domClass.remove(node, 'mwgButtonActive');
        node = dom.byId('my_wishboard_button');
        if (node) domClass.add(node, 'mwgButtonActive');
      }

      var options;
      if (window.mwg.account) {
        options = [
          {label: 'Select new filter', value: 'dummy', selected: true},
          {label: 'Favorites', value: 'favorites'},
          {label: 'Public', value: 'public'},
          {label: 'Pages', value: 'pages'}
        ];
      }
      else if (!window.mwg.config.userId) {
        options = [
          {label: 'Select new filter', value: 'dummy', selected: true},
          {label: 'Public', value: 'public'},
          {label: 'Pages', value: 'pages'}
        ];
      }
      else {
        options = [
          {label: 'Select new filter', value: 'dummy', selected: true},
          {label: 'My Friends', value: 'friends'},
          {label: 'Everyone', value: 'everyone'},
          {label: 'Favorites', value: 'favorites'},
          {label: 'Public', value: 'public'},
          {label: 'Pages', value: 'pages'}
        ];
      }
      var select = new Select({
        name: 'filter',
        options: options
      }, 'filter');

      boards.start();
      //queryArgs = {
      //  type: 'everyone'
      //};
      setQueryArgs();
      setShowFilter();
      // TODO find better solution to keep account cover on the first space
      //setTimeout(wishQuery, 400);
      wishQuery();
      InfiniteScroll.init(main.wishStore, queryArgs, boards.place);

      on(select, 'change', function() {
        var node = registry.byId('filter');
        if (node.value == 'dummy') {
          return;
        }
        user = null;
        boards.clear();
        setQueryArgs();
        setShowFilter();
        InfiniteScroll.restart(queryArgs);
        wishQuery();
      });

      if (window.mwg.account) {
        domConstruct.destroy('searchFriend');
      }
      else {
        if (dom.byId('searchFriend')) {
          on(dom.byId('searchFriend'), 'click', function() {
            if (window.mwg.filterDialog === undefined) {
              window.mwg.filterDialog = new FilterDialog();
            }
            window.mwg.filterDialog.show();
          });
        }
      }
      on(dom.byId('searchPage'), 'click', function() {
        if (window.mwg.pagesDialog === undefined) {
          window.mwg.pagesDialog = new PagesDialog();
        }
        window.mwg.pagesDialog.show();
      });

      if (window.mwg.account) {
        if (dom.byId('wishboardCover')) {
          on(dom.byId('coverImage'), 'click', lang.hitch(this, function() {
            dom.byId('uploadCoverInput').click();
          }));

          on(dom.byId('uploadCoverInput'), 'change', lang.hitch(this, function() {
            //domClass.add(this.imguplNode, 'dragdrop_upload');

            iframeArgs = {
              url: '/fofimages/',
              method: 'post',
              form: dom.byId('coverUploadForm'),
              handleAs: 'json'};

            Deferred.when(
              iframe.send(iframeArgs),
              function(response) {
                if ('error' in response) {
                  alert(response.error);
                }
                else {
                  var args = {
                    _csrf_token: cookie('c'),
                    user_id: window.mwg.account.id,
                    wishboard_cover: response.url
                  };
                  main.userStore.put(args).then(function() {
                    dom.byId('coverImage').src = response.url;
                    boards.reload();
                  });
                }
              },
              function() {
                console.warn('05f117aa-649b-4169-91d8-a9bc6ada1357');
                console.warn(arguments);
              });
          }));
        }
      }
    };

    ready(function() {
      if (window.mwg.config.userId && !window.mwg.account) {
        main.friendStore.query().then(lang.hitch(this, function(response) {
          window.mwg.friends = response;
        }));
      }
      main.accountStore.query().then(lang.hitch(this, function(response) {
        window.mwg.accounts = response;
      }));
      init();
    });

});

