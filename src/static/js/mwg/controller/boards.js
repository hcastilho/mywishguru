define([
  'dojo/_base/Deferred',
  'dojo/dom',
  'dojo/dom-construct',
  'dojo/on',
  'dojo/_base/fx',
  'mwg/widget/WishModel',
  'mwg/widget/WishCard',
  'mwg/InfiniteScroll',
  'dojo/ready'
  //'/js/masonry.pkgd.min.js',
  //'/js/imagesloaded.pkgd.min.js'
], function(
  Deferred,
  dom,
  domConstruct,
  on,
  fx,
  WishModel,
  WishCard,
  InfiniteScroll,
  ready
  //Masonry,
  //imagesloaded
  ) {


    var clear = function() {
      //hide();
      var node = dom.byId('masonryContainer');
      domConstruct.empty('masonryContainer');
      //window.masonryContainer.reload();
      var $masonryContainer = $('#masonryContainer');
      $masonryContainer.masonry('reload');
    },

    start = function() {
      //window.masonryContainer = new Masonry(dom.byId('masonryContainer'),
      //  {
      //    itemSelector : '.masonrySeparator',
      //    columnWidth : 293,
      //    gutterWidth : 12
      //    //isAnimated: true
      //  });
      var masonryContainer = $('#masonryContainer');
        masonryContainer.masonry({
          itemSelector : '.masonrySeparator',
          columnWidth : 293,
          gutterWidth : 12
          //isAnimated: true
        });
    },

    place = function(wishes) {
      var model, card, cards = [];
      var $card;
      var $masonryContainer = $('#masonryContainer');
      for (var i=0; i<wishes.length; i++) {
        model = new WishModel(wishes[i]);
        card = new WishCard({model: model});
        card.placeAt(dom.byId('masonryContainer'));
        cards.push(card.domNode);
      }
      //imagesLoaded(cards, function(){
      //  window.masonryContainer.appended(cards);
      //  for (var i=0; i<cards.length; i++) {
      //    fx.fadeIn({node: cards[i]}).play();
      //  }
      //  InfiniteScroll.destroyOverlay();
      //});
      var $cards = $(cards);
      $cards.imagesLoaded(function(){
        //$masonryContainer.masonry('appended', $cards, true);
        $masonryContainer.masonry('appended', $cards);
        for (var i=0; i<cards.length; i++) {
          fx.fadeIn({node: cards[i]}).play();
        }
        InfiniteScroll.destroyOverlay();
      });
    },

    append = function(node) {
      var $masonryContainer = $('#masonryContainer');
      var $node = $(node);
      $node.imagesLoaded(function(){
        $masonryContainer.masonry('appended', $(node));
      });
      //masonryContainer.appended(node);
    },

    //prepend = function(node) {
    //  var $masonryContainer = $('#masonryContainer');
    //  $masonryContainer.masonry('prepended', $(node));
    //  //masonryContainer.prepended(node);
    //},

    reload = function() {
      var $masonryContainer = $('#masonryContainer');
      $masonryContainer.masonry('reload');
      //masonryContainer.reload(node);
    },

    imagesLoaded = function(node) {
      var def = new Deferred();
      var $node = $(node);
      $node.imagesLoaded(function(){
        def.resolve();
      });
      return def;
    };

    return {
      clear: clear,
      start: start,
      place: place,
      append: append,
      //prepend: prepend,
      reload: reload
    };
});


