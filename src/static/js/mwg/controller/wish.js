define([
  'dojo/ready',
  'dojo/_base/fx',
  'dojo/on',
  'dojo/dom',
  'dojo/dom-class',
  'dojo/topic',
  'dijit/registry',
  'dijit/layout/StackContainer',
  'mwg/main',
  'mwg/controller/boards',
  'mwg/widget/WishModel',
  'mwg/widget/StepAddTab',
  'mwg/widget/StepModel',
  'mwg/widget/StepView',
  'mwg/widget/StepViewTab',
  'mwg/widget/StepController',
  'dojo/domReady!'
  ],
function(
  ready,
  fx,
  on,
  dom,
  domClass,
  topic,
  registry,
  StackContainer,
  main,
  boards,
  WishModel,
  StepAddTab,
  StepModel,
  StepView,
  StepViewTab,
  StepController) {

    var init = function(wish, steps, selectedStep) {
      var i;
      wish = new WishModel(wish);
      main.getWishStats(wish);
      for (i=0; i<steps.length; i++) {
        steps[i] = new StepModel(steps[i]);
      }

      stepController = new StepController({
        stackContainerNode: 'stackContainer',
        wish: wish,
        steps: steps,
        selectedStep: selectedStep
      }, 'stepController');

      fx.fadeIn({node: 'overlayNode'}).play();
    };

    return {
      init: init
    };
});
