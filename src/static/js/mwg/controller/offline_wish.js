require([
  'dojo/dom',
  'dojo/ready',
  'mwg/widget/SlideShow'
  ],
function(
  dom,
  ready,
  SlideShow
  ) {

  ready(function() {
    new SlideShow().placeAt(dom.byId('slideshow'));
  });
});
