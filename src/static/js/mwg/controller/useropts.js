require([
    'dojo/_base/lang',
    'dojo/cookie',
    'dojo/query',
    'dojo/on',
    'dojo/dom',
    'dojo/dom-construct',
    'dojo/dom-class',
    'dojo/NodeList-dom',
    'dojo/_base/Deferred',
    'dojo/_base/event',
    'dojo/io-query',
    'mwg/main',
    'mwg/widget/NotificationDialog',
    'mwg/widget/TwoButtonDialog',
    'mwg/widget/OneButtonDialog',
    'dijit/registry',
    'dijit/Dialog',
    'dijit/form/CheckBox',
    'dijit/form/Button',
    'dojo/ready'],
    function(
      lang,
      cookie,
      query,
      on,
      dom,
      domConstruct,
      domClass,
      NodeListDom,
      Deferred,
      event,
      ioQuery,
      main,
      NotificationDialog,
      TwoButtonDialog,
      OneButtonDialog,
      registry,
      Dialog,
      CheckBox,
      Button,
      ready
      ) {

  ready(function () {

    // User Dropdown
    if (dom.byId('opts_menu')) {
      // Click out from opts
      on(window, 'click', function(evt) {
        //var tar = evt.target;
        //if (!(domClass.contains(evt.target, 'opts'))) {
          domClass.add(dom.byId('opts_dropdown'), 'hidden');
        //}
      });

      on(dom.byId('opts_menu'), 'click', function(evt) {
        var node = dom.byId('opts_dropdown');
        if (domClass.contains(node, 'hidden')) {
          domClass.remove(node, 'hidden');
        }
        else {
          domClass.add(node, 'hidden');
        }
        event.stop(evt);
      });
      on(dom.byId('invite'), 'click', function(){
        FB.ui({method: 'apprequests',
          message: 'Join me on MyWishGuru',
          filters: ['app_non_users','app_users','all']
        });
      });
      on(dom.byId('logout'), 'click', function(){
        FB.logout(function(response) {
          main.goHome();
        });
      });


      var userMenu = function() {
        var asFbNode = domConstruct.create('li', {
            innerHTML: 'Wish as FB page'
          },
          dom.byId('notifications'),
          'after');
        on(asFbNode, 'click', function() {
          main.addPermission('manage_pages').then(function(authorized) {
            if (!authorized) return;
            fields = ['id', 'access_token', 'name', 'picture', 'username'].join(',');
            FB.api('/me/accounts?fields=' + fields, 'get', function(response) {
              if (response.data.length === 0) {
                var confDialog = new OneButtonDialog( {
                    title: 'No pages',
                    message: 'You do not have any pages.'
                });
                confDialog.show();
              }
              for(var i=0; i < response.data.length; i++) {
                domConstruct.destroy(asFbNode);
                var node = domConstruct.create('li', {
                    'class': 'accountEntry',
                    innerHTML: ['<img src="' + response.data[i].picture.data.url + '"/>',
                      '<span class="account_name">' + response.data[i].name + '</span>',
                      '<span class="delete_account"></span>'].join('')
                  },
                  dom.byId('opts_dropdown'),
                  'first'
                );
                node.id = response.data[i].id;
                node.access_token = response.data[i].access_token;
                node.name = response.data[i].name;
                node.picture = response.data[i].picture.data.url;
                node.username = response.data[i].username;

                on(node, 'click', changeToAccount);
                domClass.remove(dom.byId('opts_dropdown'), 'hidden');

                query('.delete_account', node).on('click', delete_account);
              }
            });
          });
        });
      };
      var delete_account = function(evt) {
        event.stop(evt);
        var node = evt.target;
        if (!('id' in evt.target) || (evt.target.id === '')) {
          node = evt.target.parentNode;
        }
        var id = node.id;
        var confDialog = new TwoButtonDialog( {
            title: 'Delete Page',
            message: 'Are you sure?',
            confirmMsg: 'Delete',
            cancelMsg: 'Cancel'
        });
        confDialog.show().then(function(response) {
          if (response) {
            main.accountStore.remove(id).then(function() {
              changeToUser();
            });
          }
        });
      };
      var accountMenu = function() {
        var backToUserNode = domConstruct.create('li', {
            'class': 'backToUser',
            innerHTML: 'Go back to user'
          },
          dom.byId('notifications'),
          'after');
        on(backToUserNode, 'click', changeToUser);
      };
      var changeToUser = function() {
          main.removeAccount();
          window.location.reload();
      };
      var changeToAccount = function(evt) {
        var node = evt.target;
        if (!('id' in evt.target) || (evt.target.id === '')) {
          node = evt.target.parentNode;
        }
        main.setAccount(node);
        window.location.reload();
      };

      if (window.mwg.account) {
        accountMenu();
      }
      else {
        userMenu();
      }


      var openNotifications = function() {
        if (window.mwg.notificationDialog === undefined) {
          var user_id = window.mwg.config.userId;
          if (window.mwg.account) user_id = window.mwg.account.id;
          main.userStore.get(user_id).then(lang.hitch(this, function(response) {
            window.mwg.notificationDialog = new NotificationDialog(response);
            window.mwg.notificationDialog.show();
          }));
        }
        else {
          window.mwg.notificationDialog.show();
        }
      };

      on(dom.byId('notifications'), 'click', function() {
        openNotifications();
      });

      var search = window.location.search;
      search = search.substring(1, search.length);
      var queryObj = ioQuery.queryToObject(search);
      if ('notifications' in queryObj) {
        openNotifications();
      }
    }

  });
});
