define([
  'mwg/widget/WishModel',
  'mwg/widget/WishEdit',
  'mwg/widget/StepModel',
  'mwg/widget/StepEdit',
  'mwg/widget/StepEditTab',
  'mwg/widget/StepAddTab',
  'mwg/widget/StepController',
  'mwg/widget/TwoButtonDialog',
  'mwg/controller/boards',
  'dijit/registry',
  'dojo/_base/event',
  'dojo/_base/lang',
  'dojo/_base/unload',
  'dojo/_base/fx',
  'dojo/io-query',
  'dojo/on',
  'dojo/dom',
  'dojo/ready',
  'dojo/domReady!'
], function(
  WishModel,
  WishEdit,
  StepModel,
  StepEdit,
  StepEditTab,
  StepAddTab,
  StepController,
  TwoButtonDialog,
  boards,
  registry,
  event,
  lang,
  baseUnload,
  fx,
  ioQuery,
  on,
  dom,
  ready) {

  var init = function(wish, steps) {
    var i;
    wish = new WishModel(wish);
    for (i=0; i<steps.length; i++) {
      steps[i] = new StepModel(steps[i]);
    }

    // Load Steps
    stepController = new StepController({
      stackContainerNode: 'stackContainer',
      edit: true,
      wish: wish,
      steps: steps
    }, 'stepController');


    // Add Step Button
    var tab = new StepAddTab({stepController: stepController});
    stepController.addTab(tab);

    if (wish.wish_id === '') {
      tab = new StepAddTab(
          {stepController: stepController,
           type: 2
          });
      stepController.addTab(tab);
      tab = new StepAddTab(
          {stepController: stepController,
           type: 3
          });
      stepController.addTab(tab);
    }

    stepController.selectChild(wish);

    fx.fadeIn({node: 'overlayNode'}).play();

    baseUnload.addOnUnload(function(evt) {
      var models = stepController.getModels();
      var confMessage = 'You have unsaved changes.';
      for (var i=0; i<models.length; i++) {
        if (models[i].isChanged()) {
          (evt || window.event).returnValue = confMessage;
          return confMessage;
        }
      }
    });


    // New Step
    var uri = window.location.search;
    var query = uri.substring(uri.indexOf("?") + 1, uri.length);
    var queryObject = ioQuery.queryToObject(query);
    if ('newstep' in queryObject) {
      stepController.addStep();
    }
  };

  return {
    init: init
  };
});
