define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/_base/Deferred',
  'dojo/_base/event',
  'dojo/_base/sniff',
  'dojo/query',
  'dojo/on',
  'dojo/topic',
  'dojo/has',
  'dojo/io/iframe',
  'dojo/dom',
  'dojo/dom-construct',
  'dojo/dom-class',
  'dojo/dom-style',
  'dojo/mouse',
  'mwg/main',
  'dijit/_WidgetBase',
  'dijit/_TemplatedMixin',
  'dijit/_WidgetsInTemplateMixin',
  'dijit/form/Select'
], function(
  declare,
  lang,
  Deferred,
  event,
  sniff,
  query,
  on,
  topic,
  has,
  iframe,
  dom,
  domConstruct,
  domClass,
  domStyle,
  mouse,
  main,
  _WidgetBase,
  _TemplatedMixin,
  _WidgetsInTemplateMixin,
  Select) {

  return declare('mwg.widget.BaseEdit',
      [_WidgetBase,
      _TemplatedMixin,
      _WidgetsInTemplateMixin], {

    baseClass: 'baseEdit',
    title: '',
    desc: '',
    logo: '',
    status: '',


    // DRAG
    dragging: false,
    lastPos: null,
    imageDisplay: null,
    tlPerc: null,

    _onMouseDown: function(e) {
      this.dragging = true;
      this.lastPos = {
        x: e.pageX,
        y: e.pageY
      };
    },

    _onMouseUp: function(e) {
      this.dragging = false;
      // Change cursor
      //dojo.style(document.body, {
      //  cursor: 'default'
      //});
      //dojo.style(this.cropNode, {
      //  cursor: 'move'
      //});
      //this.onDone(this.getInfo());
    },

    _onMouseMove: function(e) {
      if (!this.dragging) return;
      //this.doMove(e);
      var currL = domStyle.get(this.imageDisplay, 'left');
      var currT = domStyle.get(this.imageDisplay, 'top');

      var l = currL + e.pageX - this.lastPos.x;
      var t = currT + e.pageY - this.lastPos.y;

      this.lastPos = {
        x: e.pageX,
        y: e.pageY
      };

      var maxL = this.imguplNode.offsetWidth - this.imageDisplay.offsetWidth;
      var maxT = this.imguplNode.offsetHeight - this.imageDisplay.offsetHeight;

      if (l > 0) l = 0;
      if (t > 0) t = 0;
      if (l < maxL) l = maxL;
      if (t < maxT) t = maxT;

      this.model.set('logoTop', t/this.imageDisplay.offsetHeight);
      this.model.set('logoLeft', l/this.imageDisplay.offsetWidth);

      domStyle.set(this.imageDisplay, 'left', l+'px');
      domStyle.set(this.imageDisplay, 'top', t+'px');

    },

    setTopLeft: function() {
      if (this.imageDisplay === null) {
        return;
      }
      if (this.imageDisplay.offsetHeight === 0 ||
          this.imageDisplay.offsetWidth === 0) {
        return;
      }
      else {
        this.ranSetTopLeft= true;
      }

      var value = this.logoTop;
      value = value * this.imageDisplay.offsetHeight;
      domStyle.set(this.imageDisplay, 'top', value+'px');

      value = this.logoLeft;
      value = value * this.imageDisplay.offsetWidth;
      domStyle.set(this.imageDisplay, 'left', value+'px');
    },

    setFixedLength: function() {
      if (this.imageDisplay === null) {
        return;
      }
      if (this.imageDisplay.offsetHeight === 0 ||
          this.imageDisplay.offsetWidth === 0) {
        return;
      }
      else {
        this.ranSetFixedLength = true;
      }

      domStyle.set(this.imageDisplay, 'height', '');
      domStyle.set(this.imageDisplay, 'width', '');
      var aspectRatio = (this.imageDisplay.offsetWidth / this.imageDisplay.offsetHeight);
      if (aspectRatio > 1.4987) {
        domStyle.set(this.imageDisplay, 'height', '100%');
      }
      else {
        domStyle.set(this.imageDisplay, 'width', '100%');
      }
    },

    ranSetTopLeft: false,
    ranSetFixedLength: false,
    updateDisplay: function() {
      // Every time a new image is selected
      if (!this.ranSetFixedLength) {
        this.setFixedLength();
        this.ranSetFixedLength = true;
      }

      // Once to initialize existing image
      // (Editing existing wish)
      if (!this.ranSetTopLeft) {
        this.setTopLeft();
        this.ranSetTopLeft = true;
      }

    },

    _setLogoAttr: function(value) {
      //var errors = this.model._checkLogoAttr(value);
      //this._dispLogoError(errors);
      //if (errors.length === 0) {
        this._set('logo', value);
        this.model.set('logo', value);
      //}

      this.ranSetFixedLength = false;
      if (value !== '') {
        if (this.imageDisplay === null) {
          domClass.remove(this.imguplNode, 'dragdrop_border');
          domConstruct.create('span', {'class': 'dragrepos'}, this.imguplNode);
          this.imageDisplay = domConstruct.create('img', {src: value}, this.imguplNode);
          // Drag to repos
          on(this.imageDisplay, 'load', lang.hitch(this, function() {
            this.setFixedLength();
          }));
          on.once(this.imageDisplay, 'load', lang.hitch(this, function() {
            this.setTopLeft();
          }));
          on(this.imageDisplay, 'mousedown', lang.hitch(this, '_onMouseDown'));
          on(this.imageDisplay, 'mouseup', lang.hitch(this, '_onMouseUp'));
          on(this.imageDisplay, mouse.leave, lang.hitch(this, '_onMouseUp'));
          on(this.imageDisplay, 'mousemove', lang.hitch(this, '_onMouseMove'));
          on(this.imageDisplay, 'dragstart', function(evt) {
            event.stop(evt);
          });
        }
        else {
          domStyle.set(this.imageDisplay, 'top', '');
          domStyle.set(this.imageDisplay, 'left', '');
          this.imageDisplay.src = value;
        }

      }
    },

    // Title cannot be empty but is on wish creation
    // validation check cannot be made here
    _setTitleAttr: function(value) {
      this._set('title', value);
      this.model.set('title', this.title);
      this.titleEdit.value = value;
    },

    _setDescAttr: function(value) {
      this._set('desc', value);
      this.model.set('desc', value);
      this.descEdit.value = value;
      //var errors = this.model._checkDescAttr();
      //this._dispDescError(errors);
      //if (errors.length === 0) {
      //  this._set('desc', value);
      //  this.model.set('desc', value);
      //  this.descEdit.value = value;
      //}
    },

    _setStatusAttr: function(value) {
      this._set('status', value);
      this.model.set('status', this.status);
      if ((value !== 'editing' && value !== '') && this.saveButton) {
        domClass.add(this.saveButton, 'hidden');
      }
    },

    _dispTitleError: function(errors) {
      if (errors.length === 0) {
        this.clearError(this.titleError, this.titleEdit);
      }
      else {
        this.createError(errors[0].msg, this.titleError, this.titleEdit);
      }
    },

    _dispDescError: function(errors) {
      if (errors.length === 0) {
        this.clearError(this.descError, this.descEdit);
      }
      else {
        this.createError(errors[0].msg, this.descError, this.descEdit);
      }
    },

    _dispLogoError: function(errors) {
      if (errors.length === 0) {
        this.clearError(this.logoError, this.imguplNode);
      }
      else {
        this.createError(errors[0].msg, this.logoError, this.imguplNode);
      }
    },

    createError: function(errorMsg, errorNode, inputNode) {
      domConstruct.empty(errorNode);
      var html = ['<span class="error_box">',
        '<span class="left"></span>',
        '<span class="cnt">' + errorMsg + '</span>',
        '<span class="right"></span>',
        '<div class="clear"></div>',
        '</span>'].join('');
      domConstruct.place(html, errorNode);
      domClass.add(inputNode, 'error');
    },

    clearError: function(errorNode, inputNode) {
        domConstruct.empty(errorNode);
        domClass.remove(inputNode, 'error');
    },

    check: function() {
      var error, errors = [];

      error = this.model._checkTitleAttr();
      this._dispTitleError(error);
      errors = errors.concat(error);

      error = this.model._checkDescAttr();
      this._dispDescError(error);
      errors = errors.concat(error);

      error = this.model._checkLogoAttr();
      this._dispLogoError(error);
      errors = errors.concat(error);

      return errors;
    },

    postCreate: function() {
      this.set('status', this.model.status);
      this.set('title', this.model.title);
      this.set('desc', this.model.desc);
      this.set('logo', this.model.logo);
      this.set('logoTop', this.model.logoTop);
      this.set('logoLeft', this.model.logoLeft);


      if (has('ie')) {
        domClass.add(this.uploadButton, 'hidden');
        domClass.remove(this.imgNode, 'outOfScreen');
      }

      // Connects
      this.model.watch('status', lang.hitch(this, function() {
        this.set('status', this.model.status);
      }));

      //if (this.hasFeatures()) this.addDnd();
      //else {
      //  if (this.canHideBrowse()) {
      //    this.dndtextNode.innerHTML = 'Click to upload';
      //  }
      //  else {
      //    domClass.remove(this.imgNode, 'hidden');
      //    domClass.add(this.imguplNode, 'hidden');
      //  }
      //}

      if (true) {
        on(this.uploadButton, 'click', lang.hitch(this, function() {
          this.imgNode.click();
        }));
      }
      else {
        domClass.remove(this.imgNode, 'hidden');
        domClass.add(this.uploadButton, 'hidden');
      }
      this.addImageUpload();

      on(this.titleEdit, 'change', lang.hitch(this, function() {
        var value = this.titleEdit.value;
        var errors = this.model._checkTitleAttr(value);
        this._dispTitleError(errors);
        if (errors.length === 0) {
          this.set('title', value);
          //this.titleEdit.value = value;
        }
      }));
      on(this.descEdit, 'change',  lang.hitch(this, function() {
        var value = this.descEdit.value;
        var errors = this.model._checkDescAttr(value);
        this._dispDescError(errors);
        if (errors.length === 0) {
          this.set('desc', value);
          //this.descEdit.value = value;
        }
      }));

    },

    publishModel: function() {
      this.model.set('status', 'open');
      return this.model.putModel().then(lang.hitch(this, function(response) {
        topic.publish(this.id + '-PublishModel', response);
        return response;
      }));
    },

    saveModel: function() {
      this.model.set('status', 'editing');
      return this.model.putModel().then(lang.hitch(this, function(response) {
        topic.publish(this.id + '-SaveModel', response);
      }));
    },

    cancelModel: function() {
      topic.publish(this.id + '-CancelModel');
    },

    //hasFeatures: function() {
    //  if (has.add(
    //        'dnd',
    //        function(global, document, anElement) {
    //          return 'draggable' in document.createElement('span');
    //        },
    //        true) &&
    //      has.add(
    //      'file-api',
    //      function(global, document, anElement) {
    //          return typeof FileReader != 'undefined';
    //        },
    //      true) &&
    //    has.add(
    //      'native-xhr-uploadevents',
    //      function(global, document, anElement) {
    //          return has('native-xhr') && ('upload' in new XMLHttpRequest());
    //        },
    //      true)
    //  ) {
    //    return true;
    //  }
    //  return false;
    //},

    //canHideBrowse: function() {
    //  //var android = navigator.userAgent.indexOf('Android') > -1,
    //  // TODO test this
    //  var android = has('android'),
    //  ios = has('agent-ios'),
    //  ie = has('ie');

    //  if (ie || ios || android) return false;
    //  else return true;
    //},

    addImageUpload: function() {
      on(this.imgNode, 'change', lang.hitch(this, function() {
        domClass.add(this.imguplNode, 'dragdrop_upload');

        iframeArgs = {
          url: '/fofimages/',
          method: 'post',
          form: this.imageUploadForm,
          handleAs: 'json'};

        Deferred.when(
          iframe.send(iframeArgs),
          lang.hitch(this, function(response) {
            if ('error' in response) {
              alert(response.error);
            }
            else {
              //this.set('logo', response.url);
              var errors = this.model._checkLogoAttr(response.url);
              this._dispLogoError(errors);
              if (errors.length === 0) {
                this.set('logo', response.url);
              }
            }
            domClass.remove(this.imguplNode, 'dragdrop_upload');

          }),
          function() {
            console.warn('c5155ceb-1db9-49b0-bf8e-fc92c0164578');
            console.warn(arguments);
          });
      }));
    }

    //addDnd: function() {
    //  on(window, 'dragover', function(evt) {
    //    evt.preventDefault();   // necessary for dnd to work
    //  });
    //  on(window, 'drop', function(evt) {
    //    evt.preventDefault();
    //  });

    //  on(this.imguplNode, 'dragenter', function() {
    //    domClass.add(this, 'dragdrop_file_hover');
    //  });
    //  on(this.imguplNode, 'dragleave', function() {
    //    domClass.remove(this, 'dragdrop_file_hover');
    //  });
    //  on(this.imguplNode, mouse.leave, function() {
    //    domClass.remove(this, 'dragdrop_file_hover');
    //  });
    //  on(this.imguplNode, 'drop', lang.hitch(this, function(evt) {

    //    if (evt.dataTransfer.files.length === 0) {
    //      var code = evt.dataTransfer.getData('text/html');
    //      if (code === '') {
    //        return;
    //      }
    //      domClass.remove(this.imguplNode, 'dragdrop_file_hover');
    //      return;
    //    }
    //    domClass.remove(this.imguplNode, 'dragdrop_file_hover');
    //    domClass.add(this.imguplNode, 'dragdrop_upload');

    //    var file = evt.dataTransfer.files[0];
    //    var req = new XMLHttpRequest();
    //    on(req, 'readystatechange', lang.hitch(this, function() {
    //      if (req.readyState == 4) {
    //        // upload finished successful
    //        domClass.remove(this, 'dragdrop_upload');
    //        if (req.status == 200 || req.status == 201) {
    //          response = JSON.parse(req.responseText);
    //          domClass.remove(this.imguplNode, 'dragdrop_upload');

    //        }
    //      }
    //    }));
    //    req.open('post', '/fofimages/' , true);
    //    req.setRequestHeader('Cache-Control', 'no-cache');
    //    req.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    //    req.setRequestHeader('X-File-Name', file.name);
    //    req.setRequestHeader('X-File-Size', file.size);
    //    req.send(file);
    //  }));
    //}
  });

});


