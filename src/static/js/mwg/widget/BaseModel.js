define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/_base/Deferred',
  'dojo/DeferredList',
  'dojo/topic',
  'dojo/store/JsonRest',
  'dojo/cookie',
  'dojox/validate/web',
  'dijit/_WidgetBase',
  'mwg/main'
], function(
  declare,
  lang,
  Deferred,
  DeferredList,
  topic,
  Store,
  cookie,
  validateWeb,
  _WidgetBase,
  main) {

  return declare('', [_WidgetBase], {

    _csrf_token: cookie('c'),

    url: '',
    title: '',
    desc: '',
    content: '',
    logo: '',
    logoTop: 0,
    logoLeft: 0,
    small_logo: '',
    status: '',
    user: null,
    stats: null,

    changed: null,
    owner: false,
    userType: 'member', // 'visitor'

    store: null,

    _setOwnerAttr: function(value) {
      this._set('owner', value);
    },
    _setTitleAttr: function(value) {
      if (this.title == value) return;
      this.changed.title = '';
      this._set('title', value);
    },
    _setDescAttr: function(value) {
      if (this.desc == value) return;
      this.changed.desc = '';
      this._set('desc', value);
    },
    _setContentAttr: function(value) {
      if (this.content == value) return;
      this.changed.content = '';
      this._set('content', value);
    },
    _setLogoAttr: function(value) {
      if (this.logo == value) return;
      this.changed.logo = '';
      this._set('logo', value);
    },
    _setLogoTopAttr: function(value) {
      if (this.logoTop == value) return;
      this.changed.logoTop = '';
      this._set('logoTop', value);
    },
    _setLogoLeftAttr: function(value) {
      if (this.logoLeft == value) return;
      this.changed.logoLeft = '';
      this._set('logoLeft', value);
    },

    // TODO set maxlength for title (on html too)
    _checkTitleAttr: function(value) {
      var error, errors = [];
      if (value === undefined) {
        value = this.title;
      }
      if (value.length > 500) {
        error = {msg: "The title can't be longer than 500 characters."};
        errors.push(error);
      }
      else if (value.length === 0){
        error = {msg: "The title is empty."};
        errors.push(error);
      }
      return errors;
    },

    _checkDescAttr: function(value) {
      var error, errors = [];
      if (value === undefined) {
        value = this.desc;
      }
      var max_length = 2000;
      if (value.length > max_length) {
        error = {msg: "The description can't be longer than " + max_length + " characters."};
        errors.push(error);
      }
      return errors;
    },

    _checkLogoAttr: function(value) {
      var error, errors = [];
      if (value === undefined) {
        value = this.logo;
      }
      if (value === '') {
        error = {msg: 'Missing picture.'};
        errors.push(error);
      }
      if (value !== '' && !validateWeb.isUrl(value, {allowLocal: true})) {
        error = {msg: 'Invalid URL.'};
        errors.push(error);
      }
      return errors;
    },

    _setStatusAttr: function(value) {
      if (this.status == value) return;
      this.changed.status = '';
      this._set('status', value);
    },

    check: function() {
      var errors = [];
      errors = errors.concat(this._checkTitleAttr());
      errors = errors.concat(this._checkDescAttr());
      errors = errors.concat(this._checkLogoAttr());
      return errors;
    },

    isChanged: function() {
      return Object.keys(this.changed).length !== 0;
    },

    constructor: function(args) {
      this.user = {
        user_id: '',
        fb_user_id: '',
        name: '',
        picture: ''
      };
      this.changed = {};

      this.stats = {
        likes: 0,
        comments: 0,
        shares: 0,
        posts: 0
      };
      this.total_stats = {};
      this.posts = [];
      this.admins = [];
    },

    postMixInProperties: function() {
      if (!('user_id' in this.user) || ('user_id' in this.user && !this.user.user_id)) {
        this.owner = true;
        // TODO check this
        this.user.user_id = window.mwg.config.userId;
        this.user.name = window.mwg.config.userName;
        this.user.first_name = window.mwg.config.userFirstName;
        this.user.picture = window.mwg.config.userPicture;
      }
      if (!('first_name' in this.user) || !this.user.first_name) {
        this.user.first_name = this.user.name;
      }
    },

    postCreate: function() {
      if (window.mwg.account) {
        if (window.mwg.account.id == this.user.user_id) {
          this.set('owner', true);
        }
      }
      else {
        if (window.mwg.config.userId == this.user.user_id) {
          this.set('owner', true);
        }
        if (!window.mwg.config.userId) {
          this.set('userType', 'visitor');
        }
      }
    },

    deleteModel: null,
    putModel: null,

    getModel: function(model_id) {
      return this.store.get(model_id).then(
        lang.hitch(this, function(response) {
          if ('error' in response) {
            console.warn('0be95137-3cb6-4c34-b36a-d4a33dc73547');
            console.log(response);
          }
          else {
            if (response.created !== undefined) {
              response.created = stamp.fromISOString(response.created);
            }
            if (response.updated !== undefined) {
              response.updated = stamp.fromISOString(response.updated);
            }
            lang.mixin(this, response);
          }
          return response;
        }),
        function(args) {
          console.warn('6ef25caa-af45-4181-b1f1-589479830a30');
          console.warn(args);
        });
    },

    getPosts: function() {
      var deferred = new Deferred();
      var wish_id, step_id = 0;
      wish_id = this.wish_id;
      if ('step_id' in this) {
        step_id = this.step_id;
      }
      new DeferredList([main.getPosts(this.posts), main.getStats(this.url)]).then(
        function(response) {
          var pub;
          if ('error' in response[0][1]) {
            response[0][1].posts = [];
          }
          if ('error' in response[1][1]) {
            response[1][1].stats = {shares: 0,
              likes: 0,
              comments: 0};
          }
          var reply = {
            posts: {},
            stats: {}
          };
          reply.posts = response[0][1].posts;
          reply.stats = response[1][1].stats;

          if (step_id !== 0) {
            pub = '/wish_id/' + wish_id + '/step_id/' + step_id;
          }
          else {
            pub = '/wish_id/' + wish_id;
          }
          topic.publish('getPosts' + pub, reply);
          topic.publish('helpers_update');
          deferred.resolve(reply);
        },
        function() {
          deferred.reject(response);
        });

      return deferred;
    },

    addPost: function(post_id) {
      this.posts.push(post_id);

      var putArgs = {
        _csrf_token: this._csrf_token,
        post: post_id
      };
      if ('step_id' in this) {
        putArgs.step_id = this.step_id;
      }
      else {
        putArgs.wish_id = this.wish_id;
      }

      return this.store.put(putArgs).then(
          function(response) {
            if ('error' in response) {
              console.warn('e983fb5c-dda0-4584-ac15-d10ef39001fe');
              console.warn(response);
            }
            else {
              _gaq.push([
                '_trackEvent',
                'Wish',
                'Publish',
                response.wish_id.toString()]);
            }

            return response;
          });
    },

    publishModel: function() {
      //this.set('status', 'open');
      return this.putModel().then(lang.hitch(this, function(response) {
        topic.publish(this.id + '-PublishModel', response);
        return response;
      }));
    },

    // TODO o que fazer com isto
    saveModel: function() {
      //this.set('status', 'editing');
      return this.putModel().then(lang.hitch(this, function(response) {
        topic.publish(this.id + '-SaveModel', response);
      }));
    },

    cancelModel: function() {
      topic.publish(this.id + '-CancelModel');
    },

    toggleFulfill: function() {
      if (this.status == 'open') {
        this.set('status', 'fulfilled');
        this.putModel();
      }
      else if (this.status == 'fulfilled'){
        this.set('status', 'open');
        this.putModel();
      }
    }

  });
});


