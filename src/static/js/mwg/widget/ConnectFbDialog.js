define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/topic',
  'dojo/on',
  'dijit/Dialog',
  'dojo/text!./resources/connect_fb.html',
  'dijit/_WidgetBase',
  'dijit/_TemplatedMixin',
  'dijit/_WidgetsInTemplateMixin'
], function(
  declare,
  lang,
  topic,
  on,
  Dialog,
  connectFbTemplate,
  _WidgetBase,
  _TemplatedMixin,
  _WidgetsInTemplateMixin
  ) {

  var ConnectFbDialog = declare('mwg.widget.ConnectFbDialog', [
      _WidgetBase,
      _TemplatedMixin,
      _WidgetsInTemplateMixin
      ], {

    wish_id: '',

    baseClass: 'connectFbDialog',
    templateString: connectFbTemplate,

    //postMixInProperties: function(){
    //},

    postCreate: function() {
      this.dialogNode.show();
    },

    hide: function() {
      this.dialogNode.hide();
    }
  });

  return ConnectFbDialog;
});



