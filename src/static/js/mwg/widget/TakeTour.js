define([
    'dojo/_base/declare',
    'dojo/_base/lang',
    'dojo/_base/fx',
    'dojo/fx',
    'dojo/on',
    'dojo/query',
    'dojo/dom',
    'dojo/dom-class',
    'dojo/dom-attr',
    'dojo/dom-style',
    'dojo/dom-geometry',
    'dojo/text!./resources/TakeTour.html',
    'dijit/_WidgetBase',
    'dijit/_TemplatedMixin'
  ], function(
    declare,
    lang,
    fx,
    coreFx,
    on,
    query,
    dom,
    domClass,
    domAttr,
    domStyle,
    domGeom,
    template,
    _WidgetBase,
    _TemplatedMixin) {

  return declare('mwg.widget.TakeTour', [_WidgetBase, _TemplatedMixin], {
    baseClass: 'tour',
    templateString: template,

    slideShow: null,

    numSteps: 7,
    pageSize: 3,

    currentTopStep: 1,
    selectedStep: 1,

    stepColor: ['orange', 'blue', 'green', 'red', 'cyan', 'yellow', 'red'],

    show: function() {
      fx.fadeIn({node: this.domNode}).play();
    },

    setSlideShow: function() {
      if (this.slideShow) {
        clearInterval(this.slideShow);
      }
      this.slideShow = setInterval(lang.hitch(this, function() {
        var nextStep = this.selectedStep + 1;
        if (nextStep>this.numSteps) {
          nextStep = 1;
        }
        this.selectStep(nextStep);
      }), 7000);
    },

    postCreate: function() {
      this.selectCircle(1);
      this.fadeInStep(1);
      this.fadeInImage(1);
      coreFx.slideTo({
        node: this.sliderNode,
        top: '0',
        left: '0',
        unit: 'px'
      }).play();

      this.setSlideShow();

      query('.stepSelector', this.domNode).on('click', lang.hitch(this, function(evt) {
        // Get Step and top step
        var selectedStep = parseInt(domAttr.get(evt.target, 'data-step-number'), 10);
        this.selectStep(selectedStep);
        this.setSlideShow();
      }));

      on(this.step1, 'click', lang.hitch(this, function () {
        this.selectStep(1);
        this.setSlideShow();
      }));
      on(this.step2, 'click', lang.hitch(this, function () {
        this.selectStep(2);
        this.setSlideShow();
      }));
      on(this.step3, 'click', lang.hitch(this, function () {
        this.selectStep(3);
        this.setSlideShow();
      }));
      on(this.step4, 'click', lang.hitch(this, function () {
        this.selectStep(4);
        this.setSlideShow();
      }));
      on(this.step5, 'click', lang.hitch(this, function () {
        this.selectStep(5);
        this.setSlideShow();
      }));
      on(this.step6, 'click', lang.hitch(this, function () {
        this.selectStep(6);
        this.setSlideShow();
      }));
      on(this.step7, 'click', lang.hitch(this, function () {
        this.selectStep(7);
        this.setSlideShow();
      }));

      this.show();
    },

    selectStep: function(selectedStep) {
      if (selectedStep === this.selectedStep) return;
      else this.selectedStep = selectedStep;

      var stepTop;
      if (selectedStep === 1) {
        stepTop = 1;
      }
      else if (selectedStep === this.numSteps) {
        stepTop = this.numSteps + 1 - this.pageSize;
      }
      else {
        stepTop = selectedStep - 1;
      }


      this.selectCircle(selectedStep);
      this.fadeInStep(selectedStep);
      this.fadeInImage(selectedStep);
      if (stepTop !== this.currentTopStep) {
        this.currentTopStep = stepTop;
        this.moveSlider(stepTop);
      }

    },

    selectCircle: function(selectedStep) {
      query('.stepSelector', this.domNode).forEach(lang.hitch(this, function(node) {
        // Get Step and top step
        var stepNumber = parseInt(domAttr.get(node, 'data-step-number'), 10);
        if (stepNumber == selectedStep) {
          fx.animateProperty({
            node: node,
            properties: {
                borderColor: '#19222b'
            }
          }).play();
        }
        else {
          fx.animateProperty({
            node: node,
            properties: {
                borderColor: '#94a5b6'
            }
          }).play();
        }
      }));
    },

    moveSlider: function(stepTop) {
      // Hide Connectors
      var i, stepConnector;
      for (i=1; i<=this.numSteps; i++) {
        stepConnector = this['connector' + i];
        fx.fadeOut({node: stepConnector}).play();
      }

      // Move slider and Show Connectors
      var sliderTop = -(stepTop - 1) * (127 + 14);
      coreFx.slideTo({
        node: this.sliderNode,
        top: sliderTop.toString(),
        left: '0',
        unit: 'px',
        onEnd: lang.hitch(this, function() {
          for (i=0; i<3; i++) {
            stepConnector = this['connector' + (stepTop + i)];
            domClass.remove(stepConnector);
            domClass.add(stepConnector, 'connect');
            domClass.add(stepConnector, 'c_' + this.stepColor[stepTop + i - 1] + '_' + (3-i));
          }
          for (i=0; i<3; i++) {
            stepConnector = this['connector' + (stepTop + i)];
            fx.fadeIn({node: stepConnector}).play();
          }
        })
      }).play();
    },

    fadeInStep: function(selectedStep) {
      var step;
      for (i=1; i<=this.numSteps; i++) {
        step = this['step' + i];
        if (i == selectedStep) {
          fx.animateProperty({
            node: step,
            properties: {
                opacity: 1
            }
          }).play();
        }
        else {
          fx.animateProperty({
            node: step,
            properties: {
                opacity: 0.5
            }
          }).play();
        }
      }
    },

    fadeInImage: function(selectedStep) {
      var image;
      for (i=1; i<=this.numSteps; i++) {
        image = this['stepImage' + i];
        if (i == selectedStep) {
          fx.animateProperty({
            node: image,
            properties: {
                opacity: 1
            }
          }).play();
        }
        else {
          fx.animateProperty({
            node: image,
            properties: {
                opacity: 0
            }
          }).play();
        }
      }
    }

  });
});
