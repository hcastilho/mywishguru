define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/_base/Deferred',
  'dojo/_base/fx',
  'dojo/on',
  'dojo/topic',
  'dojo/dom',
  'dojo/dom-construct',
  'dojo/dom-class',
  'dojo/dom-style',
  'dojo/mouse',
  'mwg/main',
  'mwg/widget/WishModel',
  'dojo/text!./resources/WishCard.html',
  'dijit/_WidgetBase',
  'dijit/_TemplatedMixin'
], function(
  declare,
  lang,
  Deferred,
  fx,
  on,
  topic,
  dom,
  domConstruct,
  domClass,
  domStyle,
  mouse,
  main,
  WishModel,
  wishTemplate,
  _WidgetBase,
  _TemplatedMixin) {

  return declare('mwg.widget.WishCard',
      [_WidgetBase,
      _TemplatedMixin], {

    limitHeight: false,

    baseClass: 'wishCard masonrySeparator',
    templateString: wishTemplate,
    model: null,

    status: '',

    postMixInProperties: function() {
      if (this.model.logo === '') {
        this.model.logo = mwg.config.defaultImage;
      }
    },

    postCreate: function() {
      this.inherited(arguments);

      // Copy from model
      this.set('status', this.model.status);

      // Favorite
      if (window.mwg.config.userId && !this.model.owner) {
        domClass.remove(this.favstarNode, 'hidden');
        on(this.favstarNode, 'click', lang.hitch(this, function() {
          this.model.toggleFavorite();
        }));
        this.model.watch('favorite', lang.hitch(this, function() {
          if (this.model.favorite) {
            domClass.add(this.favstarNode, 'favorited');
          }
          else {
            domClass.remove(this.favstarNode, 'favorited');
          }
        }));
        if (this.model.favorite) {
          domClass.add(this.favstarNode, 'favorited');
        }
      }

      // Stats and posts
      main.getWishStats(this.model).then(lang.hitch(this, function(stats) {
        this.likesNode.innerHTML = stats.likes;
        this.commentsNode.innerHTML = stats.comments;
        this.sharesNode.innerHTML = stats.shares;
      })) ;

      if (this.model.status == 'fulfilled') {
        var fulnode = domConstruct.create('a', {href: this.model.url}, this.postuser);
        domConstruct.create('div', {
          'class': 'fulfilled',
          title: 'Wish Fulfilled'}, fulnode);
      }
      //
      if (this.limitHeight) {
        this.logoNode.src = this.model.logo;
        on(this.logoNode, 'load', lang.hitch(this, function() {
          domClass.add(this.logoWrapNode, 'limitHeight');
          domClass.add(this.logoNode, 'limitHeight');

          domStyle.set(this.logoNode, 'height', '');
          domStyle.set(this.logoNode, 'width', '');
          var aspectRatio = (this.logoNode.offsetWidth / this.logoNode.offsetHeight);
          if (aspectRatio > 1.4987) {
            domStyle.set(this.logoNode, 'height', '100%');
          }
          else {
            domStyle.set(this.logoNode, 'width', '100%');
          }

          var value = this.model.logoTop;
          value = value * this.logoNode.offsetHeight;
          domStyle.set(this.logoNode, 'top', value+'px');
          value = this.model.logoLeft;
          value = value * this.logoNode.offsetWidth;
          domStyle.set(this.logoNode, 'left', value+'px');
        }));
      }

    }

  });
});


