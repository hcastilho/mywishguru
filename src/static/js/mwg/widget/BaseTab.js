define([
    'dijit/_TemplatedMixin',
    'dijit/_WidgetBase',
    'dojo/_base/declare',
    'dojo/_base/lang',
    'dojo/dom-class',
    'dojo/dom-style',
    'dojo/mouse',
    'dojo/on',
    'dojo/topic'
  ], function(
    _TemplatedMixin,
    _WidgetBase,
    declare,
    lang,
    domClass,
    domStyle,
    mouse,
    on,
    topic) {

  return declare('mwg/widget/BaseTab', [_WidgetBase, _TemplatedMixin], {
    baseClass: 'baseTab',

    model: null,
    logo: '',
    logoTop: 0,
    logoLeft: 0,

    _setLogoAttr: function(value) {
      this._set('logo', value);
      domStyle.set(this.logoNode, 'top', '0');
      domStyle.set(this.logoNode, 'left', '0');
      this.logoNode.src = value;
    },
    _setLogoTopAttr: function(value) {
      this._set('logoTop', value);
      value = value * this.logoNode.offsetHeight;
      domStyle.set(this.logoNode, 'top', value+'px');
    },
    _setLogoLeftAttr: function(value) {
      this._set('logoLeft', value);
      value = value * this.logoNode.offsetWidth;
      domStyle.set(this.logoNode, 'left', value+'px');
    },

    postCreate: function() {
      // Logo
      this.set('logo', this.model.logo);
      this.set('logoTop', this.model.logoTop);
      this.set('logoLeft', this.model.logoLeft);
      if (this.logo === '') {
        this.set('logo', mwg.config.defaultThumb);
      }
      on(this.logoNode, 'load', lang.hitch(this, function() {
        domStyle.set(this.logoNode, 'height', '');
        domStyle.set(this.logoNode, 'width', '');
        var aspectRatio = (this.logoNode.offsetWidth / this.logoNode.offsetHeight);
        if (aspectRatio > 1.4987) {
          domStyle.set(this.logoNode, 'height', '100%');
        }
        else {
          domStyle.set(this.logoNode, 'width', '100%');
        }
      }));
      on.once(this.logoNode, 'load', lang.hitch(this, function() {
        var value = this.logoTop;
        value = value * this.logoNode.offsetHeight;
        domStyle.set(this.logoNode, 'top', value+'px');
        value = this.logoLeft;
        value = value * this.logoNode.offsetWidth;
        domStyle.set(this.logoNode, 'left', value+'px');
      }));


      if ('index' in this.model) {
        this.set('index', this.model.index);
      }
      else {
        this.set('index', 0);
      }
    }

  });
});
