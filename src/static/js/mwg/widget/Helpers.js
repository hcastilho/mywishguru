define([
    'dojo/_base/declare',
    'dojo/_base/lang',
    'dojo/topic',
    'dojo/on',
    'dojo/query',
    'dojo/dom',
    'dojo/dom-class',
    'dojo/dom-attr',
    'dojo/dom-style',
    'dojo/dom-geometry',
    'dojo/dom-construct',
    'dojo/text!./resources/helpers.html',
    'mwg/main',
    'dijit/_WidgetBase',
    'dijit/_TemplatedMixin',
    'dijit/_WidgetsInTemplateMixin'
  ], function(
    declare,
    lang,
    topic,
    on,
    query,
    dom,
    domClass,
    domAttr,
    domStyle,
    domGeom,
    domConstruct,
    template,
    main,
    _WidgetBase,
    _TemplatedMixin,
    _WidgetsInTemplateMixin) {

  return declare('mwg.widget.helpers', [_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin], {
    baseClass: 'helpers',
    templateString: template,

    total: 0,
    _setTotalAttr: function(value) {
      this._set('total', value);
      this.totalNode.innertHTML = value;
    },

    postCreate: function() {
      topic.subscribe('helpers_update', lang.hitch(this, 'update_helpers'));
    },

    update_helpers: function() {
      window.mwg.helpers.sort(main.sort_by_count);
      var helpers = window.mwg.helpers;
      var total = helpers.length;
      if (total > 0) {
        domClass.remove(this.domNode, 'hidden');
      }
      else {
        return;
      }

      this.set('total',total);

      if (window.mwg.helpers[0]) {
        main.getObjectData(
            window.mwg.helpers[0].user_id).then(
              lang.hitch(this, function(helper) {
          this.helper0Node.src = helper.picture.data.url;
          domClass.remove(this.helper0Node, 'hidden');
        }));
      }
      if (window.mwg.helpers[1]) {
        main.getObjectData(
            window.mwg.helpers[1].user_id).then(
              lang.hitch(this, function(helper) {
          this.helper1Node.src = helper.picture;
          domClass.remove(this.helper1Node, 'hidden');
        }));
      }
      if (window.mwg.helpers[2]) {
        main.getObjectData(
            window.mwg.helpers[2].user_id).then(
              lang.hitch(this, function(helper) {
          this.helper2Node.src = helper.picture;
          domClass.remove(this.helper2Node, 'hidden');
        }));
      }
      if (window.mwg.helpers[3]) {
        main.getObjectData(
            window.mwg.helpers[3].user_id).then(
              lang.hitch(this, function(helper) {
          this.helper3Node.src = helper.picture;
          domClass.remove(this.helper3Node, 'hidden');
        }));
      }

      if (total > 5)  {
        domClass.add(this.helper4Node, 'hidden');
        this.moreNode.innerHTML='+' + (total-4);
        domClass.remove(this.moreNode, 'hidden');
      }
      else {
        if (window.mwg.helpers[4]) {
          main.getObjectData(
              window.mwg.helpers[4].user_id).then(
                lang.hitch(this, function(helper) {
            this.helper4Node.src = helper.picture;
            domClass.remove(this.helper4Node, 'hidden');
            domClass.add(this.moreNode, 'hidden');
          }));
        }
      }
    }

  });
});
