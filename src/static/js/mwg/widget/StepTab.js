define([
    'dijit/_TemplatedMixin',
    'dijit/_WidgetBase',
    'dojo/_base/declare',
    'dojo/_base/lang',
    'dojo/dom-class',
    'dojo/dom-construct',
    'dojo/on',
    'dojo/topic',
    'dojo/mouse',
    'mwg/widget/BaseTab'
  ], function(
    _TemplatedMixin,
    _WidgetBase,
    declare,
    lang,
    domClass,
    domConstruct,
    on,
    topic,
    mouse,
    BaseTab) {

  return declare('mwg/widget/StepTab', [], {
    index: 0,
    tabTitle: '',
    selected: false,

    _setIndexAttr: function(value) {
      this._set('index', value);
      this.numberNode.innerHTML = value;
      //this.set('tabTitle', 'Step ' + String(value));
    },

    _setTabTitleAttr: function(value) {
      this._set('tabTitle', value);
      this.tabTitleNode.innerHTML = value;
      this.tabTitleNode.title = value;
    },

    _setSelectedAttr: function(value) {
      this._set('selected', value);
      if (value) {
        domClass.remove(this.selectedNode, 'step_regular');
        domClass.add(this.selectedNode, 'step_selected');
        domConstruct.create('img', {
          'class': 'arrow_selected',
          'src': '/images/step_arrow_blue.png'
        }, this.arrowNode);
      }
      else {
        domClass.remove(this.selectedNode, 'step_selected');
        domClass.add(this.selectedNode, 'step_regular');
        domConstruct.empty(this.arrowNode);
      }
    },

    stepTabPostCreate: function() {
      this.set('tabTitle', this.model.title);
      on(this.thumbNode, mouse.enter, lang.hitch(this, function() {
        domClass.add(this.selectedNode, 'thumbHover');
      }));
      on(this.thumbNode, mouse.leave, lang.hitch(this, function() {
        domClass.remove(this.selectedNode, 'thumbHover');
      }));
    }

  });
});
