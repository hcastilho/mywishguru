define([
    'dojo/_base/declare',
    'dojo/_base/lang',
    'dojo/dom-class',
    'dojo/on',
    'dojo/topic',
    'mwg/widget/BaseTab'
  ], function(
    declare,
    lang,
    domClass,
    on,
    topic,
    BaseTab) {

  return declare('mwg/widget/ViewTab', [BaseTab], {
    baseClass: 'baseTab wishTab',

    index: 0,

    postCreate: function() {
      this.inherited(arguments);
    }

  });
});
