define([
    'mwg/main',
    'dojo/_base/declare',
    'dojo/_base/lang',
    'dojo/string',
    'dojo/query',
    'dojo/on',
    'dijit/popup',
    'dijit/TooltipDialog'
    ], function(
        main,
        declare,
        lang,
        string,
        query,
        on,
        popup,
        TooltipDialog
        ) {

  return declare('mwg.widget.UserTooltip', [], {
    id: '',
    cover: '',
    picture: '',

    constructor: function(args) {
      this.tooltipDialog = {};
      declare.safeMixin(this, args);

      var tooltipDialog = new TooltipDialog({
          'class': 'userTooltip',
          onMouseLeave: function(){
              popup.close(tooltipDialog);
          }
      });
      this.tooltipDialog = tooltipDialog;

      main.getObjectData(this.id).then(lang.hitch(this, function(response) {
        var content = '<div class="userCard">';
        // Cover
        if ('cover' in response) {
          content += ['<a class="userCover"',
          'href="http://www.facebook.com/' + this.id + '" target="_blank">',
          '<img src="' + response.cover.source + '"/>',
          '</a>'].join('');
        }
        // Picture
        content += ['<div class="userPicture">',
          '<a href="http://www.facebook.com/' + this.id + '" target="_blank">',
          '<img src="' + response.picture.data.url + '"/>',
          '</a>',
          '</div>'].join('');
        // Name
        content += '<div class="userLinks">';
        content += ['<div>',
          '<a href="http://www.facebook.com/' + this.id + '" target="_blank">',
          response.name,
          '</a>',
          '</div>'].join('');
        // Friends in common
        if ((response.metadata.type !== 'page') && (this.id != window.mwg.config.userId) && ('mutualfriends' in response)) {
          var friend = 'friends';
          if (response.mutualfriends.data.length == 1) friend = 'friend';
          content += ['<div>',
            '<a class="mutualFriends" href="http://www.facebook.com/friends?id=' + this.id + '" target="_blank">',
            response.mutualfriends.data.length + ' mutual ' + friend,
            '</a>',
            '</div>'].join('');
        }
        // See Friendship
        if((response.metadata.type !== 'page') && (this.id != window.mwg.config.userId)) {
          content += ['<div>',
          '<a class="seeFriendship" href="https://www.facebook.com/profile.php?id=',
          this.id,
          '&and=',
          window.mwg.config.userId,
          '" target="_blank">See friendship</a>',
          '</div>'].join('');
        }
        content += '</div>';  // userLinks
        content += '</div>';  // userCard
        this.tooltipDialog.set('content', content);
      }));
    },

    open: function(node) {
      popup.open({
        popup: this.tooltipDialog,
        around: node
      });
    }
  });
});
