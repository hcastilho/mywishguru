define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/_base/Deferred',
  'dojo/topic',
  'dojo/on',
  'dijit/Dialog',
  'dojo/text!./resources/FbSharePage.html',
  'dijit/_WidgetBase',
  'dijit/_TemplatedMixin',
  'dijit/_WidgetsInTemplateMixin'
], function(
  declare,
  lang,
  Deferred,
  topic,
  on,
  Dialog,
  template,
  _WidgetBase,
  _TemplatedMixin,
  _WidgetsInTemplateMixin
  ) {

  var ConnectFbDialog = declare('mwg.widget.FbSharePage', [
      _WidgetBase,
      _TemplatedMixin,
      _WidgetsInTemplateMixin
      ], {

    deferred: '',

    baseClass: 'fbSharePage',
    templateString: template,

    //postMixInProperties: function(){
    //},

    postCreate: function() {
      //this.dialogNode.show();
      on(this.submitButton, 'click', lang.hitch(this, function() {
        this.deferred.resolve(this.messageNode.value);
        this.hide();
      }));
    },

    show: function() {
      var deferred = new Deferred();
      this.deferred = deferred;
      this.dialogNode.show();
      return deferred;
    },

    hide: function() {
      this.dialogNode.hide();
    }
  });

  return ConnectFbDialog;
});



