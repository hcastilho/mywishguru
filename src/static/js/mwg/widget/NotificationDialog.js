define([
    'mwg/main',
    'dojo/_base/declare',
    'dojo/_base/lang',
    'dojo/topic',
    'dojo/on',
    'dojo/dom',
    'dojo/dom-class',
    'dojo/dom-construct',
    'dojo/string',
    'dojo/cookie',
    'dijit/Dialog',
    'dijit/form/CheckBox',
    'dojo/text!./resources/notification_dialog.html',
    'dijit/_WidgetBase',
    'dijit/_TemplatedMixin',
    'dijit/_WidgetsInTemplateMixin'
    ], function(
      main,
      declare,
      lang,
      topic,
      on,
      dom,
      domClass,
      domCtr,
      string,
      cookie,
      Dialog,
      Checkbox,
      NotificationDialogTemplate,
      _WidgetBase,
      _TemplatedMixin,
      _WidgetsInTemplateMixin
      ) {

  NotificationDialog = declare('mwg.widget.NotificationDialog', [
      _WidgetBase,
      _TemplatedMixin,
      _WidgetsInTemplateMixin
      ], {

    baseClass: 'NotificationDialog',
    templateString: NotificationDialogTemplate,
    _csrf_token: '',

    show: function() {
      this.dialogNode.show();
    },

    hide: function() {
      this.dialogNode.hide();
    },

    postMixInProperties: function() {
      this.notification_by_facebook_checked = '';
      if (this.notification_by_facebook)
        this.notification_by_facebook_checked = 'checked';
      this.notification_by_email_checked = '';
      if (this.notification_by_email)
        this.notification_by_email_checked = 'checked';
      this.notification_friend_join_checked = '';
      if (this.notification_friend_join)
        this.notification_friend_join_checked = 'checked';
      this.notification_my_wish_share_checked = '';
      if (this.notification_friend_join)
        this.notification_my_wish_share_checked = 'checked';
      this.notification_my_wish_comment_checked = '';
      if (this.notification_my_wish_comment)
        this.notification_my_wish_comment_checked = 'checked';
      this.notification_my_wish_like_checked = '';
      if (this.notification_my_wish_like)
        this.notification_my_wish_like_checked = 'checked';
      this.notification_friend_wish_create_checked = '';
      if (this.notification_friend_wish_create)
        this.notification_friend_wish_create_checked = 'checked';
      this.notification_friend_wish_fulfill_checked = '';
      if (this.notification_friend_wish_fulfill)
        this.notification_friend_wish_fulfill_checked = 'checked';
      this.notification_favorite_wish_update_checked = '';
      if (this.notification_favorite_wish_update)
        this.notification_favorite_wish_update_checked = 'checked';
      this.notification_favorite_wish_fulfill_checked = '';
      if (this.notification_favorite_wish_fulfill)
        this.notification_favorite_wish_fulfill_checked = 'checked';
      this.notification_favorite_wish_share_checked = '';
      if (this.notification_favorite_wish_share)
        this.notification_favorite_wish_share_checked = 'checked';
      this.notification_favorite_wish_comment_checked = '';
      if (this.notification_favorite_wish_comment)
        this.notification_favorite_wish_comment_checked = 'checked';
      this.notification_favorite_wish_like_checked = '';
      if (this.notification_favorite_wish_like)
        this.notification_favorite_wish_like_checked = 'checked';
    },

    postCreate: function() {
      on(this.closeNode, 'click', lang.hitch(this, function() {
        this.dialogNode.hide();
      }));
      this._csrf_token = cookie('c');
      if (window.mwg.account) {
        domClass.add(this.friend_join_wrapper, 'hidden');
        domClass.add(this.friendsWishes, 'hidden');
      }

      on(this.notification_by_facebook_check, 'change', lang.hitch(this, function() {
        this.storeChange(
          'notification_by_facebook',
          this.notification_by_facebook_check.checked);
      }));
      on(this.notification_by_email_check, 'change', lang.hitch(this, function() {
        this.storeChange(
          'notification_by_email',
          this.notification_by_email_check.checked);
      }));
      on(this.notification_friend_join_check, 'change', lang.hitch(this, function() {
        this.storeChange(
          'notification_friend_join',
          this.notification_friend_join_check.checked);
      }));
      on(this.notification_my_wish_share_check, 'change', lang.hitch(this, function() {
        this.storeChange(
          'notification_my_wish_share',
          this.notification_my_wish_share_check.checked);
      }));
      on(this.notification_my_wish_comment_check, 'change', lang.hitch(this, function() {
        this.storeChange(
          'notification_my_wish_comment',
          this.notification_my_wish_comment_check.checked);
      }));
      on(this.notification_my_wish_like_check, 'change', lang.hitch(this, function() {
        this.storeChange(
          'notification_my_wish_like',
          this.notification_my_wish_like_check.checked);
      }));
      on(this.notification_friend_wish_create_check, 'change', lang.hitch(this, function() {
        this.storeChange(
          'notification_friend_wish_create',
          this.notification_friend_wish_create_check.checked);
      }));
      on(this.notification_friend_wish_fulfill_check, 'change', lang.hitch(this, function() {
        this.storeChange(
          'notification_friend_wish_fulfill',
          this.notification_friend_wish_fulfill_check.checked);
      }));
      on(this.notification_favorite_wish_update_check, 'change', lang.hitch(this, function() {
        this.storeChange(
          'notification_favorite_wish_update',
          this.notification_favorite_wish_update_check.checked);
      }));
      on(this.notification_favorite_wish_fulfill_check, 'change', lang.hitch(this, function() {
        this.storeChange(
          'notification_favorite_wish_fulfill',
          this.notification_favorite_wish_fulfill_check.checked);
      }));
      on(this.notification_favorite_wish_share_check, 'change', lang.hitch(this, function() {
        this.storeChange(
          'notification_favorite_wish_share',
          this.notification_favorite_wish_share_check.checked);
      }));
      on(this.notification_favorite_wish_comment_check, 'change', lang.hitch(this, function() {
        this.storeChange(
          'notification_favorite_wish_comment',
          this.notification_favorite_wish_comment_check.checked);
      }));
      on(this.notification_favorite_wish_like_check, 'change', lang.hitch(this, function() {
        this.storeChange(
          'notification_favorite_wish_like',
          this.notification_favorite_wish_like_check.checked
          );
      }));
    },

    storeChange: function(notification, value) {
      var args = {
        _csrf_token: this._csrf_token,
        user_id: window.mwg.config.userId
      };
      args[notification] = value;
      main.userStore.put(args);
    }

  });

  return NotificationDialog;
});



