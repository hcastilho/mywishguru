define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/_base/Deferred',
  'dojo/on',
  'dojo/cache',
  'dojo/store/JsonRest',
  'mwg/widget/BaseModel',
  'dijit/registry'
], function(
  declare,
  lang,
  Deferred,
  on,
  cache,
  Store,
  BaseModel,
  registry) {

  return declare('', [BaseModel], {

    baseClass: 'stepModel',

    step_id: '',
    index: '',
    wish_id: null,
    //privacy: '',

    store: new Store({
      target: '/rest/step/',
      idProperty: 'step_id'
    }),

    getDisplayTitle: function() {
      var title = this.title;
      if (this.title === '') {
        title = 'New Step';
      }
      return title;
    },

    _setIndexAttr: function(value) {
      if (this.index == value) return;
      this.changed.index = '';
      this._set('index', value);
    },

    postCreate: function() {
      this.inherited(arguments);
      //this.set('privacy', this.wish_id);
      //this.wish.watch('privacy', lang.hitch(this, function(name, oldValue, value) {
      //  this.set('privacy', value);
      //}));
    },

    // BaseModel.deleteModel
    deleteModel: function() {
      if (this.step_id === '') return;
      return this.store.remove(this.step_id).then(lang.hitch(this, function() {
        _gaq.push(['_trackEvent', 'Wish', 'Update', this.wish_id.toString()]);
      }));
    },

    // Overload BaseModel.putModel
    putModel: function() {
      var args = {
        _csrf_token: this._csrf_token,
        wish_id: this.wish_id
      };
      if (this.step_id) {
        args.step_id = this.step_id;
      }
      for (var key in this.changed) {
        args[key] = this[key];
      }
      this.changed = {};

      return this.store.put(args).then(
          lang.hitch(this, function(response) {
            if ('error' in response) {
              console.warn('679aae1a-7a70-49bd-8573-3d7b117aa403');
              console.warn(response);
            }
            else {
              _gaq.push([
                '_trackEvent',
                'Wish',
                'Update',
                response.wish_id.toString()]);
              if (this.step_id === '') {
                this.step_id = response.step_id;
              }
            }
            return response;
          }));
    }
  });
});

