define([
    'mwg/main',
    'mwg/widget/WishTab',
    'mwg/widget/ViewTab',
    'mwg/widget/TwoButtonDialog',
    //'mwg/widget/Helpers',
    'dojo/_base/declare',
    'dojo/query',
    'dojo/on',
    'dojo/dom',
    'dojo/dom-class',
    'dojo/dom-construct',
    'dojo/string',
    'dojo/topic',
    'dojo/_base/lang',
    'dojo/text!./resources/WishViewTab.html'
  ], function(
    main,
    WishTab,
    ViewTab,
    TwoButtonDialog,
    //Helpers,
    declare,
    query,
    on,
    dom,
    domClass,
    domConstruct,
    string,
    topic,
    lang,
    template) {

  return declare('mwg/widget/WishViewTab', [WishTab, ViewTab], {
    baseClass: 'wishTab wishViewTab',
    templateString: template,

    model: null,
    favorite: false,
    privacy: null,

    _setAccountAttr: function(value) {
      if (window.mwg.account)
        return;
      this._set('account', value);
      domConstruct.empty(this.pageNode);
      if (value && 'id' in value) {
        domClass.remove(this.pageNode, 'hidden');
        domClass.remove(this.pageNode, 'no_page');
        var account_template = ['<div class="userbox">',
          '<ul>',
          '<li>',
          '<img class="userPicture" src="${picture}"/>',
          '</li>',
          '<li class="extrapad">',
          '${name}',
          '</li>',
          '</ul>',
          '</div>'].join('');
        var item = domConstruct.toDom(string.substitute(account_template, value));
        domConstruct.place(item, this.pageNode);
      }
    },

    _setStatusAttr: function(value) {
      this._set('status', value);
      if (this.model.owner) {
        if (value == 'fulfilled') {
          this.fulfilledNode.title = 'Unfulfill wish';
          domClass.remove(this.fulfilledNode, 'ribbon_btn');
          domClass.add(this.fulfilledNode, 'ribbon_btn_full');
        }
        else {
          this.fulfilledNode.title = 'Fulfill wish';
          domClass.remove(this.fulfilledNode, 'ribbon_btn_full');
          domClass.add(this.fulfilledNode, 'ribbon_btn');
        }
      }
      else {
        if (value == 'fulfilled') {
          this.fulfilledNode.title = 'Wish fulfilled';
          domClass.remove(this.fulfilledNode, 'ribbon_default');
          domClass.add(this.fulfilledNode, 'ribbon_done');
        }
        else {
          this.fulfilledNode.title = "Wish not fulfilled";
          domClass.remove(this.fulfilledNode, 'ribbon_done');
          domClass.add(this.fulfilledNode, 'ribbon_default');
        }
      }
    },

    _setFavoriteAttr: function(value) {
      this._set('favorite', value);
      if (value) {
        domClass.add(this.favoriteButton, 'favselected');
        this.favoriteButton.innerHTML = 'In Favorites';
      }
      else {
        domClass.remove(this.favoriteButton, 'favselected');
        this.favoriteButton.innerHTML = 'Add to Favorites';
      }
    },

    _setPrivacyAttr: function(value) {
      this._set('privacy', value);
      if (value === 'invisible') {
        domClass.add(this.privacyNode, 'onlyme');
        this.privacyNode.innerHTML = 'Only Me';
      }
      else if (value === 'friends') {
        domClass.add(this.privacyNode, 'jfriends');
        this.privacyNode.innerHTML = 'Friends';
      }
      else if (value === 'fof') {
        domClass.add(this.privacyNode, 'ff');
        this.privacyNode.innerHTML = 'Friends of Friends';
      }
      else if (value === 'public') {
        domClass.add(this.privacyNode, 'public');
        this.privacyNode.innerHTML = 'Public';
      }
      else if (value === 'flist') {
        domClass.add(this.privacyNode, 'flist');
        this.privacyNode.innerHTML = 'Friendlist';
      }
    },

    postCreate: function() {
      this.inherited(arguments);
      this.wishTabPostCreate();

      this.thumbNode.href = '/wish/' + this.model.wish_id;
      this.numberLink.href = '/wish/' + this.model.wish_id;

      // Favorite
      this.init_favorite();

      // Fulfilled
      this.init_fulfilled();

      // Privacy
      this.set('privacy', this.model.privacy);

      // Edit
      if (this.model.owner) {
        this.editButton.href = '/editwish/' + this.model.wish_id;
      }
      else {
        domClass.add(this.editButton, 'hidden');
      }

      // Contact
      this.init_contact();

      // Remove tag button for account owners
      this.init_remove_tag();

      // Stats
      topic.subscribe('WishStats', lang.hitch(this, function(stats) {
        this.likesNode.innerHTML = stats.likes;
        this.commentsNode.innerHTML = stats.comments;
        this.sharesNode.innerHTML = stats.shares;
      }));

      // Helpers
      //this.helpers_node = new Helpers().placeAt(this.helpers_node);
    },

    init_favorite: function() {
      if (!window.mwg.config.userId || this.model.owner) {
        domClass.add(this.favoriteButton, 'hidden');
      }
      else {
        this.set('favorite', this.model.favorite);
        this.model.watch('favorite', lang.hitch(this, function(name, oldValue, value) {
          this.set('favorite', value);
        }));
        on(this.favoriteButton, 'click', lang.hitch(this, function() {
          this.model.toggleFavorite();
        }));
      }
    },

    init_fulfilled: function() {
      if (this.model.owner) {
        domClass.add(this.fulfilledNode, 'clickable');
        this.model.watch('status', lang.hitch(this, function(name, oldValue, value) {
          this.set('status', value);
        }));
        on(this.fulfilledNode, 'click', lang.hitch(this, function() {
          this.model.toggleFulfill();
        }));
      }
    },

    init_contact: function() {
      if (this.model.owner) {
        domClass.add(this.contactButton, 'hidden');
      }
      else {
        var to = this.model.user.user_id;
        if (this.model.admins.length >= 1) {
          to = this.model.admins[0];
        }
        on(this.contactButton, 'click', lang.hitch(this, function() {
          FB.ui({
            method: 'send',
            to: to,
            link: this.model.url
          });
        }));
      }
    },

    init_remove_tag: function() {
      if (window.mwg.account &&
          this.model.account &&
          this.model.user_type != 'account' &&
          this.model.account.id == window.mwg.account.id) {
        var remove_tag_node = domConstruct.toDom(
            '<li><div class="delete">Remove Tag</div></li>'
            );
        domConstruct.place(remove_tag_node, this.userbtnsNode);
        //var remove_tag_node = domConstruct.create('li',
        //    {innerHTML: '<div class="delete">Remove Tag</div>'},
        //  //{innerHTML: 'Remove Tag'},
        //    this.userbtnsNode);
        on(remove_tag_node, 'click', lang.hitch(this, function() {
          var args = {
            _csrf_token: this.model._csrf_token,
            wish_id: this.model.wish_id,
            remove_account: true
          };
          var confDialog = new TwoButtonDialog(
            {
              title: 'Remove Tag',
              message: 'Are you sure?',
              confirmMsg: 'Delete',
              cancelMsg: 'Cancel'
            });
          Deferred.when(confDialog.show(), lang.hitch(this, function(response) {
            if (response) {
              main.wishStore.put(args);
              window.location.reload();
            }
          }));
        }));
      }
    }

  });
});
