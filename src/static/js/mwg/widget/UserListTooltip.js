define([
    'dojo/_base/declare',
    'dojo/_base/lang',
    'dojo/query',
    'dojo/on',
    'dijit/popup',
    'dijit/TooltipDialog'
    ], function(
        declare,
        lang,
        query,
        on,
        popup,
        TooltipDialog
        ) {

  return declare('mwg.widget.UserTooltip', [], {
    constructor: function(args) {
      this.tooltipDialog = {};
      this.users = [];
      declare.safeMixin(this, args);

      var content = '<ul>';
      for (var i = 0; i<this.users.length; i++) {
        content += ['<li>',
          '<span>',
          '<a href="http://www.facebook.com/' + this.users[i].id + '" target="_blank">',
          this.users[i].name,
          '</a>',
          '</span></li>'].join('');
      }
      content += '</ul>';
      var tooltipDialog = new TooltipDialog({
          'class': 'userListTooltip',
          content: content,
          onMouseLeave: function(){
              popup.close(tooltipDialog);
          }
      });
      this.tooltipDialog = tooltipDialog;
    },

    open: function(node) {
      popup.open({
        popup: this.tooltipDialog,
        around: node
      });
    }
  });
});
