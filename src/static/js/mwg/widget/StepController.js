define([
    'mwg/main',
    'mwg/widget/StepEditTab',
    'mwg/widget/StepEdit',
    'mwg/widget/StepViewTab',
    'mwg/widget/StepView',
    'mwg/widget/StepModel',
    'mwg/widget/WishEditTab',
    'mwg/widget/WishViewTab',
    'dojo/_base/declare',
    'dojo/_base/lang',
    'dojo/DeferredList',
    'dojo/on',
    'dojo/topic',
    'dojo/dom',
    'dojo/dom-construct',
    'dijit/_WidgetBase',
    'dijit/_TemplatedMixin',
    'dijit/_WidgetsInTemplateMixin',
    'dijit/layout/StackContainer',
    'dojo/text!./resources/StepController.html'
  ], function(
    main,
    StepEditTab,
    StepEdit,
    StepViewTab,
    StepView,
    StepModel,
    WishEditTab,
    WishViewTab,
    declare,
    lang,
    DeferredList,
    on,
    topic,
    dom,
    domConstruct,
    _WidgetBase,
    _TemplatedMixin,
    _WidgetsInTemplateMixin,
    StackContainer,
    StepControllerTemplate) {

  return declare('mwg/widget/StepController',
      [_WidgetBase,
      _TemplatedMixin,
      _WidgetsInTemplateMixin], {

    baseClass: 'stepController',
    templateString: StepControllerTemplate,
    selectedTabWidget: null,
    selectedStackWidget: null,
    selectedModel: null,

    stackContainerNode: null,
    stackContainer: null,

    wish: null,
    steps: null,
    models: null,

    edit: false,
    selectedStep: null, // Step selected in url

    deletedChildren: [],

    addChild: function(widget, tabWidget, insertIndex) {
      this.stackContainer.addChild(widget);
      this.addTab(tabWidget, insertIndex);
    },

    addTab: function(tabWidget, insertIndex) {
      var refNode = this.containerNode,
        children = this.getChildren();
      if(insertIndex && typeof insertIndex == "number"){
        if(children && children.length >= insertIndex){
          refNode = children[insertIndex-1].domNode;
          insertIndex = "after";
        }
      }

      domConstruct.place(tabWidget.domNode, refNode, insertIndex);
    },

    check: function() {
      var children = this.stackContainer.getChildren();
      var errors = [];
      for (var i=0; i<children.length; i++) {
        errors = errors.concat(children[i].check());
      }
      return errors;
    },

    addStep: function() {
      var errors = this.check();
      if (errors.length>0) {
        return;
      }

      //var models = this.getModels(),
      var index = this.models.length,
        step = {wish_id: this.wish.wish_id},
        model = new StepModel(step);
      this.steps.push(model);
      this.models.push(model);
      model.set('index', index);


      var view = new StepEdit({model: model}),
        tab = new StepEditTab({model: model});

      this.addChild(view, tab, index);
      this.selectChild(model);
    },

    removeChild: function(model) {
      if ('step_id' in model) {
        var widget = this.getStackWidget(model);
        this.stackContainer.removeChild(widget);

        widget = this.getTabWidget(model);
        var node = widget.domNode;
        if(node && node.parentNode){
          node.parentNode.removeChild(node); // detach but don't destroy
        }

        var index;
        // Remove from steps
        index = this.steps.indexOf(model);
        this.steps.splice(index, 1);

        // Remove Step
        index = this.models.indexOf(model);
        this.models.splice(index, 1);

        // Re-index Steps
        for (var i=index; i<this.models.length; i++) {
          this.models[i].set('index', i);
        }

        this.selectChild(this.models[index - 1]);
      }

      if ('step_id' in model) {
        if (model.step_id !== '') {
          this.deletedChildren.push(model);
        }
      }
      else {
        if (model.wish_id !== '') {
          model.deleteModel().then(function() {
            window.location = '/';
          });
        }
        else {
          window.location = '/';
        }
      }
    },

    selectChild: function(model) {
      this.selectedModel = model;

      this._selectView(model);
      this._selectTab(model);

      if (!this.edit) {
        dom.byId('wishtitle').innerHTML = model.title;
        dom.byId('wishtitle').title = model.title;
      }
      // Show selected timeline
      //this.refreshTimeline();
    },

    _selectView: function(model) {
      var widget = this.getStackWidget(model);
      this.selectedStackWidget = widget;
      this.stackContainer.selectChild(widget, true);
      if (this.edit) {
        lang.hitch(widget, widget.updateDisplay());
      }
    },

    _selectTab: function(model) {
      // Show selected tab
      var widget = this.getTabWidget(model);
      this.selectedTabWidget = widget;
      var children = this.getChildren();
      for (i=0; i<children.length; i++) {
        if (children[i] === widget) {
          children[i].set('selected', true);
        }
        else {
          children[i].set('selected', false);
        }
      }
    },

    //getIndex: function(model) {
    //  var i, children;
    //  children = this.stackContainer.getChildren();
    //  for (i=0; i<children.length; i++) {
    //    if(children[i].model === model) {
    //      return i;
    //    }
    //  }
    //},

    getModels: function() {
      //var i, children, models = [];
      //children = this.stackContainer.getChildren();
      //for (i=0; i<children.length; i++) {
      //  if ('model' in children[i]) {
      //    models.push(children[i].model);
      //  }
      //}
      //return models;
      return [this.wish].concat(this.steps);
    },

    //getComponents: function() {
    //  return {
    //    stack: this.stackContainer.getChildren(),
    //    tab: this.getChildren(),
    //    models: this.getModels()
    //  };
    //},

    getStackWidget: function(model) {
      var i, children;
      children = this.stackContainer.getChildren();
      for (i=0; i<children.length; i++) {
        if(children[i].model === model) {
          return children[i];
        }
      }
    },

    getTabWidget: function(model) {
      children = this.getChildren();
      for (i=0; i<children.length; i++) {
        if (children[i].model === model) {
          return children[i];
        }
      }
    },

    //getLastFulfilled: function() {
    //  var models = this.getModels();
    //  for (var i=1; i<models.length; i++) {
    //    if (models[i].status !== 'fulfilled') {
    //      return {idx: i-1, model: models[i-1]};
    //    }
    //  }
    //  return {idx: i-1, model: models[i-1]};
    //},

    //refreshTimeline: function() {
    //  var widget = this.selectedTabWidget,
    //    children = this.getChildren(),
    //    widgetIndex = 0,
    //    i;

    //  for (i=0; i<children.length; i++) {
    //    if (widget === children[i]) {
    //      widgetIndex = i;
    //    }
    //  }
    //  for (i=0; i<children.length; i++) {
    //    if (i < widgetIndex) {
    //      children[i].set('timeline', 'full');
    //    }
    //    else if (i == widgetIndex) {
    //      children[i].set('timeline', 'last');
    //    }
    //    else {
    //      children[i].set('timeline', 'empty');
    //    }
    //  }
    //},

    postCreate: function() {

      this.models = [this.wish].concat(this.steps);

      this.stackContainer = new StackContainer({}, this.stackContainerNode);
      this.stackContainer.startup();

      var tab, view;
      if (this.edit) {
        view =  new StepEdit({model: this.wish});
        tab =  new WishEditTab({model: this.wish});
        this.addChild(view, tab, 0);

        for (i=0; i<this.steps.length; i++) {
          view =  new StepEdit({model: this.steps[i]});
          tab =  new StepEditTab({model: this.steps[i]});
          this.addChild(view, tab, i+1);
        }
      }
      else {

        if (this.wish.owner) {
          // Click to add step button
          var addtabHtml = ['<a href="/editwish/' + this.wish.wish_id + '?newstep">',
          '<div class="stepTab stepcnt">',
          '<div class="addstep"></div>',
          '</div>',
          '</a>'].join('');
          domConstruct.place(addtabHtml, this.containerNode, 'last');
        }

        // Add Tabs
        tab =  new WishViewTab({model: this.wish});
        this.addTab(tab, 0);
        for (i=0; i<this.steps.length; i++) {
          tab =  new StepViewTab({model: this.steps[i]});
          this.addTab(tab, i+1);
        }

        // Get selected step
        if (this.selectedStep === 0) {
          this.selectedStep = this.wish;
        }
        else {
          this.selectedStep = this.models[this.selectedStep];
        }
        main.hasPermission('read_stream').then(lang.hitch(this, function(response) {
          this.selectedStep.getPosts();
        }));

        // Add Single View
        view =  new StepView({model: this.selectedStep});
        this.stackContainer.addChild(view);
        this.selectChild(this.selectedStep);
      }

      topic.subscribe('stepController.selectChild', lang.hitch(this, 'selectChild'));
      topic.subscribe('stepController.removeChild', lang.hitch(this, 'removeChild'));
      topic.subscribe('stepController.addStep', lang.hitch(this, 'addStep'));
      topic.subscribe('stepController.publishChildren', lang.hitch(this, 'publishChildren'));
    },

    publishChildren: function() {
      var stepsDef = [],
        models = this.getModels(),
        wishModel = models[0];

      var errors = this.check();
      if (errors.length>0) {
        // TODO error message ?
        return;
      }

      wishModel.publishModel().then(lang.hitch(this, function(response) {
        if (response.wish_id === '') return;
        var i;
        for (i=1; i<models.length; i++) {
          models[i].wish_id = response.wish_id;
          stepsDef.push(models[i].publishModel());
        }
        for (i=0; i<this.deletedChildren.length; i++) {
          stepsDef.push(this.deletedChildren[i].deleteModel());
        }
        stepsDef = new DeferredList(stepsDef);
        stepsDef.then(function() {
          window.location = response.url;
        });
      }));
    }

  });
});
