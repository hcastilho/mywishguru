define([
  'mwg/main',
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/query',
  'dojo/topic',
  'dojo/on',
  'dojo/dom',
  'dojo/dom-construct',
  'dojo/string',
  'dijit/Dialog',
  'dojo/text!./resources/filter_dialog.html',
  'dijit/_WidgetBase',
  'dijit/_TemplatedMixin',
  'dijit/_WidgetsInTemplateMixin'
], function(
  main,
  declare,
  lang,
  query,
  topic,
  on,
  dom,
  domConstruct,
  string,
  Dialog,
  FilterDialogTemplate,
  _WidgetBase,
  _TemplatedMixin,
  _WidgetsInTemplateMixin
  ) {

  FilterDialog = declare('mwg.widget.FilterDialog', [
      _WidgetBase,
      _TemplatedMixin,
      _WidgetsInTemplateMixin
      ], {

    baseClass: 'FilterDialog',
    templateString: FilterDialogTemplate,
    filterItem: ['<div class="userbox">',
      '<ul>',
      '<li>',
      '<a href="${url}"><img class="userPicture" src="${picture}"/></a>',
      '</li>',
      '<li class="extrapad">',
      '<a href="${url}">${name}</a>',
      '</li>',
      '</ul>',
      '</div>'].join(''),

    accounts: null,

    show: function() {
      this.dialogNode.show();
    },

    hide: function() {
      this.dialogNode.hide();
    },

    postCreate: function() {
      this.titleNode.innerHTML = 'Pages on MyWishGuru';
      this.searchNode.placeholder = 'search page by name here...';
      if (window.mwg.accounts) {
        this.loadAccounts(window.mwg.accounts);
      }
      else {
        main.accountStore.query().then(lang.hitch(this, function(response) {
          this.loadAccounts(window.mwg.friends);
        }));
      }
    },

    showFilter: function(filter) {
      this.boxNode.innerHTML = '';
      filter = typeof filter !== 'undefined' ? filter : '';
      var item;
      var flist = [];
      for (var i = 0; i < this.accounts.length; i++) {
        if (main.startsWith(this.accounts[i].name, filter)) {
          this.place_item(this.accounts[i]);
        }
      }
    },

    place_item: function(args) {
      item = domConstruct.toDom(string.substitute(this.filterItem, args));
      domConstruct.place(item, this.boxNode);
    },

    loadAccounts: function(response) {
      this.accounts = response;
      this.showFilter(this.searchNode.value);
      on(this.searchNode, 'keyup', lang.hitch(this, function(evt) {
        this.showFilter(this.searchNode.value);
      }));
    }


  });

  return FilterDialog;
});



