define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/_base/Deferred',
  'dojo/store/JsonRest',
  'dojo/cookie',
  'mwg/widget/BaseModel'
], function(
  declare,
  lang,
  Deferred,
  Store,
  cookie,
  BaseModel) {

  return declare('', [BaseModel], {

    wish_id: '',
    status: '',
    privacy: '',
    user_type: '',

    favorite: false,
    owner: false,
    userType: 'member', // 'visitor'

    store: new Store({
      target: '/rest/wish/',
      idProperty: 'wish_id'
    }),

    favoriteStore: new Store({
      target: '/rest/favorite/',
      idProperty: 'wish_id'
    }),

    getDisplayTitle: function() {
      var title = this.title;
      if (this.title === '') {
        title = 'New Wish';
      }
      return title;
    },

    _setFavoriteAttr: function(value) {
      this._set('favorite', value);
    },

    _setAccountAttr: function(value) {
      this.changed.account = '';
      this._set('account', value);
    },

    _setPrivacyAttr: function(value) {
      if (this.privacy == value) return;
      this.changed.privacy = '';
      this._set('privacy', value);

      if (value != 'friendlist') {
        this.set('friendlist', '');
      }
    },

    _setFriendlistAttr: function(value) {
      if (this.privacy !== 'friendlist') {
        if (this.friendlist !== '') {
          this.changed.friendlist = '';
          this._set('friendlist', '');
          return;
        }
      }

      if (this.friendlist == value) return;
      this.changed.friendlist = '';
      this._set('friendlist', value);
    },

    postCreate: function() {
      this.inherited(arguments);
      if (this.privacy === '') {
        this.set('privacy', 'public');
      }
      if (this.status === '') {
        this.set('status', 'open');
      }
    },

    toggleFavorite: function() {
      if (this.favorite) {
        this.set('favorite', false);
        this.putFavorite();
      }
      else {
        this.set('favorite', true);
        this.putFavorite();
      }
    },

    deleteModel: function() {
      return this.store.remove(this.wish_id).then(lang.hitch(this, function() {
        _gaq.push(['_trackEvent', 'Wish', 'Delete', this.wish_id.toString()]);
      }));
    },

    putModel: function() {
      var args = {
          _csrf_token: this._csrf_token
        };

      if (this.wish_id) {
        args.wish_id = this.wish_id;
      }
      for (var key in this.changed) {
        args[key] = this[key];

        if (key === 'status' && this.wish_id !== '') {
          if (this[key] === 'fulfilled') {
            _gaq.push(['_trackEvent',
                'Wish',
                'Fulfilled',
                this.wish_id.toString()]);
          }
          else if (this[key] === 'open') {
            _gaq.push(['_trackEvent',
                'Wish',
                'Unfulfilled',
                this.wish_id.toString()]);
          }
        }
      }
      this.changed = {};

      return this.store.put(args).then(
          lang.hitch(this, function(response) {
            if ('error' in response) {
              console.warn('bb5fcf37-ba2b-4c9c-8cb8-d99a3c6fd076');
              console.warn(response);
            }
            else {
              if (this.wish_id === '') {
                _gaq.push(['_trackEvent',
                  'Wish',
                  'Create',
                  response.wish_id.toString()]);
                this.wish_id = response.wish_id;
                this.url = response.url;
              }
              else {
                _gaq.push(['_trackEvent',
                  'Wish',
                  'Update',
                  response.wish_id.toString()]);
              }
            }
            return response;
          }));
    },

    putFavorite: function() {
      var args = {
          _csrf_token: this._csrf_token,
          wish_id: this.wish_id,
          favorite: this.favorite
        };
        this.favoriteStore.put(args).then(function(response) {
          if (this.favorite) {
            _gaq.push(['_trackEvent',
              'Wish',
              'Favorite',
              response.wish_id.toString()]);
          }
          else {
            _gaq.push([
              '_trackEvent',
              'Wish',
              'Unfavorite',
              response.wish_id.toString()]);
          }
        });
    }


  });
});


