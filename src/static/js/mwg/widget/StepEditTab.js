define([
    'dojo/_base/declare',
    'dojo/_base/lang',
    'dojo/dom-class',
    'dojo/text!./resources/StepEditTab.html',
    'mwg/widget/EditTab',
    'mwg/widget/StepTab'
  ], function(
    declare,
    lang,
    domClass,
    template,
    EditTab,
    StepTab) {

  return declare('mwg/widget/StepEditTab', [StepTab, EditTab], {
    baseClass: 'stepTab StepEditTab',
    templateString: template,

    _setSelectedAttr: function(value) {
      this.inherited(arguments);
      if (value) {
        domClass.remove(this.deleteButton, 'hidden');
      }
      else {
        domClass.add(this.deleteButton, 'hidden');
      }
    },

    postCreate: function() {
      this.inherited(arguments);
      this.stepTabPostCreate();
    }

  });
});
