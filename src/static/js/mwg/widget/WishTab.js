define([
    'dijit/_TemplatedMixin',
    'dijit/_WidgetBase',
    'dojo/_base/declare',
    'dojo/_base/lang',
    'dojo/dom-class',
    'dojo/dom-construct',
    'dojo/string',
    'dojo/on',
    'dojo/topic',
    'dojo/mouse',
    'mwg/widget/BaseTab'
  ], function(
    _TemplatedMixin,
    _WidgetBase,
    declare,
    lang,
    domClass,
    domConstruct,
    string,
    on,
    topic,
    mouse,
    BaseTab) {

  return declare('mwg/widget/WishTab', [], {
    status: null,
    selected: false,

    _setSelectedAttr: function(value) {
      this._set('selected', value);
      if (value) {
        domClass.add(this.selectedNode, 'selected');
        domConstruct.create('img', {
          'class': 'arrow_selected',
          'src': '/images/step_arrow_yellow.png'
        }, this.arrowNode);
      }
      else {
        domClass.remove(this.selectedNode, 'selected');
        domConstruct.empty(this.arrowNode);
      }
    },

    wishTabPostCreate: function() {
      // Fulfilled
      this.set('status', this.model.status);
      if ('account' in this.model)
        this.set('account', this.model.account);

      on(this.thumbNode, mouse.enter, lang.hitch(this, function() {
        domClass.add(this.selectedNode, 'thumbHover');
      }));
      on(this.thumbNode, mouse.leave, lang.hitch(this, function() {
        domClass.remove(this.selectedNode, 'thumbHover');
      }));
    }

  });
});
