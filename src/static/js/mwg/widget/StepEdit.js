define([
    'mwg/widget/BaseEdit',
    'dojo/_base/declare',
    'dojo/_base/lang',
    'dojo/on',
    'dojo/dom',
    'dojo/text!./resources/StepEdit.html'
  ], function(
    BaseEdit,
    declare,
    lang,
    on,
    dom,
    template) {

  return declare('mwg.widget.StepEdit', [BaseEdit], {
    baseClass: 'baseEdit stepEdit',
    templateString: template

  });
});
