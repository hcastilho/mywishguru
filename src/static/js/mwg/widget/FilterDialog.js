define([
  'mwg/main',
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/query',
  'dojo/topic',
  'dojo/on',
  'dojo/dom',
  'dojo/dom-construct',
  'dojo/string',
  'dijit/Dialog',
  'dojo/text!./resources/filter_dialog.html',
  'dijit/_WidgetBase',
  'dijit/_TemplatedMixin',
  'dijit/_WidgetsInTemplateMixin'
], function(
  main,
  declare,
  lang,
  query,
  topic,
  on,
  dom,
  domCtr,
  string,
  Dialog,
  FilterDialogTemplate,
  _WidgetBase,
  _TemplatedMixin,
  _WidgetsInTemplateMixin
  ) {

  var startswith = function(str, strcmp) {
    return str.toLowerCase().slice(0, strcmp.length) == strcmp.toLowerCase();
  },

  FilterDialog = declare('mwg.widget.FilterDialog', [
      _WidgetBase,
      _TemplatedMixin,
      _WidgetsInTemplateMixin
      ], {

    baseClass: 'FilterDialog',
    templateString: FilterDialogTemplate,
    //filterItem: ['<li>',
    //  '<img class="userPicture" src="${picture}" ${title} ${imgdisabled}/>',
    //  '<span ${title} ${labeldisabled}>${name}</span>',
    //  '<input type="checkbox" name="user_select" id="check-${user_id}" value="${user_id}" ${title} ${disabled}/>',
    //  '</li>'].join(''),
    filterItem: ['<div class="userbox">',
      '<ul>',
      '<li>',
      '<a ${title} href="${url}"><img class="userPicture" src="${picture}" ${title} ${imgdisabled}/></a>',
      '</li>',
      '<li class="extrapad">',
      '<a ${title} href="${url}" ${labeldisabled}>${name}</a>',
      '</li>',
      '</ul>',
      '</div>'].join(''),

    friendList: null,

    show: function() {
      this.dialogNode.show();
    },

    hide: function() {
      this.dialogNode.hide();
    },

    postCreate: function() {
      this.titleNode.innerHTML = 'Friends on MyWishGuru';
      if (window.mwg.friends) {
        this.loadFriends(window.mwg.friends);
      }
      else {
        main.friendStore.query().then(lang.hitch(this, function(response) {
          this.loadFriends(window.mwg.friends);
        }));
      }
    },

    showFriends: function(filter) {
      this.boxNode.innerHTML = '';

      filter = typeof filter !== 'undefined' ? filter : '';

      var item;
      var flist = [];
      for (var i = 0; i < this.friendList.length; i++) {
        if (startswith(this.friendList[i].name, filter)) {
          item = domCtr.toDom(string.substitute(this.filterItem, this.friendList[i]));
          domCtr.place(item, this.boxNode);
        }
      }


    },

    loadFriends: function(response) {
        if (response.length === 0) {
          this.friendList = [];

          domCtr.destroy(this.searchNode);
          this.boxNode.innerHTML = [
            '<p>None of your friends has joined MyWishGuru yet.</p>',
            '<div id="invite_more" class="mwgButton center">Invite</div>'].join('');

          on(dom.byId('invite_more'), 'click', function(){
            FB.ui({
              method: 'apprequests',
              message: 'Join me on MyWishGuru'
            });
          });

        }
        else {
          this.friendList = response;

          for (var i = 0; i < this.friendList.length; i++) {
            if (this.friendList[i].hasWish) {
              this.friendList[i].disabled='';
              this.friendList[i].imgdisabled='';
              this.friendList[i].labeldisabled='';
              this.friendList[i].title = '';
            }
            else {
              this.friendList[i].disabled='disabled';
              this.friendList[i].imgdisabled='class="disabled"';
              this.friendList[i].labeldisabled='class="disabled"';
              this.friendList[i].title = 'title="This user has not created any wishes"';
            }
          }


          this.showFriends(this.searchNode.value);

          on(this.searchNode, 'keyup', lang.hitch(this, function(evt) {
            this.showFriends(this.searchNode.value);
          }));
        }
    }

  });

  return FilterDialog;
});



