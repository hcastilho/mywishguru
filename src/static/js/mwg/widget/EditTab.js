define([
    'dojo/_base/Deferred',
    'dojo/_base/declare',
    'dojo/_base/lang',
    'dojo/dom-style',
    'dojo/dom-class',
    'dojo/on',
    'dojo/topic',
    'mwg/widget/BaseTab',
    'mwg/widget/TwoButtonDialog'
  ], function(
    Deferred,
    declare,
    lang,
    domStyle,
    domClass,
    on,
    topic,
    BaseTab,
    TwoButtonDialog) {

  return declare('mwg/widget/BaseEditTab', [BaseTab], {
    baseClass: 'baseEditTab',

    _setStatusAttr: function(value) {
      this._set('status', value);

      if (value == 'fulfilled') {
        this.fulfilledNode.title = 'Wish is fulfilled';
        domClass.remove(this.fulfilledNode, 'ribbon_default');
        domClass.add(this.fulfilledNode, 'ribbon_done');
      }
      else {
        this.fulfilledNode.title = 'Wish is not fulfilled';
        domClass.remove(this.fulfilledNode, 'ribbon_done');
        domClass.add(this.fulfilledNode, 'ribbon_default');
      }
    },

    postCreate: function() {
      this.inherited(arguments);

      this.model.watch('title', lang.hitch(this, function(name, oldValue, value) {
        this.set('tabTitle', value);
      }));
      this.model.watch('logo', lang.hitch(this, function(name, oldValue, value) {
        this.set('logo', value);
      }));
      this.model.watch('logoTop', lang.hitch(this, function(name, oldValue, value) {
        this.set('logoTop', value);
        //domStyle.set(this.logoNode, 'top', value+'%');
      }));
      this.model.watch('logoLeft', lang.hitch(this, function(name, oldValue, value) {
        this.set('logoLeft', value);
        //domStyle.set(this.logoNode, 'left', value+'%');
      }));

      on(this.deleteButton, 'click', lang.hitch(this, 'deleteModel'));

      on(this.thumbNode, 'click', lang.hitch(this, function() {
        topic.publish('stepController.selectChild', this.model);
      }));
      on(this.numberNode, 'click', lang.hitch(this, function() {
        topic.publish('stepController.selectChild', this.model);
      }));

      if ('index' in this.model) {
        this.model.watch('index', lang.hitch(this, function(name, oldValue, value) {
          this.set('index', value);
        }));
      }
    },

    deleteModel: function() {
      var title;
      if ('step_id' in this.model) {
        title = 'Delete Step';
      }
      else {
        title = 'Delete Wish';
      }
      var confDialog = new TwoButtonDialog(
        {
          title: title,
          message: 'Are you sure?',
          confirmMsg: 'Delete',
          cancelMsg: 'Cancel'
        });
      Deferred.when(confDialog.show(), lang.hitch(this, function(response) {
        if (response) {
          topic.publish('stepController.removeChild', this.model);
        }
      }));
    }

  });
});
