define([
    'dijit/form/Select',
    'dijit/_WidgetsInTemplateMixin',
    'dojo/_base/lang',
    'dojo/_base/event',
    'dojo/query',
    'dojo/on',
    'dojo/topic',
    'dojo/store/JsonRest',
    'dojo/data/ObjectStore',
    'dojo/_base/declare',
    'dojo/dom-class',
    'dojo/dom-construct',
    'dojo/string',
    'dojo/text!./resources/WishEditTab.html',
    'mwg/widget/PageSelectDialog',
    'mwg/widget/WishTab',
    'mwg/widget/EditTab',
    'mwg/main'
  ], function(
    Select,
    _WidgetsInTemplateMixin,
    lang,
    event,
    query,
    on,
    topic,
    Store,
    ObjectStore,
    declare,
    domClass,
    domConstruct,
    string,
    template,
    PageSelectDialog,
    WishTab,
    EditTab,
    main) {

  return declare('mwg/widget/WishEditTab', [WishTab, EditTab, _WidgetsInTemplateMixin], {
    baseClass: 'wishTab wishEditTab',
    templateString: template,

    privacy: '',
    friendlist: '',

    friendlistStore: new ObjectStore({
      objectStore: new Store({
        target: '/rest/friendlist/',
        idProperty: 'list_id'
      }),
      labelProperty: 'name'
    }),

    _setAccountAttr: function(value) {
      if (window.mwg.account)
        return;
      this._set('account', value);
      domConstruct.empty(this.pageNode);
      if (value && 'id' in value) {
        domClass.remove(this.pageNode, 'hidden');
        domClass.remove(this.pageNode, 'no_page');
        var account_template = ['<div class="userbox">',
          '<ul>',
          '<li>',
          '<img class="userPicture" src="${picture}"/>',
          '</li>',
          '<li class="extrapad">',
          '${name}',
          '</li>',
          '<li>',
          '<div class="del"></div>',
          '</li>',
          '</ul>',
          '</div>'].join('');
        var item = domConstruct.toDom(string.substitute(account_template, value));
        var remove = query('.del', item)[0];
        on(remove, 'click', lang.hitch(this, function(evt) {
          event.stop(evt);
          this.set('account', {});
          this.model.set('account', {});
        }));
        domConstruct.place(item, this.pageNode);
      }
      else {
        domClass.remove(this.pageNode, 'hidden');
        domClass.add(this.pageNode, 'no_page');
        domConstruct.create('div',
            {'class': 'no_page',
              innerHTML: 'Add to a page'},
            this.pageNode);
      }
    },

    _setPrivacyAttr: function(value) {
      var oldvalue = this.privacy;
      //showSelectsPrivacy: function() {
      this.privacySelect.set('value', value);
      this.inherited(arguments);
      this._set('privacy', value);

      if (!window.mwg.account) {
        if (value == 'friendlist') {
          main.hasPermission('read_friendlists').then(lang.hitch(this, function(response) {
            if (response) {
              domClass.remove(this.friendlistNode, 'hidden');
            }
            else {
              main.addPermission('read_friendlists').then(lang.hitch(this, function(response) {
                if (response) {
                  this.createFriendlistSelect();
                  domClass.remove(this.friendlistNode, 'hidden');
                }
                else {
                  this.privacySelect.set('value', oldvalue);
                  this._set('privacy', oldvalue);
                }
              }));
            }
          }));
        }
        else {
          domClass.add(this.friendlistNode, 'hidden');
        }
        if (value == 'friends' || value == 'fof' || value == 'friendlist') {
          this.set('account', {});
          this.model.set('account', {});
        }
      }
    },

    buildRendering: function() {
      this.inherited(arguments);
      // Privacy Select
      var options;
      if (window.mwg.account) {
        options = [
                { label: '<span class="onlyme">Only Me</span>', value: 'invisible'},
                { label: '<span class="public">Public</span>', value: 'public', selected: true }
            ];
      }
      else {
        options = [
                { label: '<span class="onlyme">Only Me</span>', value: 'invisible'},
                { label: '<span class="jfriends">Friends</span>', value: 'friends'},
                { label: '<span class="ff">Friends of Friends</span>', value: 'fof'},
                { label: '<span class="public">Public</span>', value: 'public', selected: true },
                { label: '<span class="flist">Friendlist</span>', value: 'friendlist'}
            ];
      }

      this.privacySelect = new Select({
            name: "privacy",
            options: options
        }, this.privacyNode);
    },

    postCreate: function() {
      this.inherited(arguments);
      this.wishTabPostCreate();

      this.set('privacy', this.model.privacy);
      this.set('friendlist', this.model.friendlist);

      if (!window.mwg.account) {
        domClass.remove(this.pageNode, 'hidden');
        domClass.add(this.pageNode, 'clickable');
      }

      if (this.model.wish_id) {
        this.makeWishNode.innerHTML = 'Update Wish';
      }
      // Create Wish
      on(this.makeWishNode, 'click', function() {
        topic.publish('stepController.publishChildren');
      });

      main.getPermissions().then(lang.hitch(this, function(response) {
        if ('read_friendlists' in response) {
          if (!window.mwg.account) {
            this.createFriendlistSelect();
          }
        }
      }));

      on(this.privacySelect, 'change', lang.hitch(this, function() {
        this.set('privacy', this.privacySelect.value);
        this.model.set('privacy', this.privacySelect.value);
      }));
      on(this.friendlistSelect, 'change', lang.hitch(this, function() {
        this.set('friendlist', this.friendlistSelect.value);
        this.model.set('friendlist', this.friendlistSelect.value);
      }));

      var args = "";
      if (this.model.wish_id) {
        args = "wish_id=" + this.model.wish_id;
      }
      main.accountStore.query(args).then(lang.hitch(this, function(response) {
        window.mwg.accounts = response;
      }));
      on(this.pageNode, 'click', lang.hitch(this, function() {
        if (window.mwg.pageSelectDialog === undefined) {
          window.mwg.pageSelectDialog = new PageSelectDialog();
        }
        window.mwg.pageSelectDialog.show().then(lang.hitch(this, function(response) {
          this.set('account', response);
          this.model.set('account', response);
          if (this.model.privacy != 'public' && this.model.privacy !='invisible') {
            this.privacySelect.set('value', 'public');
          }
        }));
      }));


    },

    createFriendlistSelect: function() {
      this.select = new Select({
          name: 'friendlist',
          store: this.friendlistStore,
          labelAttr: 'name'
        }, this.friendlistSelect);
      this.select.startup();
    }

  });
});
