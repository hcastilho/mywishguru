define([
    'dojo/_base/declare',
    'dojo/_base/lang',
    'dojo/on',
    'dojo/dom-class',
    'dojo/dom-construct',
    'dojo/topic',
    'dijit/_WidgetBase',
    'dijit/_TemplatedMixin',
    'dojo/text!./resources/StepAddTab.html'
  ], function(
    declare,
    lang,
    on,
    domClass,
    domConstruct,
    topic,
    _WidgetBase,
    _TemplatedMixin,
    template) {

  return declare('mwg/widget/StepEditTab', [_WidgetBase, _TemplatedMixin], {
    baseClass: 'stepTab',
    templateString: template,

    type: 1,

    postCreate: function() {
      if (this.type == 2) {
        domClass.remove(this.clickNode, 'addstep');
        domClass.add(this.clickNode, 'addstep2');
      }
      else if (this.type == 3) {
        domClass.remove(this.clickNode, 'addstep');
        domClass.add(this.clickNode, 'addstep3');
      }

      on(this.clickNode, 'click', lang.hitch(this, function() {
        topic.publish('stepController.addStep');
        //if (this.type !== 1) {
        //  domConstruct.destroy(this.domNode);
        //}
      }));
    }

  });
});
