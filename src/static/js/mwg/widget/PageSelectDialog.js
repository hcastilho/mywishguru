define([
  'mwg/main',
  'mwg/widget/PagesDialog',
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/_base/Deferred',
  'dojo/query',
  'dojo/topic',
  'dojo/on',
  'dojo/dom',
  'dojo/dom-construct',
  'dojo/string',
  'dijit/Dialog',
  'dojo/text!./resources/filter_dialog.html',
  'dijit/_WidgetBase',
  'dijit/_TemplatedMixin',
  'dijit/_WidgetsInTemplateMixin'
], function(
  main,
  PagesDialog,
  declare,
  lang,
  Deferred,
  query,
  topic,
  on,
  dom,
  domConstruct,
  string,
  Dialog,
  FilterDialogTemplate,
  _WidgetBase,
  _TemplatedMixin,
  _WidgetsInTemplateMixin
  ) {

  return declare('mwg.widget.PageSelectDialog', [PagesDialog], {

    filterItem: ['<div class="userbox clickable">',
      '<ul>',
      '<li>',
      '<img class="userPicture" src="${picture}"/>',
      '</li>',
      '<li class="extrapad">',
      '${name}',
      '</li>',
      '</ul>',
      '</div>'].join(''),

    show: function() {
      this.def = new Deferred();
      this.dialogNode.show();
      return this.def;
    },

    showFilter: function(filter) {
      this.boxNode.innerHTML = '';
      filter = typeof filter !== 'undefined' ? filter : '';
      var item;
      var flist = [];
      for (var i = 0; i < this.accounts.length; i++) {
        if (main.startsWith(this.accounts[i].name, filter)) {
          this.place_item(this.accounts[i]);
        }
      }
    },

    place_item: function(args) {
      item = domConstruct.toDom(string.substitute(this.filterItem, args));
      domConstruct.place(item, this.boxNode);
      var on_click = this.make_handler(args);
      on(item, 'click', on_click);
    },

    make_handler: function(account) {
      return lang.hitch(this, function() {
        this.def.resolve(account);
        this.hide();
      });
    },

    loadAccounts: function(response) {
      this.accounts = response;
      this.showFilter(this.searchNode.value);
      on(this.searchNode, 'keyup', lang.hitch(this, function(evt) {
        this.showFilter(this.searchNode.value);
      }));
    }


  });

  
});



