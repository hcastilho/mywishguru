define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/store/JsonRest',
  'dojo/data/ObjectStore',
  'dojo/on',
  'dojo/dom',
  'dojo/dom-construct',
  'dojo/dom-class',
  'dijit/form/Select',
  'mwg/main',
  'mwg/widget/BaseEdit',
  'dojo/text!./resources/StepEdit.html'
], function(
  declare,
  lang,
  Store,
  ObjectStore,
  on,
  dom,
  domCtr,
  domClass,
  Select,
  main,
  BaseEdit,
  template) {

  return declare('mwg.widget.WishEdit', [BaseEdit], {

    baseClass: 'baseEdit wishEdit',
    templateString: template,

    // From Model
    privacy: '',
    friendlist: '',

    friendlistStore: new ObjectStore({
      objectStore: new Store({
        target: '/rest/friendlist/',
        idProperty: 'list_id'
      }),
      labelProperty: 'name'
    }),

    //_setPrivacyAttr: function(value) {
    //  var oldvalue = this.privacy;
    //  //showSelectsPrivacy: function() {
    //  //this.privacySelect.set('value', value);
    //  this.inherited(arguments);
    //  this._set('privacy', value);
    //  if (value == 'friendlist') {
    //    main.hasPermission('read_friendlists').then(lang.hitch(this, function(response) {
    //      if (response) {
    //        domClass.remove(this.friendlistNode, 'hidden');
    //      }
    //      else {
    //        main.addPermission('read_friendlists').then(lang.hitch(this, function(response) {
    //          if (response) {
    //            this.createFriendlistSelect();
    //            domClass.remove(this.friendlistNode, 'hidden');
    //          }
    //          else {
    //            //this.privacySelect.set('value', oldvalue);
    //            this._set('privacy', oldvalue);
    //          }
    //        }));
    //      }
    //    }));
    //  }
    //  else {
    //    domClass.add(this.friendlistNode, 'hidden');
    //  }
    //},

    postCreate: function() {
      this.inherited(arguments);

      this.set('privacy', this.model.privacy);
      this.set('friendlist', this.model.friendlist);

      //main.getPermissions().then(lang.hitch(this, function(response) {
      //  if ('read_friendlists' in response) {
      //    this.createFriendlistSelect();
      //  }
      //}));

      //on(this.privacySelect, 'change', lang.hitch(this, function() {
      //  this.set('privacy', this.privacySelect.value);
      //  this.model.set('privacy', this.privacySelect.value);
      //}));
      //on(this.friendlistSelect, 'change', lang.hitch(this, function() {
      //  this.set('friendlist', this.friendlistSelect.value);
      //  this.model.set('friendlist', this.friendlistSelect.value);
      //}));
    },

    createFriendlistSelect: function() {
      this.select = new Select({
          name: 'friendlist',
          store: this.friendlistStore,
          labelAttr: 'name'
        }, this.friendlistSelect);
      this.select.startup();
    },

    publishModel: function() {
      return this.inherited(arguments);
    },

    saveModel: function() {
      return this.inherited(arguments);
    },

    cancelModel: function() {
      this.inherited(arguments);
      //this.privacySelect.value = this.model.privacy;
      //if (this.model.privacy == 'friendlist') {
      //  this.friendlistSelect.value = this.model.friendlist;
      //}
    }
  });

});


