define([
    'mwg/main',
    'mwg/widget/UserTooltip',
    'dojo/_base/declare',
    'dojo/_base/lang',
    'dojo/dom',
    'dojo/dom-class',
    'dojo/dom-construct',
    'dojo/string',
    'dojo/date',
    'dojo/date/locale',
    'dojo/query',
    'dojo/on',
    'dojo/store/JsonRest',
    'dojo/cookie',
    'dojo/parser',
    'dijit/registry',
    'dijit/form/Textarea',
    'dijit/_WidgetBase',
    'dijit/_TemplatedMixin',
    'dijit/_WidgetsInTemplateMixin',
    'dojo/text!./resources/FbComment.html'
], function(
    main,
    UserTooltip,
    declare,
    lang,
    dom,
    domClass,
    domConstruct,
    string,
    date,
    locale,
    query,
    on,
    JsonRestStore,
    cookie,
    parser,
    registry,
    Textarea,
    _WidgetBase,
    _TemplatedMixin,
    _WidgetsInTemplateMixin,
    template) {

    return declare('mwg.widget.FbPost', [_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin], {

      baseClass: 'fbComment',
      templateString: template,

      id: '',
      message: '',
      created_time: '',
      like_count: 0,
      user_likes: false,

      _setUser_likesAttr: function(value) {
        this._set('user_likes', value);
        this.setLikeNode();
      },
      _setLike_countAttr: function(value) {
        this._set('like_count', value);
        this.setLikeCountNode();
      },

      constructor: function(args) {
        this.from = {id: '', name: ''};
        declare.safeMixin(this, args);

        this.message = this.message.replace(/\n|\r/g, '<br />');
        this.long_created = locale.format(
          this.created_time,
          {selector: 'date', datePattern: "EEEE, dd 'of' MMMM 'at' HH:mm"});
        //this.abbr_created = locale.format(
        //  this.created_time,
        //  {selector: 'date', datePattern: 'dd/MM HH:mm'});
        this.abbr_created = main.timeDifference(new Date().getTime(),
            this.created_time.getTime());
      },

      postCreate: function() {

        //if(!('likes' in this)) {
        //  this.set('likes', {count: 0, data: []});
        //}

        // User picture
        main.getObjectData(this.from.id).then(lang.hitch(this, function(response) {
          this.userPicture.src = response.picture.data.url;
        }));
        var userTooltip = new UserTooltip({'id': this.from.id});
        var node = this.userNameNode;
        on(this.userNameNode, 'mouseover', lang.hitch(this, function() {
          userTooltip.open(node);
        }));
        var pictureNode = this.userPictureNode;
        on(this.userPictureNode, 'mouseover', lang.hitch(this, function() {
          userTooltip.open(pictureNode);
        }));

      },

      setLikeCountNode: function(value) {
        domConstruct.empty(this.likeCountNode);
        domClass.add(this.middotNode, 'hidden');

        if(this.like_count === 0) return;

        var verb = ' Like';
        if (this.like_count > 1) {
          verb = verb + 's';
        }
        this.likeCountNode.innerHTML = this.like_count + verb;
        domClass.remove(this.middotNode, 'hidden');

      },

      setLikeNode: function(value) {
        domConstruct.empty(this.likeNode);

        if (this.user_likes) {
          //this.likeNode.innerHTML = 'Unlike';
          //this.likeNode.innerHTML = 'You Like This';
          this.likeNode.innerHTML = 'Unlike';
          this.likeNode.title = "Facebook doesn't allow unliking through the application";
          domClass.remove(this.likeNode,'pointer');
        }
        else {
          this.likeNode.innerHTML = 'Like';
          on.once(this.likeNode, 'click', lang.hitch(this, function(evt) {
            //if (this.likeNode.innerHTML == 'Like') {
              main.addPermission('publish_actions').then(lang.hitch(this, function() {
                var params = {};
                if (window.mwg.account) params.access_token = mwg.account.access_token;
                FB.api(this.id + '/likes',
                  'post',
                  params,
                  lang.hitch(this, function(response) {
                    //this.likes.count += 1;
                    //this.likes.data.push({
                    //  'id': window.mwg.config.userId,
                    //  'name': window.mwg.config.userName
                    //});
                    //this.set('likes', this.likes);
                    this.set('user_likes', true);
                    this.set('like_count', (this.like_count + 1));
                    domClass.remove(this.likeNode,'pointer');
                }));
              }));
            //}
            //else {
            //  // Facebook isn't allowing deleting likes
            //  FB.api(this.id + '/likes', 'delete', lang.hitch(this, function(response) {

            //    this.likes.count -= 1;
            //    var verb = ' Like';
            //    if (this.likes.count > 1) {
            //      verb = verb + 's';
            //    }
            //    this.likeCountNode.innerHTML = this.likes.count + verb;
            //    this.likeNode.innerHTML = 'Like';
            //  }));
            //}
          }));
        }
      }

      });
    });
