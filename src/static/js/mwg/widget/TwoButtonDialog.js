define([
    'dojo/_base/declare',
    'dojo/on',
    'dojo/_base/lang',
    'dojo/_base/Deferred',
    'dijit/Dialog',
    'dijit/_WidgetBase',
    'dijit/_TemplatedMixin',
    'dijit/_WidgetsInTemplateMixin',
    'dojo/text!./resources/TwoButtonDialog.html',
    'dijit/form/Button'
], function(
    declare,
    on,
    lang,
    Deferred,
    Dialog,
    _WidgetBase,
    _TemplatedMixin,
    _WidgetsInTemplateMixin,
    template,
    Button) {

        return declare('mwg.widget.TwoButtonDialog', [_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin], {
            title: '',
            message: 'Are you sure?',
            confirmMsg: 'Yes',
            cancelMsg: 'Cancel',
            templateString: template,
            widgetsInTemplate: true,

            postCreate: function() {
                on(this.yesBtn, 'click', lang.hitch(this, 'yes'));
                on(this.noBtn, 'click', lang.hitch(this, 'no'));
                on(this.dialogNode, 'cancel', lang.hitch(this, 'no'));
            },

            show: function() {
                this.dialogNode.show();
                this.deferred = new Deferred();
                return this.deferred;
            },

            yes: function() {
                this.dialogNode.hide();
                this.deferred.resolve(true);
                this.destroy();
            },

            no: function() {
                this.dialogNode.hide();
                this.deferred.resolve(false);
                this.destroy();
            }
        });
});
