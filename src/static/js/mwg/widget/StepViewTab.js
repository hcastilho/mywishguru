define([
    'mwg/widget/ViewTab',
    'mwg/widget/StepTab',
    'dojo/_base/declare',
    'dojo/on',
    'dojo/topic',
    'dojo/_base/lang',
    'dojo/dom-class',
    'dojo/text!./resources/StepViewTab.html'
  ], function(
    ViewTab,
    StepTab,
    declare,
    on,
    topic,
    lang,
    domClass,
    template) {

  return declare('mwg/widget/StepViewTab', [StepTab, ViewTab], {
    baseClass: 'stepTab stepViewTab',
    templateString: template,

    signal: null,
    status: '',

    _setStatusAttr: function(value) {
      this._set('status', value);
      if (this.model.owner) {
        if (value === 'fulfilled') {
          this.fulfilledNode.title = 'Unfulfill step';
          domClass.remove(this.fulfilledNode, 'default');
          domClass.add(this.fulfilledNode, 'done');
        }
        else {
          this.fulfilledNode.title = 'Fulfill step';
          domClass.remove(this.fulfilledNode, 'done');
          domClass.add(this.fulfilledNode, 'default');
        }
      }
      else {
        if (value === 'fulfilled') {
          this.fulfilledNode.title = 'Step fulfilled';
          domClass.remove(this.fulfilledNode, 'default_view');
          domClass.add(this.fulfilledNode, 'done_view');
        }
        else {
          this.fulfilledNode.title = "Step not fulfilled";
          domClass.remove(this.fulfilledNode, 'done_view');
          domClass.add(this.fulfilledNode, 'default_view');
        }
      }
    },

    postCreate: function() {
      this.inherited(arguments);
      this.stepTabPostCreate();

      this.set('status', this.model.status);

      if (this.model.owner) {
        domClass.add(this.fulfilledNode, 'enabled');

        on(this.fulfilledNode, 'click', lang.hitch(this, function() {
          this.model.toggleFulfill();
        }));

        if (this.index !== 0) {
          this.model.watch('status', lang.hitch(this, function(name, oldValue, value) {
            this.set('status', value);
          }));
        }
      }

      this.thumbNode.href = '/wish/' + this.model.wish_id + '/step/' + this.model.index;
      this.numberLink.href = '/wish/' + this.model.wish_id + '/step/' + this.model.index;

    }
  });
});
