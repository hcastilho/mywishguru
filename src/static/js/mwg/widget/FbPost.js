define([
    'mwg/main',
    'mwg/widget/FbComment',
    'mwg/widget/UserTooltip',
    'mwg/widget/UserListTooltip',
    'dojo/_base/declare',
    'dojo/_base/lang',
    'dojo/dom',
    'dojo/dom-class',
    'dojo/dom-construct',
    'dojo/string',
    'dojo/date',
    'dojo/date/locale',
    'dojo/query',
    'dojo/on',
    'dojo/store/JsonRest',
    'dojo/cookie',
    'dojo/parser',
    'dijit/registry',
    'dijit/form/Textarea',
    'dijit/_WidgetBase',
    'dijit/_TemplatedMixin',
    'dijit/_WidgetsInTemplateMixin',
    'dojo/text!./resources/FbPost.html'
    ], function(
        main,
        FbComment,
        UserTooltip,
        UserListTooltip,
        declare,
        lang,
        dom,
        domClass,
        domConstruct,
        string,
        date,
        locale,
        query,
        on,
        JsonRestStore,
        cookie,
        parser,
        registry,
        Textarea,
        _WidgetBase,
        _TemplatedMixin,
        _WidgetsInTemplateMixin,
        template
        ) {

        return declare('mwg.widget.FbPost', [_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin], {
          baseClass: 'fbPost',
          templateString: template,

          wish_id: '',
          step_id: '',
          // FB Post Fields
          caption: '',
          created_time: '',
          description: '',
          icon: '',
          id: '',
          link: '',
          message: '',
          name: '',
          picture: '',
          type: '',
          updated_time:'',

          allow_thanking: false,

          userLikes: false,
          userIdx: -1,

          can_comment: true,
          can_like: true,

          _setCan_commentAttr: function(value) {
            this._set('can_comment', value);
            if (value) {
              domClass.remove(this.new_comment_node, 'hidden');
            }
            else {
              domClass.add(this.new_comment_node, 'hidden');
            }
          },

          _setCan_likeAttr: function(value) {
            this._set('can_like', value);
            if (value) {
              domClass.remove(this.likeNode, 'hidden');
            }
            else {
              domClass.add(this.likeNode, 'hidden');
            }
          },

          _setLikesAttr: function(value) {
            this._set('likes', value);

            this.userLikes = false;
            this.userLikesIdx = -1;
            for (var i=0; i < this.likes.data.length; i++) {
              if (this.likes.data[i].id == window.mwg.config.userId) {
                this.userLikes = true;
                this.userLikesIdx = i;
                break;
              }
            }
            this.setLikeCountNode();
            this.setLikeNode();
          },

          constructor: function(args) {
            // FB Post Objects
            this.actions = [];
            this.application = {};
            this.comments = {data: []};
            this.from = {id: '', name: ''};
            this.privacy = {};

            declare.safeMixin(this, args);
            //this.post_link = this.actions[0].link;
            var split_id = this.id.split('_');
            this.post_link = ['https://www.facebook.com/',
              split_id[0],
              '/posts/',
              split_id[1]].join('');

            this.message = this.message.replace(/\n|\r/g, '<br />');
            this.long_created = locale.format(
                this.created_time,
                {selector: 'date', datePattern: "EEEE, dd 'of' MMMM 'at' HH:mm"});
            //this.abbr_created = locale.format(
            //    this.created_time,
            //    {selector: 'date', datePattern: 'dd/MM HH:mm'});
            this.abbr_created = main.timeDifference(new Date().getTime(),
                this.created_time.getTime());

          },

          postCreate: function() {
            var i;
            //
            if(!('likes' in this)) {
              this.set('likes', {count: 0, data: []});
            }

            // Post picture
            main.getObjectData(this.from.id).then(lang.hitch(this, function(response) {
              this.userPictureNode.src = response.picture.data.url;
            }));

            // Can comment/can like
            if ('actions' in this) {
              var comment_present = false, like_present = false;
              for (i=0; i < this.actions.length; i++){
                if (this.actions[i].name == 'Comment') comment_present = true;
                if (this.actions[i].name == 'Like') like_present = true;
              }
              if (comment_present === false) this.set('can_comment', false);
              if (like_present === false) this.set('can_like', false);
            }
            else {
              this.set('can_comment', false);
              this.set('can_like', false);
            }

            // New Comment picture
            if (window.mwg.account) {
              main.getObjectData(window.mwg.account.id).then(lang.hitch(this, function(response) {
                this.new_comment_picture.src = response.picture.data.url;
              }));
            }
            else {
              main.getObjectData(window.mwg.config.userId).then(lang.hitch(this, function(response) {
                this.new_comment_picture.src = response.picture.data.url;
              }));
            }

            // User card
            var node = this.userNameNode;
            var userTooltip = new UserTooltip({'id': this.from.id});
            on(node, 'mouseover', function() {
              userTooltip.open(node);
            });
            var pictureNode = this.userPictureNode;
            on(pictureNode, 'mouseover', function() {
              userTooltip.open(pictureNode);
            });

            // Comments
            for (i = 0; i < this.comments.data.length; i++) {
              new FbComment(this.comments.data[i]).placeAt(this.commentsNode);
            }

            // New Comment
            on(this.new_comment, 'keyup', lang.hitch(this,function(evt) {

              if (evt.keyCode == 13 && !evt.shiftKey) {
                var msg = this.new_comment.get('value');
                if (msg === '') return;
                var comment = {created_time: new Date(),
                  from: {id: window.mwg.config.userId, name: window.mwg.config.userName},
                  message: msg
                };
                if (window.mwg.account) {
                  comment.from.id = window.mwg.account.id;
                  comment.from.name = window.mwg.account.name;
                }

                new FbComment(comment).placeAt(this.commentsNode);
                this.new_comment.set('value', '');
                var params = {message: msg};
                if (window.mwg.account) params.access_token = mwg.account.access_token;
                FB.api('/'+ this.id +'/comments',
                  'post',
                  params,
                  lang.hitch(this, function(response) {
                    if (typeof response === 'undefined') return;
                    if ('error' in response) {
                      console.warn('8ae1ee9f-c736-4188-bef1-fb036c327661');
                      console.warn(response);
                    }
                    else {
                      var args = {
                        comment_id: response.commentID,
                        wish_id: this.wish_id
                      };
                      if (this.step_id)
                        args.step_id = this.step_id;
                      main.commentStore.put(args);
                    }
                }));
              }
            }));
          },

          setLikeCountNode: function(value) {
            domConstruct.empty(this.likeCountNode);

            var node, userTooltip, userListTooltip;
            if (this.userLikes) {
              if (this.likes.count == 1) {
                domConstruct.create('span',
                    {innerHTML: 'You like this.'},
                    this.likeCountNode);
              }
              else if (this.likes.count == 2) {
                domConstruct.create('span',
                    {innerHTML: 'You and '},
                    this.likeCountNode);

                for (i=0; i < this.likes.data.length; i++) {
                  if (this.likes.data[i].id != window.mwg.config.userId) {
                    break;
                  }
                }
                node = domConstruct.create('a',
                    {
                      innerHTML: this.likes.data[i].name,
                      href: 'http://www.facebook.com/' + this.likes.data[i].id,
                      target: '_blank'
                    },
                    this.likeCountNode);
                userTooltip = new UserTooltip({'id': this.likes.data[i].id});
                on(node, 'mouseover', function() {
                  userTooltip.open(node);
                });
                domConstruct.create('span',
                    {innerHTML: ' like this.'},
                    this.likeCountNode);
              }
              else if (this.likes.count > 2) {
                domConstruct.create('span',
                    {innerHTML: 'You and '},
                    this.likeCountNode);
                node = domConstruct.create('a',
                    {innerHTML: (this.likes.count - 1) + ' others',
                    href: 'https://www.facebook.com/browse/likes?id=' + this.id.split('_')[1],
                    target: '_blank'},
                    this.likeCountNode);
                var users = this.likes.data.slice(0);
                users = users.splice(this.userIdx, 1);
                userListTooltip = new UserListTooltip({users: this.likes.data});
                on(node, 'mouseover', function() {
                  userListTooltip.open(node);
                });
                domConstruct.create('span',
                    {innerHTML: ' like this.'},
                    this.likeCountNode);
              }
            }
            else {
              if (this.likes.count == 1) {
                node = domConstruct.create('a',
                    {
                      innerHTML: this.likes.data[0].name,
                      href: 'http://www.facebook.com/' + this.likes.data[0].id,
                      target: '_blank'
                    },
                    this.likeCountNode);
                userTooltip = new UserTooltip({'id': this.likes.data[0].id});
                on(node, 'mouseover', function() {
                  userTooltip.open(node);
                });
                domConstruct.create('span',
                    {innerHTML: ' likes this.'},
                    this.likeCountNode);
              }
              else if (this.likes.count > 1){
                node = domConstruct.create('a',
                    {innerHTML: this.likes.count + ' people',
                    href: 'https://www.facebook.com/browse/likes?id=' + this.id.split('_')[1],
                    target: '_blank'},
                    this.likeCountNode);
                userListTooltip = UserListTooltip({users: this.likes.data});
                on(node, 'mouseover', function() {
                  userListTooltip.open(node);
                });
                domConstruct.create('span',
                    {innerHTML: ' like this.'},
                    this.likeCountNode);
              }
            }
          },

          setLikeNode: function(value) {
            domConstruct.empty(this.likeNode);

            if (this.userLikes) {
              //this.likeNode.innerHTML = 'Unlike';
              //this.likeNode.innerHTML = 'You Like This';
              this.likeNode.innerHTML = 'Unlike';
              this.likeNode.title = "Facebook doesn't allow unliking through the application";
              domClass.remove(this.likeNode,'pointer');
            }
            else {
              this.likeNode.innerHTML = 'Like';
              on.once(this.likeNode, 'click', lang.hitch(this, function(evt) {
                //if (this.likeNode.innerHTML == 'Like') {
                  main.addPermission('publish_actions').then(lang.hitch(this, function() {
                    var params = {};
                    if (window.mwg.account) params.access_token = mwg.account.access_token;
                    FB.api(this.id + '/likes',
                      'post',
                      params,
                      lang.hitch(this, function(response) {
                        this.likes.count += 1;
                        this.likes.data.push({
                          'id': window.mwg.config.userId,
                          'name': window.mwg.config.userName
                        });
                        this.set('likes', this.likes);
                        domClass.remove(this.likeNode,'pointer');
                    }));
                  }));
                //}
                //else {
                //  // Facebook isn't allowing deleting likes
                //  FB.api(this.id + '/likes', 'delete', lang.hitch(this, function(response) {

                //    this.likes.count -= 1;
                //    var verb = ' Like';
                //    if (this.likes.count > 1) {
                //      verb = verb + 's';
                //    }
                //    this.likeCountNode.innerHTML = this.likes.count + verb;
                //    this.likeNode.innerHTML = 'Like';
                //  }));
                //}
              }));
            }
          }

          });
        });
