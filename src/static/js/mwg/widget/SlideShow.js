define([
    'dojo/_base/declare',
    'dojo/_base/lang',
    'dojo/_base/fx',
    'dojo/on',
    'dojo/query',
    'dojo/dom',
    'dojo/dom-class',
    'dojo/dom-attr',
    'dojo/dom-style',
    'dojo/dom-geometry',
    'dojo/dom-construct',
    'dojo/text!./resources/SlideShow.html',
    'dojox/widget/Rotator',
    'dojox/widget/rotator/Pan',
    'dojox/widget/rotator/Slide',
    'dojox/widget/rotator/Wipe',
    'mwg/main',
    'mwg/widget/WishModel',
    'mwg/widget/WishCard',
    'dijit/_WidgetBase',
    'dijit/_TemplatedMixin',
    'dijit/_WidgetsInTemplateMixin'
  ], function(
    declare,
    lang,
    fx,
    on,
    query,
    dom,
    domClass,
    domAttr,
    domStyle,
    domGeom,
    domConstruct,
    template,
    Rotator,
    Pan,
    Slide,
    Wipe,
    main,
    WishModel,
    WishCard,
    _WidgetBase,
    _TemplatedMixin,
    _WidgetsInTemplateMixin) {

  return declare('mwg.widget.TakeTour', [_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin], {
    baseClass: 'slideshowWidget',
    templateString: template,

    slideShow: null,

    setSlideShow: function() {
      if (this.slideShow) {
        clearInterval(this.slideShow);
      }
      this.slideShow = setInterval(lang.hitch(this, function() {
        this.slideShowNode.next();
      }), 7000);
    },

    show: function() {
      fx.fadeIn({node: this.domNode}).play();
    },

    postCreate: function() {
      main.wishStore.query(
          {type: 'random_public'},
          {count: 10}).then(lang.hitch(this, function (response) {
        var i, wishModel, wishCard, currentPane, rotator;
        for (i=0; i<response.length; i++) {
          if (i%3 === 0) {
            currentPane = domConstruct.create('div', {'class': 'slide'}, this.slideShowNode);
          }
          wishModel = new WishModel(response[i]);
          wishCard = new WishCard({
            limitHeight: true,
            model: wishModel});
          wishCard.placeAt(currentPane);
          fx.fadeIn({node: wishCard.domNode}).play();
        }
        rotator = new Rotator(
          {
            transition: "dojox.widget.rotator.panRight"
          },
          this.slideShowNode);
        this.slideShowNode = rotator;
        on(this.nextNode, 'click', lang.hitch(this, function() {
          rotator.next();
          this.setSlideShow();
        }));
        on(this.prevNode, 'click', lang.hitch(this, function() {
          rotator.prev();
          this.setSlideShow();
        }));
        this.setSlideShow();
        this.show();
      }));
      //rotator.go(1);
    }


  });
});
