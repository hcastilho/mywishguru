define([
  'mwg/main',
  'mwg/widget/FbPost',
  'mwg/widget/FbSharePage',
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/cache',
  'dojo/on',
  'dojo/query',
  'dojo/topic',
  'dojo/dom',
  'dojo/dom-construct',
  'dojo/dom-class',
  'dojo/dom-style',
  'dojo/io-query',
  'dijit/_WidgetBase',
  'dijit/_TemplatedMixin',
  'dojo/text!./resources/StepView.html'
], function (
  main,
  FbPost,
  FbSharePage,
  declare,
  lang,
  cache,
  on,
  query,
  topic,
  dom,
  domConstruct,
  domClass,
  domStyle,
  ioQuery,
  _WidgetBase,
  _TemplatedMixin,
  template) {

  return declare('mwg/widget/StepView',
      [_WidgetBase,
      _TemplatedMixin], {

    baseClass: 'stepView',
    templateString: template,
    model: null,

    // From model
    owner: false,
    user: null,
    title: '',
    desc: '',
    logo: '',
    logoTop: 0,
    logoLeft: 0,
    userpic: '',
    username: '',
    userid: '',

    _setTitleAttr: function(value) {
      this._set('title', value);
    },

    _setDescAttr: function(value) {
      this._set('desc', value);
      var desc = main.replaceURLWithHTMLLinks(value);
      this.descNode.innerHTML = desc.replace(/\n/g, '<br />');
    },

    _setLogoAttr: function(value) {
      this._set('logo', value);
      this.logoNode.src = value;
    },
    _setLogoTopAttr: function(value) {
      this._set('logoTop', value);
      value = value * this.logoNode.offsetHeight;
      domStyle.set(this.logoNode, 'top', value+'px');
    },
    _setLogoLeftAttr: function(value) {
      this._set('logoLeft', value);
      value = value * this.logoNode.offsetWidth;
      domStyle.set(this.logoNode, 'left', value+'px');
    },

    _setUserpicAttr: function(value) {
      this._set('userpic', value);
      this.userpicNode.src = value;
    },

    _setUsernameAttr: function(value) {
      this._set('username', value);
      this.usernameNode.innerHTML = value;
    },

    _setUser_linkAttr: function(value) {
      this._set('user_link', value);
      this.userLinkPicNode.href = value;
      this.userLinkNameNode.href = value;
    },

    _setOwnerAttr: function(value) {
      this._set('owner', value);
    },

    postCreate: function() {
      this.set('owner', this.model.owner);
      this.set('title', this.model.title);
      this.set('desc', this.model.desc);
      this.set('userpic', this.model.user.picture);
      this.set('username', this.model.user.first_name);
      this.set('user_link', this.model.user.url);
      this.set('logo', this.model.logo);
      this.set('logoTop', this.model.logoTop);
      this.set('logoLeft', this.model.logoLeft);
      if (this.logo === '') {
        this.set('logo', mwg.config.defaultImage);
      }
      on(this.logoNode, 'load', lang.hitch(this, function() {
        domStyle.set(this.logoNode, 'height', '');
        domStyle.set(this.logoNode, 'width', '');
        var aspectRatio = (this.logoNode.offsetWidth / this.logoNode.offsetHeight);
        if (aspectRatio > 1.4987) {
          domStyle.set(this.logoNode, 'height', '100%');
        }
        else {
          domStyle.set(this.logoNode, 'width', '100%');
        }
        var value = this.logoTop;
        value = value * this.logoNode.offsetHeight;
        domStyle.set(this.logoNode, 'top', value+'px');
        value = this.logoLeft;
        value = value * this.logoNode.offsetWidth;
        domStyle.set(this.logoNode, 'left', value+'px');
      }));

      // Post
      if (this.model.privacy == 'invisible') {
        domConstruct.destroy(this.postFb);
        domConstruct.destroy(this.likeFb);
      }
      else {
        on(this.postFb, 'click', lang.hitch(this, 'publishFB'));
      }

      // First Post
      if (this.model.owner && this.model.privacy !== 'invisible') {
        if (!('step_id' in this.model) && this.model.posts.length === 0) {
          this.publishFB();
          //this.postFb.click();
        }
      }

      // CommentBox and Likes
      main.FbDeferred().then(lang.hitch(this, function() {
        var args = {
          wish_id: this.model.wish_id
        };
        if ('userId' in window.mwg.config && window.mwg.config.userId)
          args.user_id = window.mwg.config.userId;
        if ('step_id' in this.model)
          args.step_id = this.model.step_id;

        FB.Event.subscribe('comment.create', function(response) {
          args.comment_id = response.commentID;
          args.href = response.href;
          main.commentStore.put(args);
        });
      }));

      main.FbDeferred().then(lang.hitch(this, function() {
        var args = {wish_id: this.model.wish_id};
        if ('userId' in window.mwg.config && window.mwg.config.userId)
          args.user_id = window.mwg.config.userId;
        if ('step_id' in this.model)
          args.step_id = this.model.step_id;
        FB.Event.subscribe('edge.create', function(response) {
          args.href = response.href;
          main.likeStore.put(args);
        });
      }));


      // Facebook posts
      var pub = '/wish_id/' + this.model.wish_id;
      if ('step_id' in this.model) {
        pub = pub + '/step_id/' + this.model.step_id;
      }
      topic.subscribe('getPosts' + pub, lang.hitch(this, function (response) {
        for (var i=0; i<response.posts.length; i++) {
          response.posts[i].wish_id = this.model.wish_id;
          if ('step_id' in this.model)
            response.posts[i].step_id = this.model.step_id;
          new FbPost(response.posts[i]).placeAt(this.postsNode);
        }
      }));

      main.FbDeferred().then(lang.hitch(this, function() {
        main.hasPermission('read_stream').then(lang.hitch(this, function(response) {
          if (!response) {
            // Doesn't have permission to get posts
            this.postsNode.innerHTML = ['To see facebook posts',
            ' directly from MyWishGuru click <span class="addReadStream">here</span>'].join('');

            query('.addReadStream', this.postsNode).on('click', lang.hitch(this, function() {
              main.addPermission('read_stream').then(lang.hitch(this, function(response) {
                if (response) {
                  this.postsNode.innerHTML = '';
                  this.model.getPosts();
                }
              }));
            }));
          }
        }));
      }));

      var search = window.location.search;
      search = search.substring(1, search.length);
      var queryObj = ioQuery.queryToObject(search);
      if ('share' in queryObj) {
        publishFB();
      }

    },

    publishFB: function() {
      //var picture = this.model.small_logo;
      //if (picture === '') {
      //  picture = this.model.logo;
      //}
      var picture = this.model.logo;
      if (picture === '') {
        picture = mwg.config.defaultImage;
      }
      var obj = {
        method: 'feed',
        link: this.model.url,
        picture: picture,
        name: this.model.title + ' - Wished by ' + this.model.user.first_name,
        description: this.model.desc
      };
      var _this = this;
      if (window.mwg.account) {
        main.addPermission('publish_stream').then(lang.hitch(this, function() {
          var share = new FbSharePage();
          share.show().then(lang.hitch(this, function(response) {
            obj.message = response;
            main.postAsPage(obj).then(lang.hitch(this, function(response) {
              if (response && 'id' in response) {
                _this.model.addPost(response.id);
                main.getPost(response.id).then(lang.hitch(this, function(response) {
                  response.wish_id = this.model.wish_id;
                  if ('step_id' in this.model)
                    response.step_id = this.model.step_id;
                  new FbPost(response).placeAt(_this.postsNode, 'first');
                }));
              }
            }));
          }));
        }));
      }
      else {
        main.FbDeferred().then(lang.hitch(this, function() {
          FB.ui(obj, lang.hitch(this, function(response) {
            if (response && 'post_id' in response) {
              _this.model.addPost(response.post_id);
              main.getPost(response.post_id).then(lang.hitch(this, function(response) {
                response.wish_id = this.model.wish_id;
                if ('step_id' in this.model)
                  response.step_id = this.model.step_id;
                new FbPost(response).placeAt(_this.postsNode, 'first');
              }));
            }
          }));
        }));
      }
    }
  });
});


