define([
    'dojo/_base/declare',
    'dojo/on',
    'dojo/_base/lang',
    'dojo/_base/Deferred',
    'dijit/Dialog',
    'dijit/_WidgetBase',
    'dijit/_TemplatedMixin',
    'dijit/_WidgetsInTemplateMixin',
    'dojo/text!./resources/OneButtonDialog.html',
    'dijit/form/Button'
], function(
    declare,
    on,
    lang,
    Deferred,
    Dialog,
    _WidgetBase,
    _TemplatedMixin,
    _WidgetsInTemplateMixin,
    template,
    Button) {

        return declare('mwg.widget.OneButtonDialog', [_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin], {
            title: '',
            message: 'Are you sure?',
            confirmMsg: 'Ok',
            templateString: template,
            widgetsInTemplate: true,

            postCreate: function() {
                on(this.OkBtn, 'click', lang.hitch(this, 'ok'));
                on(this.dialogNode, 'cancel', lang.hitch(this, 'ok'));
            },

            show: function() {
                this.dialogNode.show();
                this.deferred = new Deferred();
                return this.deferred;
            },

            ok: function() {
                this.dialogNode.hide();
                this.deferred.resolve(true);
                this.destroy();
            }
        });
});
