define([
  'dojo/_base/event',
  'dojo/_base/lang',
  'dojo/_base/Deferred',
  'dojo/DeferredList',
  'dojo/store/JsonRest',
  'dojo/query',
  'dojo/topic',
  'dojo/on',
  'dojo/cookie',
  'dojo/date/stamp',
  'mwg/widget/TwoButtonDialog',
  'mwg/widget/ConnectFbDialog'
  ],
function(
  event,
  lang,
  Deferred,
  DeferredList,
  Store,
  query,
  topic,
  on,
  cookie,
  stamp,
  TwoButtonDialog,
  ConnectFbDialog
  ) {

  var startsWith = function(str, strcmp) {
    return str.toLowerCase().slice(0, strcmp.length) == strcmp.toLowerCase();
  },

  init = function() {
    // Init global variables
    // TODO add pre-existing
    // TODO probably time to split main
    window.mwg.helpers = [];


    // TODO eventualy remove this
    // Fix issue with cookie paths
    expire_all_cookies('account',
        ['/rest', '/wishboard', '/index', '/wish',
        '/browse', '/editwish', '/newwish', '/tos']);
    if (!window.mwg.config.userId) {
      expire_all_cookies('account',['/']);
    }

    // Set account cookie
    var account = cookie('account');
    if (account) {
      account = JSON.parse(account);
      window.mwg.account = account;
    }
    // Set account global
    if (window.mwg.account) {
      window.mwg.active_user = window.mwg.account;
    }
    else {
      window.mwg.active_user = {
        id: window.mwg.config.userId,
        username: window.mwg.config.username
      };
    }

    // Update cookie access_token
    if (window.mwg.account) {
      FbDeferred().then(function() {
        FB.api(
          '/' + window.mwg.account.id + '?fields=access_token',
          'get',
          {access_token: window.mwg.account.access_token},
          function(response) {
            if ('error' in response) {
              // TODO check error type
              FB.api('/'+ window.mwg.account.id +'?fields=access_token',
                'get',
                function(response) {
                  window.mwg.account.access_token = response.access_token;
                  cookie('account',
                    JSON.stringify(window.mwg.account),
                    {path: '/'}
                    );
                });
            }
        });
      });
    }
  },

  facebookInit = function() {
    escapeIframe();

    FB.init({
      appId: window.mwg.config.appId,
      channelUrl: window.mwg.config.channelUrl,
      cookie: window.mwg.config.cookie,
      status: window.mwg.config.status,
      xfbml: window.mwg.config.xfbml,
      frictionlessRequests : true
    });

    //FB.Event.subscribe('edge.create', onLike);
    //FB.Event.subscribe('edge.remove', onUnlike);
    //FB.Event.subscribe('message.send', onSend);

    FB.getLoginStatus(function(response) {
      window.facebookReady = true;
      topic.publish('facebookReady');

      var connectDialog;
      if (response.status === 'connected') {
        if (!window.mwg.config.userId) {
          // Connecting to FB
          waitToConnect();
        }
        else if (window.mwg.config.userId != response.authResponse.userID) {
          // Changing User
          waitToConnect();
        }
      }
      else if (response.status === 'not_authorized') {
      }
      else {
        if (window.mwg.config.userId) {
          reload();
        }
      }
      query('.fblogin').on('click', function(evt) {
        event.stop(evt);
        login();
      });
    });
  },

  waitToConnect = function() {
    var connectDialog = new ConnectFbDialog();
    setTimeout(function () {
        if (cookie('fbsr_' + window.mwg.config.appId)) {
          reload();
        }
        //else {
        //  // TODO else refresh manually?
        //  connectDialog.hide();
        //}
      }, 1000);
  },

  onFacebookReady = function(callback) {
    if (window.facebookReady === undefined) {
      topic.subscribe('facebookReady', callback);
    }
    else {
      callback();
    }
  },

  FbDeferred = function() {
    var deferred = new Deferred();
    if (window.facebookReady === undefined) {
      topic.subscribe('facebookReady', function() {
        deferred.resolve();
      });
    }
    else {
      deferred.resolve();
    }
    return deferred;
  },

  login = function() {
    FB.login(function(response) {
      if (response.authResponse) {
          reload();
      } else {
        // User cancelled login or did not fully authorize
      }
    },{scope: 'email,read_stream'});
  },

  getPermissions = function() {
    var def = new Deferred();
    FbDeferred().then(function() {
      FB.api('me/permissions', 'get', function(response) {
        if ('data' in response) {
          def.resolve(response.data[0]);
        }
        else {
          def.resolve(undefined);
        }
      });
    });
    return def;
  },

  hasPermission = function(permission) {
    var def = new Deferred();
    getPermissions().then(function(response) {
      if (response === undefined) {
        def.resolve(false);
        return;
      }
      if (permission in response) {
        def.resolve(true);
      }
      else {
        def.resolve(false);
      }
    });
    return def;
  },

  addPermission = function(permission) {
    var def = new Deferred();
    getPermissions().then(function(response) {
      if (permission in response) {
        def.resolve(true);
      }
      else {
        var scope = '';
        for (var key in response) {
          if (key == 'installed') continue;
          if (key == 'bookmarked') continue;
          scope += ',' + key;
        }
        scope = permission + scope;

        FbDeferred().then(function() {
          FB.login(function(response) {
            hasPermission(permission).then(function(response) {
              if (response) {
                def.resolve(true);
              }
              else {
                def.resolve(false);
              }
            });
          }, {scope: scope});
        });
      }
    });

    return def;
  },

  userStore = new Store({
    target: '/rest/user/',
    idProperty: 'user_id'
  }),
  accountStore = new Store({
    target: '/rest/account/',
    idProperty: 'id'
  }),
  wishStore = new Store({
    target: '/rest/wish/',
    idProperty: 'wish_id'
  }),
  //dashboardStore = new Store({
  //  target: '/rest/dashboard/',
  //  idProperty: 'wish_id'
  //}),
  favoriteStore = new Store({
    target: '/rest/favorite/',
    idProperty: 'wish_id'
  }),
  //browseStore = new Store({
  //  target: '/rest/browse/',
  //  idProperty: 'wish_id'
  //}),
  friendStore = new Store({
    target: '/rest/friends/',
    idProperty: 'user_id'
  }),
  likeStore = new Store({
    target: '/rest/like/',
    idProperty: 'wish_id'
  }),
  //sharesStore = new Store({
  //  target: '/rest/page-shares/',
  //  idProperty: 'wish_id'
  //}),
  commentStore = new Store({
    target: '/rest/comment/',
    idProperty: 'comment_id'
  }),

  //onLike = function() {
  //  if (window.mwg.config.userId === undefined) return;
  //    putArgs = {
  //      user_id: window.mwg.config.userId,
  //      like: true
  //    };
  //  likesStore.put(putArgs);
  //},

  //onUnlike = function() {
  //  if (window.mwg.config.userId === undefined) {
  //    return;
  //  }
  //  putArgs = {
  //    _csrf_token: cookie('c'),
  //    user_id: window.mwg.config.userId,
  //    like: false
  //  };
  //  likesStore.put(putArgs);
  //},

  onSend = function() {
    if (window.mwg.config.userId === undefined) return;
      putArgs = {
        _csrf_token: cookie('c'),
        user_id: window.mwg.config.userId,
        share: true
      };
    sharesStore.put(putArgs);

  },

  escapeIframe = function() {
    if (top != self) { top.location.replace(self.location.href); }
  },

  reload = function() {
    if (startsWith(window.location.pathname, '/index')) {
      window.location = '/';
    }
    else {
      window.location.reload();
    }
  },

  goHome = function() {
    window.location = window.mwg.config.externalHref;
  },

  isCanvas = function() {
    var canvas_hostname = 'apps.facebook.com';
      if (top.location.hostname == canvas_hostname) {
        return true;
      }
    return false;
  },

  selectRandom = function(lst, limit) {
    var last = [];
    var size = lst.length;
    var index, elem;

    while (limit && size) {
      size = size - 1;
      index = Math.round(Math.min(limit, size) * Math.random());
      elem = lst[index];
      lst[index] = lst[size];
      limit = limit - 1;
      last.push(elem);
    }
    return last;
  },

  getPosts = function(posts_id) {

    var deferred = new Deferred(),
      posts = [];

    if ((posts_id === undefined) || (posts_id.length === 0)) {
      deferred.resolve({posts: posts});
      return deferred;
    }

    var max_batch = 50,
      slices = Math.ceil(posts_id.length / max_batch) - 1,
      slice = 0;

    // Build FB request array
    var fb_batch_args = [];
    for (var i = 0; i < posts_id.length; i++) {
      fb_batch_args.push({method: 'GET',
        relative_url: posts_id[i] + ['?fields=id,from,to,message,likes,actions,',
        'picture,link,name,caption,description,icon,actions,privacy,type,status_type,',
        'application,created_time,updated_time,',
        'comments.fields(user_likes,like_count,from,id,message,created_time)'].join('')
      });
    }

    callback = function(response) {
      var post, batch_request, params;

      if ('error' in response) {
        console.warn('Top Error');
        console.warn('9551059d-2d79-4391-9f3d-135842372a6e');
        console.warn(response);
        if ('message' in response) {
          console.warn(response.message);
        }
        deferred.resolve(response);
        return response;
      }

      // Happens randomly haven't been to track why it happens
      if ('type' in response) {
        var errmsg = ['Top level access token or app id not specified for',
          'batch request'].join(' ');

        if (response.type == 'GraphBatchException' &&
            response.message.slice(0, errmsg.length) == errmsg) {
          console.warn('8e6e6fa6-b271-42a4-b205-d3c505eafd1f');
          console.warn(response);
          // Repeat request
          batch_request = fb_batch_args.slice(
              max_batch * slice, max_batch * slice + max_batch);
          params = {batch: batch_request};
          if (window.mwg.account) params.access_token = window.mwg.account.access_token;
          FB.api('/', 'POST',
              params,
              lang.hitch(this, callback));
        }
      }

      // Parse posts
      for (var i = 0; i < response.length; i++) {
        if (response[i].code == 200) {
          post = JSON.parse(response[i].body);

          if (post !== false) {
            var numk = 0;
            for (var k in post) numk++;
            if (numk == 1) {
              console.warn('3e0e24ca-8c55-4925-aaa4-f9b12538b823');
              console.warn('Post exists but user is not authorized');
              console.warn(response[i]);
              continue;
            }


            // Group posts are no longer supported
            //// Stats
            //// Means post was made to somewhere (like a group)
            //// I'm trying to filter those that aren't counted in link_stat
            //if ('to' in post) {
            //  stats.shares += 1;
            //  if ('likes' in post) {
            //    stats.likes += post.likes.count;
            //  }
            //  if ('comments' in post) {
            //    stats.comments += post.comments.data.length;
            //    for (var j = 0; j < post.comments.data.length; j++) {
            //      if ('likes' in post.comments.data[j]) {
            //        stats.likes += post.comments.data[j].likes;
            //      }
            //    }
            //  }
            //}

            // Change time/date to from iso string
            post.created_time = stamp.fromISOString(post.created_time);
            if ('comments' in post) {
              for (var l = 0; l < post.comments.data.length; l++) {
                post.comments.data[l].created_time =
                  stamp.fromISOString(post.comments.data[l].created_time);
              }
            }

            // Helper count
            if (window.mwg.active_user && (window.mwg.active_user.id != post.from.id)) {
              // TODO add count on comments
              var helper = get_helper(post.from.id);
              helper.count += 1;
            }

            posts.push(post);
          }
        }
        else {
          var error = JSON.parse(response[i].body);
          console.warn('9cce63ed-ccbb-446d-bee9-40ca89e4fa62');
          console.warn('Post doesnt exist or other unspecified error');
          console.warn(response[i]);
          //getPost('100004716903320_177878162379413').then(function(response)
          //    {
          //      console.log("HAHAHAHAH");
          //      console.log(response);
          //    });
        }
      }


      slice = slice + 1;
      if (slice > slices) {
        // All posts retrieved
        deferred.resolve({posts: posts});
      }
      else {
        // Get next batch
        batch_request = fb_batch_args.slice(
          max_batch * slice, max_batch * slice + max_batch);
        params = {batch: batch_request};
        if (window.mwg.account) params.access_token = window.mwg.account.access_token;
        FB.api('/', 'POST',
            params,
            lang.hitch(this, callback));
      }
    };

    // Get posts
    var batch_request = fb_batch_args.slice(
        max_batch * slice, max_batch * slice + max_batch);
    params = {batch: batch_request};
    if (window.mwg.account) params.access_token = window.mwg.account.access_token;
    FB.api('/', 'POST',
        params,
        lang.hitch(this, callback));

    return deferred;
  },

  getPost = function(post_id) {
    var deferred = new Deferred();
    var callback = function(response) {
      if ('error' in response) {
        console.warn('8a8aa096-0dab-4cab-bd6b-74ccaa1b787a');
        console.warn(response);
        deferred.reject(response);
      }
      else {
        // Parse dates
        response.created_time = stamp.fromISOString(response.created_time);

        if ('comments' in response) {
          for (var j = 0; j < response.comments.data.length; j++) {
            response.comments.data[j].created_time =
              stamp.fromISOString(response.comments.data[j].created_time);
          }
        }
        deferred.resolve(response);
      }
    };
    var params = {};
    if (window.mwg.account) params.access_token = window.mwg.account.access_token;
    FB.api(post_id + ['?fields=id,from,to,message,likes,actions,',
        'picture,link,name,caption,description,icon,actions,privacy,type,status_type,',
        'application,created_time,updated_time,comments.fields(user_likes,like_count,from,id,message)'].join(''),
        'get',
        params,
        callback);

    return deferred;
  },

  getStats = function(url) {
    var deferred = new Deferred(),
      stats = {shares: 0, likes: 0, comments: 0, clicks: 0, total: 0};

    var params = {
      method: 'fql.query',
      query: 'SELECT url, normalized_url, share_count, like_count, ' +
      'comment_count, total_count, commentsbox_count, comments_fbid, ' +
      'click_count FROM link_stat WHERE url="' + url + '"'
    };
    // Response with access token
    // error_code: 190
    // error_msg: Impersonated access tokens can only be used with the Graph API
    //if (window.mwg.account) params.access_token = window.mwg.account.access_token;
    FB.api(params,
      lang.hitch(this, function(response) {
        if ('error' in response) {
          console.warn('3760ee83-02e9-4d67-86c4-98d368c8ffd5');
          console.warn(response);
          deferred.reject(response);
          return;
        }
        else if (response.length > 0) {
          stats.shares = parseInt(response[0].share_count, 10);
          stats.likes = parseInt(response[0].like_count, 10);
          stats.comments = parseInt(response[0].comment_count, 10);
          stats.comments += parseInt(response[0].commentsbox_count, 10);
          stats.clicks = parseInt(response[0].click_count, 10);
          stats.total = parseInt(response[0].total_count, 10);
        }
        deferred.resolve({stats: stats});
    }));
    return deferred;
  },

  getWishStats = function(wish) {
    var deferred = new Deferred();
    FbDeferred().then(function() {
      statsDef = [getStats(wish.url)];
      for (var i=0; i<wish.step_count; i++) {
        statsDef.push(getStats(wish.url + '/step/' + String(i+1)));
      }
      new DeferredList(statsDef).then(function(response) {
        var stats = {
          shares: 0,
          likes: 0,
          comments: 0
        };
        var i,j;
        for(i=0; i<response.length; i++) {
          stats.shares += response[i][1].stats.shares;
          stats.likes += response[i][1].stats.likes;
          stats.comments += response[i][1].stats.comments;
        }
        topic.publish('WishStats', stats);
        deferred.resolve(stats);
      });
    });
    return deferred;
  },

  replaceURLWithHTMLLinks = function(text) {
    var exp = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
    return text.replace(exp,"<a href='$1' target='_blank'>$1</a>"); 
  },

  timeDifference = function(current, previous) {

    var msPerMinute = 60 * 1000;
    var msPerHour = msPerMinute * 60;
    var msPerDay = msPerHour * 24;
    var msPerMonth = msPerDay * 30;
    var msPerYear = msPerDay * 365;

    var elapsed = current - previous;

    if (elapsed < msPerMinute) {
         return Math.round(elapsed/1000) + ' seconds ago';
    }

    else if (elapsed < msPerHour) {
         return Math.round(elapsed/msPerMinute) + ' minutes ago';
    }

    else if (elapsed < msPerDay ) {
         return Math.round(elapsed/msPerHour ) + ' hours ago';
    }

    else if (elapsed < msPerMonth) {
        return 'Approximately ' + Math.round(elapsed/msPerDay) + ' days ago';
    }

    else if (elapsed < msPerYear) {
        return 'Approximately ' + Math.round(elapsed/msPerMonth) + ' months ago';
    }

    else {
        return 'Approximately ' + Math.round(elapsed/msPerYear ) + ' years ago';
    }
  },

  getObjectData = function(objectId) {
    if (window.mwg.fbObjectData === undefined) window.mwg.fbObjectData = {};
    if (window.mwg.fbObjectDataDeferreds === undefined) window.mwg.fbObjectDataDeferreds = {};

    var deferred;
    if (objectId in window.mwg.fbObjectDataDeferreds) {
      return window.mwg.fbObjectDataDeferreds[objectId];
    }
    else {
      deferred = new Deferred();
      window.mwg.fbObjectDataDeferreds[objectId] = deferred;
    }

    if (objectId in window.mwg.fbObjectData) {
      deferred.resolve(window.mwg.fbObjectData[objectId]);
    }
    else {
      var reply;
      getData(objectId, '', true).then(function(response) {
        reply = {};
        lang.mixin(reply, response);
        if (response.metadata.type == 'page') {
          getData(response.id, 'link,website,id,name,picture,cover').then(function(response1) {
            lang.mixin(reply, response1);
            window.mwg.fbObjectData[objectId] = reply;
            delete window.mwg.fbObjectDataDeferreds[objectId];
            deferred.resolve(window.mwg.fbObjectData[objectId]);
          });
        }
        else if (response.metadata.type == 'user'){
          getData(response.id, 'name,picture,cover,mutualfriends').then(function(response1) {
            lang.mixin(reply, response1);
            window.mwg.fbObjectData[objectId] = reply;
            delete window.mwg.fbObjectDataDeferreds[objectId];
            deferred.resolve(window.mwg.fbObjectData[objectId]);
          });
        }
      });
    }
    return deferred;
  },

  //getUserData = function(userId) {
  //  return getData(userId, 'name,first_name,picture,cover,gender,mutualfriends');
  //  //return getData(userId, 'name,picture,cover,mutualfriends');
  //},
  //// TODO for some reason fb is only returning picture and id
  //// until this is solved we will store all data in account cookie
  //getAccountData = function(userId) {
  //  return getData(userId, 'link,website,id,name,picture,cover');
  //},

  getData = function(objectId, fields, metadata) {
    fields = typeof fields !== 'undefined' ? fields : '';
    metadata = typeof metadata !== 'undefined' ? metadata : false;

    var deferred = new Deferred();
    var url ='/' + objectId;
    if (fields !== '') url += '?fields=' + fields;
    if (metadata) {
      if (fields !== '') url += '&metadata=1';
      else url += '?metadata=1';
    }
    var params = {};
    if (window.mwg.account) params.access_token = window.mwg.account.access_token;
    FB.api(
        url,
        params,
        function(response) {
        if ('error' in response) {
          deferred.reject(response);
          return;
        }
        deferred.resolve(response);
    });
    return deferred;
  },


  getMutualFriends = function(userId) {
    if (window.mwg.fbMutualData === undefined) window.mwg.fbMutualData = {};
    if (window.mwg.fbMutualDataDeferreds === undefined) window.mwg.fbMutualDataDeferreds = {};

    var deferred;
    if (userId in window.mwg.fbMutualDataDeferreds) {
      return window.mwg.fbMutualDataDeferreds[userId];
    }
    else {
      deferred = new Deferred();
      window.mwg.fbMutualDataDeferreds[userId] = deferred;
    }

    if (userId in window.mwg.fbMutualData) {
      deferred.resolve(window.mwg.fbMutualData[userId]);
    }
    else {
      var params = {};
      if (window.mwg.account) params.access_token = window.mwg.account.access_token;
      FB.api(
          '/me/mutualfriends' + userId,
          params,
          function(response) {
          if ('error' in response) {
            deferred.reject(response);
          }
          window.mwg.fbMutualData[userId] = response;
          deferred.resolve(window.mwg.fbMutualData[userId]);
          delete window.mwg.fbMutualDataDeferreds[userId];
      });
    }

    return deferred;
  },

  setAccount = function(node) {
    var account = {
      id: node.id,
      access_token: node.access_token,
      name: node.name,
      picture: node.picture,
      username: node.username
    };
    window.mwg.account = account;
    cookie('account',
        JSON.stringify(account),
        {path: '/'});
  },

  removeAccount = function() {
    window.mwg.account = {};
    cookie('account',
        null,
        {expire: -1, path: '/'});
  },

  postAsPage = function(obj) {
    var deferred = new Deferred();
    if (window.mwg.account) obj.access_token = window.mwg.account.access_token;
    FB.api('/' + window.mwg.account.id + '/feed',
        'post',
        obj,
        function(response) {
          deferred.resolve(response);
    });
    return deferred;
  },

  expire_all_cookies = function(name, paths) {
      var expires = new Date(0).toUTCString();

      // expire null-path cookies as well
      //document.cookie = name + '=; expires=' + expires;

      for (var i = 0, l = paths.length; i < l; i++) {
          document.cookie = name + '=; path=' + paths[i] + '; expires=' + expires;
      }
  },
  
  sort_by_count = function(a, b) {
    if (a.count < b.count)
      return -1;
    if (a.count > b.count)
      return 1;
    return 0;
  },
  
  get_helper = function(user_id) {
    for(var i=0; i < window.mwg.helpers.length; i++) {
      if (window.mwg.helpers[i].user_id == user_id) return window.mwg.helpers[i];
    }
    var helper = {user_id: user_id, count: 0};
    window.mwg.helpers.push(helper);
    return helper;
  };

  return {
    startsWith: startsWith,
    init: init,
    facebookInit: facebookInit,
    onFacebookReady: onFacebookReady,
    FbDeferred: FbDeferred,
    login: login,
    wishStore: wishStore,
    userStore: userStore,
    accountStore: accountStore,
    //dashboardStore: dashboardStore,
    favoriteStore: favoriteStore,
    //browseStore: browseStore,
    friendStore: friendStore,
    likeStore: likeStore,
    commentStore: commentStore,
    goHome: goHome,
    isCanvas: isCanvas,
    selectRandom: selectRandom,
    getPost: getPost,
    getPosts: getPosts,
    getStats: getStats,
    getWishStats: getWishStats,
    getPermissions: getPermissions,
    hasPermission: hasPermission,
    addPermission: addPermission,
    replaceURLWithHTMLLinks: replaceURLWithHTMLLinks,
    timeDifference: timeDifference,
    getObjectData: getObjectData,
    getData: getData,
    //getUserData: getUserData,
    //getAccountData: getAccountData,
    setAccount: setAccount,
    removeAccount: removeAccount,
    postAsPage: postAsPage,
    expire_all_cookies: expire_all_cookies,
    sort_by_count: sort_by_count
  };
});
