define([
    'dojo/_base/Deferred',
    'dojo/_base/window',
    'dojo/window',
    'dojo/dom',
    'dojo/dom-geometry',
    'dojo/dom-construct',
    'dojo/on',
    'dojo/topic'
], function(
    Deferred,
    dwindow,
    win,
    dom,
    domGeom,
    domCtr,
    on,
    topic
    ) {
        var proximity = 400,
        page = 1,
        more_items = true,
        loading_items = false,
        queryArgs,
        _onScroll = function () {
            //console.log('_onScroll');
            //domGeom.docScroll
            var document_height = getDocHeight(),
                //document_height = domGeom.position(window, false).h,
                scroll_bottom = win.getBox().h + win.getBox().t,
                //scroll_bottom = domGeom.position(window, false).h + domGeom.docScroll.y,
                //scroll_bottom = domGeom.position(window, false).h + window.scrollTop(),
                reached_bottom = Boolean((document_height - scroll_bottom) < proximity);

            //console.log('document_height',document_height);
            //console.log('scroll_bottom',scroll_bottom);
            //console.log('reached_bottom',reached_bottom);
            if (reached_bottom) {
                topic.publish('reached_bottom');
            }
        },

        // This function will return any document’s height. It’s been tested in IE6/7, FF2/3, Safari (Windows),
        // Google Chrome and Opera 9.5. If the actual document’s body height is less than the viewport
        // height then it will return the viewport height instead:
        //http://james.padolsey.com/javascript/get-document-height-cross-browser/
        getDocHeight = function() {
            var D = document;
            return Math.max(
                Math.max(D.body.scrollHeight, D.documentElement.scrollHeight),
                Math.max(D.body.offsetHeight, D.documentElement.offsetHeight),
                Math.max(D.body.clientHeight, D.documentElement.clientHeight)
            );
        },

        restart = function(query) {
          queryArgs = query;
          page = 1;
          more_items = true;
        },

        init = function(store, query, callback) {
          on(document, 'scroll', _onScroll);

          queryArgs = query;
          var queryOptions = {start: page * window.mwg.config.pageSize, count: window.mwg.config.pageSize};

          topic.subscribe('reached_bottom', function() {
            if (!more_items) return;
            if (loading_items) return;

            createOverlay();

            queryOptions.start = page * window.mwg.config.pageSize;

            loading_items = true;
            return Deferred.when(store.query(queryArgs, queryOptions),
                function(response) {
                    if (response.length < window.mwg.config.pageSize) more_items = false;
                    if (response.length > 0) {
                        page = page + 1;
                        if (callback !== undefined) callback(response);
                    }
                    else {
                      destroyOverlay();
                    }
                    loading_items = false;
                });
          });
        },

        createOverlay = function() {
            domCtr.create('div', {id: 'overlay'}, dwindow.body());
        },

        destroyOverlay = function() {
          domCtr.destroy('overlay');
        };

        return {
            init: init,
            restart: restart,
            createOverlay: createOverlay,
            destroyOverlay: destroyOverlay
        };
    });
