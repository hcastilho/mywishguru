#!/usr/bin/env python2

import facebook
import conf
import pprint


if __name__ == '__main__':
    access_token = facebook.get_app_access_token(
            conf.FACEBOOK_APP_ID,
            conf.FACEBOOK_APP_SECRET)

    #print access_token

    #user_id = 733664591 # Nuno Madeira (FOF from Taheer)
    #user_id = 611799743 # Juca
    #user_id = 100001944758191 # Susana Branco
    #user_id = 749653643 # Taheer
    user_id = 718182560 # Daniela Martens
    #user_id = 521830000 #Margarida Prr
    #user_id = 100004716903320 #Henrique Cimento
    #user_id = 1795310892 #Hugo Castilho

    graph = facebook.GraphAPI(access_token)
    perms = graph.get_object('%s/permissions' % user_id)

    #args = {u'access_token': access_token}
    #perms = facebook.api(
    #        str(user_id) + '/permissions',
    #        args=args)

    #print perms
    pprint.pprint(perms)

