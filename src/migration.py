import logging

from google.appengine.ext import  webapp
from google.appengine.ext import deferred

from taskhandlers import Mapper
from models import User
from models import Account
from models import Wish
from models import Step
from models import get_user_or_account
import conf


class RunConversion(webapp.RequestHandler):
    def post(self):
        mapper = WishConversionMapper()
        deferred.defer(mapper.run)
class WishConversionMapper(Mapper):
    KIND = Wish
    def map(self, wish):
        if wish.privacy == 'list':
            wish.privacy = 'friendlist'
        if wish.privacy == 'group':
            wish.privacy = 'invisible'
        if not wish.privacy:
            wish.privacy = 'fof'

        if (wish.logo == conf.DEFAULT_IMAGE or
            wish.logo == conf.DEFAULT_THUMB):
            wish.logo = ''

        wish.friendlist = wish.get_list()
        wish.friends = wish.get_friends()

        return ([wish], [])


class SchemaMigration1(webapp.RequestHandler):
    def post(self):
        mapper = SchemaMigration1Mapper()
        deferred.defer(mapper.run)
class SchemaMigration1Mapper(Mapper):
    KIND = User
    def map(self, user):
        user.accounts = None
        if hasattr(user, 'friendlists'):
            delattr(user, 'friendlists')
        if hasattr(user, 'groups'):
            delattr(user, 'groups')
        if hasattr(user, 'in_group'):
            delattr(user, 'in_group')
        if hasattr(user, 'page_likes '):
            delattr(user, 'page_likes ')
        if hasattr(user, 'page_shares '):
            delattr(user, 'page_shares ')
        if hasattr(user, 'thank_yous'):
            delattr(user, 'thank_yous')
        if hasattr(user, 'notification'):
            delattr(user, 'notification')
        if hasattr(user, 'notification_favorite '):
            delattr(user, 'notification_favorite ')
        if hasattr(user, 'notification_my_wish_shared '):
            delattr(user, 'notification_my_wish_shared ')
        if hasattr(user, 'notification_friend_fulfills '):
            delattr(user, 'notification_friend_fulfills ')
        if hasattr(user, 'fb_friends '):
            delattr(user, 'fb_friends ')
        if hasattr(user, 'friends '):
            delattr(user, 'friends ')
        return ([user], [])


class SchemaMigration2(webapp.RequestHandler):
    def post(self):
        mapper = SchemaMigration2Mapper()
        deferred.defer(mapper.run)
class SchemaMigration2Mapper(Mapper):
    KIND = Wish
    def map(self, wish):
        if hasattr(wish, 'user'):
            delattr(wish, 'user')

        if not wish.user_type:
            if User.get_by_key_name(wish.user_id):
                wish.user_type = 'user'
            else:
                wish.user_type = 'account'
            return ([wish], [])
        return ([], [])


class SchemaMigration3(webapp.RequestHandler):
    def post(self):
        mapper = SchemaMigration3Mapper()
        deferred.defer(mapper.run)
class SchemaMigration3Mapper(Mapper):
    KIND = Step
    def map(self, model):
        if hasattr(model, 'user'):
            delattr(model, 'user')
            return ([model], [])
        return ([], [])


class SetHasWishes(webapp.RequestHandler):
    def post(self):
        mapper = HasWishesAccountMapper()
        deferred.defer(mapper.run)
        mapper = HasWishesUserMapper()
class HasWishesAccountMapper(Mapper):
    KIND = Account
    def map(self, model):
        logging.info(model.key().name())
        logging.info(Wish.all().filter('user_id =', model.key().name()).count(limit=1))
        if Wish.all().filter('user_id =', model.key().name()).count(limit=1):
            model.has_wishes = True
        else:
            model.has_wishes = False
        return ([model], [])

class HasWishesUserMapper(Mapper):
    KIND = User
    def map(self, model):
        if Wish.all().filter('user_id =', model.key().name()).count(limit=1):
            model.has_wishes = True
        else:
            model.has_wishes = False
        return ([model], [])

class SetInactivityWarning(webapp.RequestHandler):
    def post(self):
        mapper =  InactivityWarningMapper()
        deferred.defer(mapper.run)
class InactivityWarningMapper(Mapper):
    KIND = Wish
    def map(self, model):
        model.received_inactivity_warning = True
        return ([model], [])

class SetAccountTag(webapp.RequestHandler):
    def post(self):
        mapper =  AccountTagMapper()
        deferred.defer(mapper.run)
class AccountTagMapper(Mapper):
    KIND = Wish
    def map(self, model):
        wish_user = get_user_or_account(model.user_id)
        if wish_user.type == 'account':
            model.account_tag = model.user_id
            return ([model], [])
        return ([], [])
