#!/usr/bin/env python
# coding: utf-8
"""Facebook Handlers"""
import os
import logging
import re
import datetime
from uuid import uuid4
from random import choice
import urllib
import urllib2
import httplib
import Cookie
from google.appengine.ext import  webapp
from google.appengine.ext.webapp import template
from google.appengine.ext import deferred
from google.appengine.api import memcache
import json
import conf
from models import User
from models import Account
from models import Wish
from models import _USER_FIELDS
from models import _ACCOUNT_FIELDS
from models import get_picture
import facebook
from facebook import GraphAPI, GraphAPIError
from facebook import refresh_open_graph
from aux import CsrfException
from aux import log_exception


class BaseHandler(webapp.RequestHandler):
    """Base handler, deals with facebook authentication """
    graph = None
    user = None
    csrf_protect = True
    # To avoid refreshing access_token when user requests fofimages
    check_access_token = True

    def __init__(self, *args, **kwargs):
        super(BaseHandler, self).__init__(*args, **kwargs)
        self.IE = False
        self.fb_user_id = None
        self.csrf_token = None


    def initialize(self, request, response):
        """General initialization for every request"""
        super(BaseHandler, self).initialize(request, response)
        try:

            if self.user:
                self.user.last_access = datetime.datetime.now()
                self.user.put()

            self.init_facebook()
            self.init_account()
            self.init_csrf()
            self.response.headers[u'P3P'] = u'CP=HONK'  # iframe cookies in IE
        except Exception:
            raise

        p = re.compile(r'MSIE (?P<version>\d\.\d)')
        if p.search(self.request.headers['User-Agent']):
            self.IE = True
        else:
            self.IE = False

    def error(self, code):
        super(BaseHandler, self).error(code)
        if code == 404:
            self.render('fourofour')

    def render(self, name, **data):
        """Render a template"""
        self.response.headers[u'Content-Type'] = u'text/html; charset=UTF-8'

        if not data:
            data = {}

        # JS Config
        data[u'config'] = json.dumps({
            u'appId': conf.FACEBOOK_APP_ID,
            u'canvasName': conf.FACEBOOK_CANVAS_NAME,
            u'channelUrl': u'//' + conf.HOSTNAME + '/html/channel.html',
            u'cookie': True,
            u'status': True,
            u'xfbml': True,
            u'userId': self.user.key().name() if self.user else None,
            u'username': self.user.username if self.user else None,
            u'userName': self.user.name if self.user else None,
            u'userFirstName': self.user.first_name if self.user else None,
            u'userPicture': self.user.picture if self.user else None,
            u'pageSize': conf.PAGE_SIZE,
            u'externalHref': conf.EXTERNAL_HREF,
            u'defaultImage': conf.DEFAULT_IMAGE,
            u'defaultThumb': conf.DEFAULT_THUMB,
        })

        data[u'conf'] = conf

        data[u'user'] = self.user if self.user else None
        data[u'account'] = self.account if self.account else None
        data[u'active_user'] = self.active_user

        data[u'csrf_token'] = self.csrf_token
        data[u'canvas_name'] = conf.FACEBOOK_CANVAS_NAME
        data[u'app_id'] = conf.FACEBOOK_APP_ID
        data[u'site_name'] = conf.SITE_NAME
        data[u'page'] = name
        data['request_url'] = self.request.url
        #if name == 'browse':
        #    logging.info('wishboard')
        #    logging.info('wishboard' in data)
        #    logging.info(data['wishboard'])
        #    if 'wishboard' in data and data['wishboard']:
        #        data[u'page'] = 'wishboard'

        # Google Analytics
        data[u'analytics_id'] = conf.ANALYTICS_ID
        data[u'member'] = 'Member' if self.user else 'Visitor'

        # Genie message
        data['message'] = get_genie_message(
                name,
                data['is_owner'] if 'is_owner' in data else False,
                )

        self.response.out.write(template.render(
            os.path.join(
                os.path.dirname(__file__), u'templates', name + u'.html'),
            data))

    def init_facebook(self):
        """Sets up the request specific Facebook and User instance"""
        parsed_request = {}

        # When the app is loaded from a canvas page it comes as POST with a
        # post variable signed_request
        # Since we are using client side auth we ignore this and change the
        # method to GET
        if u'signed_request' in self.request.POST:
            self.request.method = u'GET'

        if u'fbsr_' + conf.FACEBOOK_APP_ID in self.request.cookies:
            parsed_request = facebook.parse_signed_request(
                    self.request.cookies.get(
                        u'fbsr_' + conf.FACEBOOK_APP_ID),
                        conf.FACEBOOK_APP_SECRET)

        if not parsed_request:
            return

        if parsed_request.get('user_id'):
            self.fb_user_id = parsed_request.get('user_id')

        logging.debug(parsed_request)

        # try to load or create a user object
        user = User.get_by_key_name(parsed_request.get(u'user_id'))
        if user:
            self.user = user
            logging.info('%s %s', user.name, user.key().name())


            try:
                graph = GraphAPI(user.access_token)
                me = graph.get_object(u'me', fields=_USER_FIELDS)
            except Exception:
                user.dirty = True
                user.put()

            #if ((not user.access_token_expires)
            #    or user.dirty
            #    or (datetime.datetime.now() > (user.access_token_expires - datetime.timedelta(days=7)))):
            if user.dirty:
                try:
                    logging.info('Updating access token for user %s %s', user.name, user.user_id)
                    access_token = facebook.get_access_token_from_code(
                            parsed_request[u'code'],
                            conf.FACEBOOK_REDIRECT_URI,
                            conf.FACEBOOK_APP_ID,
                            conf.FACEBOOK_APP_SECRET)
                    logging.info('access_token: %s', access_token)

                    graph = GraphAPI(access_token.get(u'access_token'))
                    access_token = graph.extend_access_token(
                            conf.FACEBOOK_APP_ID,
                            conf.FACEBOOK_APP_SECRET)
                    logging.info('extended access_token: %s', access_token)
                    user.access_token = access_token['access_token']
                    user.access_token_update = datetime.datetime.now()
                    if 'expires' in access_token:
                        user.access_token_expires = (datetime.datetime.now()
                            + datetime.timedelta(seconds = int(access_token['expires'])))
                    else:
                        user.access_token_expires = None
                    user.put()
                except GraphAPIError:
                    # Check Facebook Login on the Project Wiki
                    return
                except httplib.HTTPException:
                    return
                except urllib2.HTTPError:
                    return

            #time_since_last = (datetime.datetime.now()
            #        - user.updated)
            #if user.dirty or (datetime.timedelta(hours=24) < time_since_last):
            # We will trust fbrealtime and weekly updates
            if user.dirty:
                logging.info('Refreshing user data')
                deferred.defer(user.refresh)

            return

        if (not user) and parsed_request.get(u'code'):
            try:
                access_token = facebook.get_access_token_from_code(
                        parsed_request[u'code'],
                        conf.FACEBOOK_REDIRECT_URI,
                        conf.FACEBOOK_APP_ID,
                        conf.FACEBOOK_APP_SECRET)
                graph = GraphAPI(access_token.get(u'access_token'))
                access_token = graph.extend_access_token(
                        conf.FACEBOOK_APP_ID,
                        conf.FACEBOOK_APP_SECRET)
                me = graph.get_object(u'me', fields = _USER_FIELDS)
                self.graph = graph
            except GraphAPIError:
                # Check Facebook Login on the Project Wiki
                return
            except httplib.HTTPException:
                return
            except urllib2.HTTPError:
                return
            User.create(me, access_token)
            logging.info('New User')
        logging.info('No User')

    def init_account(self):
        self.account = None
        if not self.user:
            return

        # TODO o que fazer se o gajo for removido??
        # Nao vou estar a verificar sempre...
        # TODO redresh page info
        #if not hasattr(self.user, 'account'):
        #    return

        #logging.info(self.request.cookies.keys())
        if ('account' not in self.request.cookies):
            return


        try:
            account_data = self.request.cookies.get('account')
            account_data = urllib.unquote(account_data)
            account_data = json.loads(account_data)
            if not account_data:
                return
            account_id = account_data['id']
        except ValueError:
            return

        account = Account.get_by_key_name(account_id)
        if not account:
            update_user_accounts(self.user, account_id = account_id)
            account = Account.get_by_key_name(account_id)

        if account:
            logging.info('account: %s %s', account.key().name(), account.name)
            if not account.updated:
                logging.info('Refreshing account data')
                update_user_accounts(self.user, account_id = account_id)
            else:
                time_since_last = (datetime.datetime.now()
                        - account.updated)
                if (account.dirty or
                        (datetime.timedelta(hours=24) < time_since_last)):
                    logging.info('Refreshing account data')
                    update_user_accounts(self.user, account_id = account_id)
            self.account = account
            logging.info('%s %s', account.name, account.key().name())

    @property
    def active_user(self):
        if self.account:
            return self.account
        else:
            return self.user

    @property
    def active_user_type(self):
        if self.account:
            return 'account'
        else:
            return 'user'


    def init_csrf(self):
        """Issue and handle CSRF token as necessary"""
        self.csrf_token = self.request.cookies.get(u'c')
        if not self.csrf_token:
            self.csrf_token = str(uuid4())[:8]
            set_cookie(self.response.headers, 'c', self.csrf_token)
        if self.request.method == u'POST' and self.csrf_protect:
            # The csrf token can come in the body (request.POST) on regular
            # forms or in the query string of a POST or in the body of a Json
            # post
            csrf_token = self.request.get(u'_csrf_token')
            if not csrf_token:
                try:
                    data = json.loads(self.request.body)
                    csrf_token = data.get(u'_csrf_token')
                except ValueError:
                    logging.warning(u'ValueError')
            if self.csrf_token != csrf_token:
                raise CsrfException(u'Missing or invalid CSRF token.')

def set_cookie(headers, name, value, expires=None):
    """Set a cookie"""
    if value is None:
        value = u'deleted'
        expires = datetime.timedelta(minutes=-50000)
    jar = Cookie.SimpleCookie()
    jar[name] = value
    jar[name][u'path'] = u'/'
    if expires:
        if isinstance(expires, datetime.timedelta):
            expires = datetime.datetime.now() + expires
        if isinstance(expires, datetime.datetime):
            expires = expires.strftime(u'%a, %d %b %Y %H:%M:%S')
        jar[name][u'expires'] = expires
    headers.add_header(*jar.output().split(u': ', 1))

def get_genie_message(name, is_owner = False):
    messages = {
        'browse': [
            'Check out your friends wishes and <a href="http://blog.mywishguru.com" title="Go to blog" target="_blank">get ideas</a>!',
            'See how your friends are doing and <a href="http://blog.mywishguru.com" title="Go to blog" target="_blank">motivate them on their journey</a>.',
            'Have you <a href="http://blog.mywishguru.com" title="Go to blog" target="_blank">helped a friend</a> today?',
            '<a href="http://blog.mywishguru.com" title="Go to blog" target="_blank">People who share their goals</a> are more likely to achieve them.',
            'Discover which friends <a href="http://blog.mywishguru.com" title="Go to blog" target="_blank">share similar goals</a>.',
        ],
        'wishboard': [
            '<a href="http://blog.mywishguru.com" title="Go to blog" target="_blank">Focus</a> on what matters.',
            'Make what you want to achieve <a href="http://blog.mywishguru.com" title="Go to blog" target="_blank">part of your everyday life</a>.',
            'Make your <a href="http://blog.mywishguru.com" title="Go to blog" target="_blank">dreams</a> real by sharing them with your friends.',
            'Use the <a href="http://blog.mywishguru.com" title="Go to blog" target="_blank">power of your network</a> to help you on your quest.',
        ],
        'wish_owner': [
            '<a href="http://blog.mywishguru.com" title="Go to blog" target="_blank">Keep track</a> of your progress and achieve your dream.',
            '<a href="http://blog.mywishguru.com" title="Go to blog" target="_blank">Share it!</a> Share it! Share it!',
            'Share your wish and let your <a href="http://blog.mywishguru.com" title="Go to blog" target="_blank">friends help</a>.',
            '<a href="http://blog.mywishguru.com" title="Go to blog" target="_blank">Make your dreams real</a> by sharing them with your friends.',
            'Use the <a href="http://blog.mywishguru.com" title="Go to blog" target="_blank">power of your network</a> to help you on your quest.',
        ],
        'wish_others': [
            'See how you can <a href="http://blog.mywishguru.com" title="Go to blog" target="_blank">help your friend</a>.',
            '<a href="http://blog.mywishguru.com" title="Go to blog" target="_blank">Share</a> your friends wish.',
            'Add a <a href="http://blog.mywishguru.com" title="Go to blog" target="_blank">friends wish</a> to favourites and follow it.',
        ],
        'newwish': [
            'Easily <a href="http://blog.mywishguru.com" title="Go to blog" target="_blank">set goals</a> you want to achieve.',
            '<a href="http://blog.mywishguru.com" title="Go to blog" target="_blank">Map out</a> the steps you need to take.',
            'Use the <a href="http://blog.mywishguru.com" title="Go to blog" target="_blank">power of your network</a> to help you on your quest.',
            'Beautifully <a href="http://blog.mywishguru.com" title="Go to blog" target="_blank">capture your journey</a> and document the steps you have taken.',
            'People who <a href="http://blog.mywishguru.com" title="Go to blog" target="_blank">write down</a> their goals are more likely to achieve them.',
        ],
    }

    message = ''
    if (name == 'wish'):
        messages = messages['%s_%s' % (name, 'owner' if is_owner else 'others')]
        message = choice(messages)
    else:
        if (name in messages):
            messages = messages[name]
            message = choice(messages)

    return message


def update_user_accounts(user, account_id=None):
    try:
        graph = GraphAPI(user.access_token)
        accounts = graph.get_connections('me', 'accounts', fields = _ACCOUNT_FIELDS)
        accounts = accounts['data']
        logging.info(accounts)
    except GraphAPIError, e:
        log_exception(e)
        return
    for account in accounts:
        if account_id and account_id != account['id']:
            continue
        props = {
            'name': account['name'],
            'access_token': account['access_token'],
            }
        if 'username' in account:
            props['username'] = account['username']
        if 'website' in account:
            props['website'] = account['website']
        if 'link' in account:
            props['link'] = account['link']
        if 'picture' in account:
            props['picture'] = get_picture(account['id'], account['picture']['data']['url'])
        if 'cover' in account:
            props['cover'] = account['cover']['source']
        props['updated'] = datetime.datetime.now()
        db_account = Account.get_by_key_name(account['id'])

        # Update account data in wishes
        update_wishes = False
        if db_account:
            if 'name' in props and props['name'] != db_account.name:
                update_wishes = True
            if 'picture' in props and props['picture'] != db_account.picture:
                update_wishes = True

        if db_account:
            for key in props:
                setattr(db_account, key, props[key])
        else:
            db_account = Account(
                key_name = account['id'],
                **props
                )
            memcache.delete('accounts')
        if user.key().name() not in db_account.admins:
            db_account.admins.append(user.key().name())
        db_account.put()
        refresh_open_graph(db_account.url)

        if update_wishes:
            for wish in Wish.all().filter('user_id =', db_account.key().name()):
                wish.user_name = props['name']
                wish.user_picture = props['picture']
                wish.put()
