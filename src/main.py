#!/usr/bin/env python
# coding: utf-8
"""Main dispacher for handlers"""
import logging
from google.appengine.ext import  webapp
from google.appengine.ext.webapp import util
#from google.appengine.ext import ereporter
from libs import ereporter

import conf
#from views import PageNotFoundHandler

from rest import WishStore

#from rest import RestDashboardHandler
from rest import RestFavoritesHandler
#from rest import RestBrowseHandler
from rest import RestUserHandler
from rest import AccountHandler
from rest import FriendsHandler
from rest import StepHandler
from rest import FriendlistHandler
from rest import LikeStore
#from rest import RestShareHandler
from rest import CommentStore

from views import WishboardHandler
from views import TosHandler
from views import IndexHandler
from views import WishHandler
#from views import WishCard
from views import BrowseHandler
from views import EditWishHandler
from fofimage import ImgServer
from taskhandlers import FbRealtimeHandler

"""Load custom Django template filters"""
webapp.template.register_template_library('filters.filters')

ereporter.register_logger()

def main():
    """Dispatcher"""
    routes = [
        (r'/rest/wish/(.*)', WishStore),
        (r'/rest/user/(.*)', RestUserHandler),
        (r'/rest/account/(.*)', AccountHandler),
        #(r'/rest/dashboard/(.*)', RestDashboardHandler),
        (r'/rest/favorite/(.*)', RestFavoritesHandler),
        #(r'/rest/browse/(.*)', RestBrowseHandler),
        (r'/rest/friends/', FriendsHandler),
        (r'/rest/step/(.*)', StepHandler),
        (r'/rest/friendlist/(.*)', FriendlistHandler),
        (r'/rest/like/(.*)', LikeStore),
        #(r'/rest/page-shares/', RestShareHandler),
        #(r'/rest/(.*)', RestHandler),
        (r'/rest/comment/(.*)', CommentStore),

        (r'/', BrowseHandler),
        (r'/wishboard', WishboardHandler),
        (r'/index', IndexHandler),
        (r'/wish/(.*)/step/(.*)', WishHandler),
        (r'/wish/(.*)', WishHandler),
        #(r'/wishcard/(.*)', WishCard),
        (r'/browse/(.*)', BrowseHandler),
        (r'/browse', BrowseHandler),
        (r'/editwish/(.*)', EditWishHandler),
        (r'/newwish', EditWishHandler),
        (r'/tos', TosHandler),

        (r'/fbrealtime', FbRealtimeHandler),
        (r'/fofimages/(.*)', ImgServer),
        # TODO When we add urls here they have to be added to User.url

        # Vanity URL
        (r'/(.*)', BrowseHandler),
        # Page not found
        #(r'/.*', PageNotFoundHandler),
    ]
    application = webapp.WSGIApplication(routes, debug=conf.DEBUG)
    util.run_wsgi_app(application)


if __name__ == u'__main__':
    if conf.DEBUG:
        logging.getLogger().setLevel(logging.DEBUG)
    else:
        logging.getLogger().setLevel(logging.INFO)
    main()
