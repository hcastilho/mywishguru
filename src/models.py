#!/usr/bin/env python
# coding: utf-8
"""
Models file
"""
import random
import logging
import urllib2
import datetime
#from operator import attrgetter
#from google.appengine.datastore import entity_pb
from google.appengine.api import taskqueue
from google.appengine.ext import db
from google.appengine.ext import blobstore
from google.appengine.api import images
#from google.appengine.api import mail
from google.appengine.ext import deferred

import conf
from facebook import GraphAPI
from facebook import GraphAPIError
from facebook import refresh_open_graph
from aux import store_image
from aux import ImageStoreError
#from aux import log_exception
#from aux import GeneralException


_USER_FIELDS = u'id,name,first_name,email,picture,gender,friends,permissions,username'
_ACCOUNT_FIELDS = u'id,name,website,link,picture,cover,access_token,username'
_FRIENDLIST_FIELDS = u'name,list_type'
class Enum(set):
    """Enum replacement"""
    def __getattr__(self, name):
        if name in self:
            return name
        raise AttributeError

ASSOC = Enum(['SELF', 'FRIEND', 'FOF'])

class BaseModel(db.Model):
    """Implements db.Model with a few bells and wistles"""
    def __init__(self, parent=None, key_name=None, _app=None, **kwds):
        self.__isdirty = False
        super(BaseModel, self).__init__(parent, key_name, _app, **kwds)

    def __setattr__(self, attrname, value):
        """
        DataStore api stores all prop values say "email" is stored in "_email" so
        we intercept the set attribute, see if it has changed, then check for an
        onchanged method for that property to call
        """
        if (attrname.find('_') != 0):
            if hasattr(self,'_' + attrname):
                curval = getattr(self,'_' + attrname)
                if curval != value:
                    self.__isdirty = True
                    setattr(self, '__' + attrname + '_isdirty', True)

                    # Store old value
                    if not hasattr(self, '__' + attrname + '_old'):
                        setattr(self, '__' + attrname + '_old', curval)

                    if hasattr(self, attrname + '_onchange'):
                        getattr(self, attrname + '_onchange')(curval, value)

        super(BaseModel, self).__setattr__(attrname, value)


    def isdirty(self, attrname):
        """Tells if the attr has been changed"""
        return hasattr(self, '__' + attrname + '_isdirty')

    def oldval(self, attrname):
        """Returns the old value for attr"""
        if hasattr(self, '__' + attrname + '_old'):
            return getattr(self, '__' + attrname + '_old')
        else:
            return getattr(self, attrname)
        return None

# Serialize db.Model for memcaching
# http://blog.notdot.net/2009/9/Efficient-model-memcaching
#def serialize_entities(models):
#    if models is None:
#        return None
#    elif isinstance(models, db.Model):
#        # Just one instance
#        return db.model_to_protobuf(models).Encode()
#    else:
#        # A list
#        return [db.model_to_protobuf(x).Encode() for x in models]
#
#def deserialize_entities(data):
#    if data is None:
#        return None
#    elif isinstance(data, str):
#        # Just one instance
#        return db.model_from_protobuf(entity_pb.EntityProto(data))
#    else:
#        return [db.model_from_protobuf(entity_pb.EntityProto(x)) for x in data]

class Account(BaseModel):
    access_token = db.StringProperty()
    # Last time access token was updated
    access_token_update = db.DateTimeProperty(auto_now_add=True)
    # Last time access token was checked
    access_token_verify = db.DateTimeProperty(auto_now_add=True)

    created = db.DateTimeProperty(auto_now_add=True)
    updated = db.DateTimeProperty(auto_now_add=True)
    dirty = db.BooleanProperty(default=False)

    username = db.StringProperty()
    name = db.StringProperty(required=True)
    website = db.StringProperty()
    link = db.StringProperty()
    picture = db.StringProperty()
    # email will commonly be empty (pages on fb don't have email)
    email = db.StringProperty()

    wishboard_cover = db.StringProperty()

    admins = db.StringListProperty()
    has_wishes = db.BooleanProperty(default=False)

    notification_by_facebook = db.BooleanProperty(default=True)
    notification_by_email = db.BooleanProperty(default=True)

    #notification_friend_join = db.BooleanProperty(default=True)
    notification_my_wish_share = db.BooleanProperty(default=True)
    notification_my_wish_comment = db.BooleanProperty(default=True)
    notification_my_wish_like = db.BooleanProperty(default=True)
    #notification_friend_wish_create = db.BooleanProperty(default=True)
    #notification_friend_wish_fulfill = db.BooleanProperty(default=True)
    notification_favorite_wish_update = db.BooleanProperty(default=True)
    notification_favorite_wish_fulfill = db.BooleanProperty(default=True)
    notification_favorite_wish_share = db.BooleanProperty(default=True)
    notification_favorite_wish_comment = db.BooleanProperty(default=True)
    notification_favorite_wish_like = db.BooleanProperty(default=True)

    dirty = db.BooleanProperty(default=False)

    def delete(self):
        for wish in Wish.all().filter('user_id =', self.key().name()):
            wish.delete()

        for fofimg in FofImage.all().filter('user_id =', self.key().name()):
            fofimg.delete()

        super(BaseModel, self).delete()

    def refresh(self):
        """Refresh user data, if there are changes in facebook friends also
        updates friendlists and indexes"""
        logging.info('Refreshing User %s', self.key().name())
        graph = GraphAPI(self.access_token)
        try:
            me = graph.get_object(self.key().name(), fields=_ACCOUNT_FIELDS)
        except GraphAPIError, e:
            self.dirty = True
            self.put()
            logging.warning(
                u'Failed Refreshing User %s\n%s', self.key().name(), e)
            #log_exception(GraphAPIError)
            return

        # Refresh data in User
        self.name = me.get('name')
        self.username = me.get('username')
        self.picture = get_picture(self.key().name(), me.get('picture').get('data').get('url'))
        self.dirty = False
        self.updated = datetime.datetime.now()
        self.put()


        # Refresh data replicated in Wishes
        for wish in Wish.all().filter('user_id =', self.key().name()):
            wish.user_name = me.get('name')
            wish.user_picture = self.picture
            # Avoid calling Wish overloaded put()
            super(BaseModel, wish).put()

    @property
    def type(self):
        return 'account'

    @property
    def url(self):
        if self.username:
            if self.username not in ('rest', 'wishboard', 'index', 'wish',
                    'browse', 'editwish', 'newwish', 'tos', 'fbrealtime',
                    'fofimages',):
                return '/%s' % self.username
            else:
                return '/browse/%s' % self.username
        else:
            return '/browse/%s' % self.key().name()


# key_name = "user_id"
#class User(db.Expando):
class User(BaseModel):
    """User"""
    user_id = db.StringProperty(required=True)
    access_token = db.StringProperty()
    access_token_expires = db.DateTimeProperty()
    # Last time access token was updated
    access_token_update = db.DateTimeProperty(auto_now_add=True)
    # Last time access token was checked
    access_token_verify = db.DateTimeProperty(auto_now_add=True)

    created = db.DateTimeProperty(auto_now_add=True)
    updated = db.DateTimeProperty()
    dirty = db.BooleanProperty(default=False)

    username = db.StringProperty()
    name = db.StringProperty(required=True)
    first_name = db.StringProperty()
    picture = db.StringProperty()
    email = db.StringProperty()
    gender = db.StringProperty()
    permissions = db.StringListProperty(default=None)
    #accounts = db.StringListProperty(default=None)
    #account_list = db.StringListProperty()

    has_wishes = db.BooleanProperty(default=False)

    notification_by_facebook = db.BooleanProperty(default=True)
    notification_by_email = db.BooleanProperty(default=True)

    notification_friend_join = db.BooleanProperty(default=True)
    notification_my_wish_share = db.BooleanProperty(default=True)
    notification_my_wish_comment = db.BooleanProperty(default=True)
    notification_my_wish_like = db.BooleanProperty(default=True)
    notification_friend_wish_create = db.BooleanProperty(default=True)
    notification_friend_wish_fulfill = db.BooleanProperty(default=True)
    notification_favorite_wish_update = db.BooleanProperty(default=True)
    notification_favorite_wish_fulfill = db.BooleanProperty(default=True)
    notification_favorite_wish_share = db.BooleanProperty(default=True)
    notification_favorite_wish_comment = db.BooleanProperty(default=True)
    notification_favorite_wish_like = db.BooleanProperty(default=True)

    @property
    def url(self):
        if self.username:
            if self.username not in ('rest', 'wishboard', 'index', 'wish',
                    'browse', 'editwish', 'newwish', 'tos', 'fbrealtime',
                    'fofimages',):
                return '/%s' % self.username
            else:
                return '/browse/%s' % self.username
        else:
            return '/browse/%s' % self.key().name()

    @property
    def type(self):
        return 'user'

    def __eq__(self, other):
        return self.key() == other.key()

    def delete(self):
        for wish in Wish.all().filter('user_id =', self.key().name()):
            wish.delete()

        for fofimg in FofImage.all().filter('user_id =', self.key().name()):
            fofimg.delete()

        super(BaseModel, self).delete()

    @staticmethod
    def create(me, access_token):
        logging.info('Creating User %s', me.get('id'))
        # If we don't get expiracy date we set it to 7 days
        if access_token.get('expires'):
            access_token_expires = (datetime.datetime.now()
                + datetime.timedelta(seconds = int(access_token['expires'])))
        else:
            access_token_expires = (datetime.datetime.now()
                + datetime.timedelta(days = 7))

        user = User(key_name = me.get('id'),
            name = me.get(u'name'),
            first_name = me.get(u'first_name'),
            picture = me.get(u'picture').get('data').get('url'),
            email = me.get(u'email'),
            updated = datetime.datetime.now(),
            user_id = me.get('id'),
            access_token = access_token['access_token'],
            access_token_update = datetime.datetime.now(),
            access_token_expires = access_token_expires ,
            )
        user.put()

        # Create main friendlist
        friendlist = FriendList(
                key_name = user.key().name(),
                list_id = user.key().name(),
                user_id = user.key().name(),
                name='All Friends',
                type='mywishguru',
                )
        fb_friends = me.get(u'friends', {}).get(u'data', [])
        fb_friends =  [friend.get(u'id') for friend in fb_friends]
        friendlist.fb_friends = fb_friends
        friendlist.put()

        # Update this user in friends lists
        friendlists = (FriendList.all().filter('fb_friends =', user.key().name()))
        for flist in friendlists:
            flist.update_friends(added=[user.key().name()])
            flist.put()

        # Notify friends
        taskqueue.add(
            url='/task/notify-friend-join',
            params={'transmitter': user.key().name(),
                'receivers': friendlist.friends})

        taskqueue.add(
            url='/task/notify-my-join',
            params={'receiver': user.key().name()})

        return user

    def is_friend(self, user):
        """True if users are friends"""
        friendlist = FriendList.get_by_key_name(self.key().name())
        if user.key().name() in friendlist.friends:
            return True
        return False

    def is_fof(self, user):
        """True if users are fof"""
        friendlist = FriendList.get_by_key_name(self.key().name())
        friendlist2 = FriendList.get_by_key_name(user.key().name())
        if list(set(friendlist.friends).intersection(friendlist2.friends)):
            return True
        else:
            return False

    @staticmethod
    def get_association(user1, user2):
        """Return degree of association"""
        if not user1 or not user2:
            return None

        if not hasattr(user1, 'key'):
            user1 = get_user_or_account(user1)

        if not hasattr(user2, 'key'):
            user2 = get_user_or_account(user2)

        if user2.key().name() == user1.key().name():
            return ASSOC.SELF

        friendlist = FriendList.get_by_key_name(user1.key().name())
        if friendlist and user2.key().name() in friendlist.friends:
            return ASSOC.FRIEND

        friendlist2 = FriendList.get_by_key_name(user2.key().name())
        if (friendlist and friendlist2 and
                list(set(friendlist.friends).intersection(friendlist2.friends))):
            return ASSOC.FOF

        return None

    def refresh(self):
        """Refresh user data, if there are changes in facebook friends also
        updates friendlists and indexes"""
        logging.info('Refreshing User %s', self.key().name())
        graph = GraphAPI(self.access_token)
        try:
            me = graph.get_object(u'me', fields=_USER_FIELDS)
        except Exception, e:
            self.dirty = True
            self.put()
            logging.warning(
                u'Failed Refreshing User %s\n%s', self.key().name(), e)
            #log_exception(GraphAPIError)
            return

        # Refresh data in User
        self.username = me.get('username')
        self.name = me.get('name')
        self.first_name = me.get('first_name')
        self.picture = get_picture(self.key().name(), me.get('picture').get('data').get('url'))
        self.email = me.get('email')
        self.dirty = False
        self.updated = datetime.datetime.now()
        self.put()


        # Refresh data replicated in Wishes
        for wish in Wish.all().filter('user_id =', self.key().name()):
            wish.user_name = me.get('name')
            wish.user_first_name = me.get('first_name')
            wish.user_picture = self.picture

            # Avoid calling Wish overloaded put()
            super(BaseModel, wish).put()
            #wish.put()

        # Changes in the user friends FriendList
        friendlist = FriendList.get_or_insert(self.key().name(),
                list_id=self.key().name(),
                user_id=self.key().name(),
                name='All Friends',
                type='mywishguru')
        friendlist = FriendList.get_by_key_name(self.key().name())
        fb_friends_data = me.get(u'friends', {}).get(u'data', [])
        friendlist.fb_friends = [friend[u'id'] for friend in fb_friends_data]
        friendlist.put()

        # Avoid DeadlineExcededError
        #taskqueue.add(#queue_name='refresh-user',
        #              url=u'/task/refresh-friendlists',
        #              params={'user_id': self.key().name()})
        deferred.defer(self.refresh_friendlists)



    def refresh_friendlists(self):
        """Refresh user facebook friendlists"""
        logging.info('Refresh friendslists %s', self.key().name())
        # TODO test if we have permission
        logging.info('user_id %s' % self.key().name())
        graph = GraphAPI(self.access_token)
        try:
            friendlists = graph.get_object(
                    u'me/friendlists',
                    fields=_FRIENDLIST_FIELDS,
                    )
        except GraphAPIError, e:
            self.dirty = True
            self.put()
            logging.warning(
                u'Failed refreshing friendlists for user %s\n%s',
                self.key().name(), e)
            return
        friendlists = friendlists.get('data', [])

        curlists = [flist.get('id') for flist in friendlists]

        self.friendlists = curlists
        self.put()

        for friendlist in FriendList.all().filter('user_id =', self.key().name()):
            if friendlist.list_id == self.key().name():
                continue

            in_fb_lists = friendlist.list_id in curlists
            if not in_fb_lists:
                #list_id = friendlist.list_id
                #list_name = friendlist.name
                friendlist.delete()

                # TODO!!
                # Send Warnings for Wishes belonging to deleted friendlists
                #wishlist = (Wish.all()
                #    .filter('user_id =', self.key().name())
                #    .filter('friendlist =', list_id)
                #    )
                #sender = 'MyWishGuru Notifications <no-reply@mywishguru.com>'
                #for wish in wishlist:
                #    body = ('Hi!\nYour wish "%s" is assigned to a deleted '
                #        'friend list (%s).' % (wish.title, list_name))
                #    mail.send_mail(
                #        sender = sender,
                #        to = self.email,
                #        subject = 'Deleted facebook friend list',
                #        body= body
                #        )
                #    wish.privacy = 'invisible'
                #    wish.friendlist = None
                #    wish.put()


        for friendlist in friendlists:
            friendlist['user_id'] = self.key().name()
            friendlist_model = FriendList.update_or_insert(
                    friendlist.get('id'),
                    **friendlist)

            try:
                members = graph.get_object(
                        '%s/members' % friendlist.get('id')
                        )
            except GraphAPIError, e:
                continue

            if not members.get('data'):
                continue
            else:
                members = members.get('data')

            friendlist_model.fb_friends = (
                    [member.get('id') for member in members]
                    )
            friendlist_model.put()

def get_picture(user_id, url):
    try:
        img = urllib2.urlopen(url).read()
    except Exception, e:
        logging.info('Gracefull')
        logging.error(e)
        return url

    try:
        (blob_key, blob_url) = store_image(img)
    except images.NotImageError, e:
        logging.warn(e)
        return url
    except ImageStoreError, e:
        logging.warn(e)
        return url
    except images.TransformationError, e:
        logging.warn(e)
        return url

    fof_image = FofImage(
            user_id = user_id,
            img = blob_key,
            url = blob_url)
    fof_image.put()
    return blob_url



# key_name = <user_id> or <friendlist_id>
class FriendList(BaseModel):
    """User friendlists stores defaul (all friends) and custom friendlists"""
    list_id = db.StringProperty(required=True)
    user_id = db.StringProperty(required=True)
    name = db.StringProperty()
    type = db.StringProperty()
    fb_friends = db.StringListProperty(default=None)
    friends = db.StringListProperty(default=None)

    def put(self):
        if self.isdirty('fb_friends'):
            old_fb_friends = self.oldval('fb_friends')
            fb_friends = self.fb_friends
            added = list(set(fb_friends).difference(old_fb_friends))
            removed = list(set(old_fb_friends).difference(fb_friends))
            added = filter_friends(added)
            removed = filter_friends(removed)
            self.update_friends(added, removed)

        super(BaseModel, self).put()

    def update_friends(self, added=None, removed=None):
        """Updates user's WishIndex and FavoriteIndex. Also changes
        FriendList.friends"""
        logging.info('Update user: %s', self.user_id)
        added = added or []
        removed = removed or []
        self.friends.extend(added)
        self.friends = list(set(self.friends))
        for friend in removed:
            if friend in self.friends:
                self.friends.remove(friend)

        wishlist = (Wish.all()
                .filter('status !=', 'editing')
                .filter('friendlist =', self.list_id)
                )
        Wish.update_friends(wishlist, self.friends, removed)

    def delete(self):
        """Forces clearing of indexes when a friendlist is deleted"""
        self.fb_friends = []
        self.put()
        super(BaseModel, self).delete()

    @staticmethod
    def update_or_insert(key_name, **kwargs):
        friendlist = FriendList.get_by_key_name(key_name)
        if not friendlist:
            friendlist = FriendList(
                    user_id = kwargs['user_id'],
                    key_name = key_name,
                    list_id = kwargs['id'],
                    name = kwargs['name'],
                    type = kwargs['list_type'],
                    )

        else:
            friendlist.name = kwargs['name']
            friendlist.type = kwargs['list_type']
        friendlist.put()

        return friendlist


#class Step(db.Expando):
class Step(BaseModel):
    """Model"""
    #user = db.ReferenceProperty(User)
    user_id = db.StringProperty()

    wish_id = db.IntegerProperty()
    index =  db.IntegerProperty()

    created = db.DateTimeProperty(auto_now_add=True)
    updated = db.DateTimeProperty(auto_now_add=True)
    title = db.StringProperty(default="")
    desc = db.TextProperty(default="")
    logo = db.StringProperty(default="")
    status = db.StringProperty(choices=(u'editing', u'open', u'fulfilled'))

    likes = db.IntegerProperty(default=0)
    comments = db.IntegerProperty(default=0)
    shares = db.IntegerProperty(default=0)

    logo_top = db.FloatProperty(default=.0)
    logo_left = db.FloatProperty(default=.0)

    posts = db.StringListProperty(default=None)
    #deleted_posts = db.StringListProperty(default=None)

    @property
    def small_logo(self):
        if self.logo.endswith('=s0'):
            return '{0}{1}'.format(self.logo[:-1], 400)
        return self.logo

    @property
    def url(self):
        """Wish page URL"""
        return conf.EXTERNAL_HREF + u'wish/%s/step/%s' % (self.wish_id, self.index)

    @property
    def url_relative(self):
        """Wish page URL"""
        return u'/wish/%s/step/%s' % (self.wish_id, self.index)

    @property
    def model_kind(self):
        """Wish page URL"""
        return u'step'

    def serialize(self, user=None):
        response = {}
        response['user'] = {}
        response['user']['user_id'] = self.user_id
        wish_user = get_user_or_account(self.user_id)
        response['user']['username'] = wish_user.username
        response['user']['name'] = wish_user.name
        if hasattr(wish_user, 'first_name'):
            response['user']['first_name'] = wish_user.first_name
        response['user']['picture'] = wish_user.picture
        response['user']['url'] = wish_user.url

        wish = Wish.get_by_id(self.wish_id)
        response['wish_id'] = self.wish_id
        response['privacy'] = wish.privacy
        response['url'] = self.url

        response['step_id'] = self.key().id()
        response['title'] = self.title or u''
        response['desc'] = self.desc or u''
        response['logo'] = self.logo or ''
        response['small_logo'] = self.small_logo or ''
        response['status'] = self.status
        response['index'] = self.index
        response['posts'] = self.posts
        response['stats'] = {
                'likes': self.likes,
                'comments': self.comments,
                'shares': self.shares,
                }

        response['logoTop'] = self.logo_top
        response['logoLeft'] = self.logo_left

        return response


#class Wish(db.Expando):
class Wish(BaseModel):
    """Wish Model"""
    #user = db.ReferenceProperty(User)

    user_id = db.StringProperty()
    user_type = db.StringProperty()

    user_name = db.StringProperty()
    user_first_name = db.StringProperty()
    user_picture = db.StringProperty()

    friendlist = db.StringProperty(default='')
    friends = db.StringListProperty(default=None)
    friends_private = db.StringListProperty(default=None)
    favorited = db.StringListProperty(default=None)

    allowed_on_frontpage = db.BooleanProperty(default=False)

    created = db.DateTimeProperty(auto_now_add=True)
    updated = db.DateTimeProperty(auto_now_add=True)
    title = db.StringProperty(default="")
    desc = db.TextProperty(default="")
    logo = db.StringProperty(default="")
    status = db.StringProperty(choices=(u'editing', u'open', u'fulfilled'))
    privacy = db.StringProperty(
        choices=(u'invisible',
            u'friends',
            u'fof',
            u'public',
            u'friendlist',
            u'list',    # TODO remove
            u'group',   # TODO remove
            ))
    posts = db.StringListProperty(default=None)
    deleted_posts = db.StringListProperty(default=None)

    likes = db.IntegerProperty(default=0)
    comments = db.IntegerProperty(default=0)
    shares = db.IntegerProperty(default=0)

    logo_top = db.FloatProperty(default=.0)
    logo_left = db.FloatProperty(default=.0)

    step_count = db.IntegerProperty(default=0)

    received_inactivity_warning = db.BooleanProperty(default=False)

    account_tag = db.StringProperty(default="")
    forbidden_tags = db.StringListProperty(default=None)

    @property
    def small_logo(self):
        if not self.logo:
            return None
        if self.logo.endswith('=s0'):
            return '{0}{1}'.format(self.logo[:-1], 400)
        return self.logo


    def __hash__(self):
        return self.key().id()

    def __eq__(self, other):
        return self.key() == other.key()

    def total_stats(self):
        stats = {
            'likes': self.likes,
            'comments': self.comments,
            'shares': self.shares
            }
        steps = Step.all().filter('wish_id =', self.key().id())
        for step in steps:
            stats['likes'] += step.likes
            stats['comments'] += step.comments
            stats['shares'] += step.shares

        return stats

    @staticmethod
    def get_random_wishes(num):
        q = (Wish.all(keys_only=True)
                .filter('privacy =', 'public')
                .filter('allowed_on_frontpage =', True)
                .order('-updated'))
        pop = q.count(limit=1000)
        if num > pop:
            num = pop
        item_keys = q.fetch(pop)
        random_keys = random.sample(item_keys, num)
        examples = Wish.get(random_keys)
        return examples


    @staticmethod
    def update_friends(wishlist, current, removed):
        for wish in wishlist:
            wish.friends = current

            users_to_remove_from_favorites = set(removed).intersection(wish.favorited)
            for user_id in users_to_remove_from_favorites:
                wish.favorited.remove(user_id)

            super(BaseModel, wish).put()

    def __unicode__(self):
        return self.title

    def get_list(self):
        if self.privacy == 'invisible':
            return None
        if self.privacy in ('fof', 'friends', 'public'):
            return self.user_id
        if self.privacy == 'friendlist':
            return self.friendlist

    def get_friends_from_list(self):
        list_id = self.get_list()
        if list_id:
            friendlist = FriendList.get_by_key_name(list_id)
            if friendlist:
                return friendlist.friends
            else:
                return []
        else:
            return []

    def get_friends(self):
        return self._friends

    @staticmethod
    def get_old_list(wish, old_privacy, old_friendlist):
        if old_privacy == 'invisible':
            return None
        if old_privacy in ('fof', 'friends', 'public'):
            return wish.user_id
        if old_privacy == 'friendlist':
            if old_friendlist:
                return old_friendlist
            else:
                return None

    @staticmethod
    def get_old_friends(wish, old_privacy, old_friendlist):
        list_id = Wish.get_old_list(wish, old_privacy, old_friendlist)
        if list_id:
            friendlist = FriendList.get_by_key_name(list_id)
            if friendlist:
                return friendlist.friends
            else:
                return []
        else:
            return []

    @property
    def url(self):
        """Wish page URL"""
        return conf.EXTERNAL_HREF + u'wish/%s' % self.key().id()

    @property
    def url_relative(self):
        """Wish page URL"""
        return u'wish/%s' % self.key().id()

    @property
    def model_kind(self):
        """Wish page URL"""
        return u'wish'


    @staticmethod
    def empty(user):
        """Return a json serializable representation of the wish"""
        response = {}
        response['user'] = {}
        response['user']['user_id'] = user.key().name()
        response['user']['username'] = user.username
        response['user']['name'] = user.name
        if hasattr(user,'first_name'):
            response['user']['first_name'] = user.first_name
        response['user']['picture'] = user.picture
        response['user']['url'] = user.url

        response['wish_id'] = ''
        response['url'] = ''
        response['title'] = ''
        response['desc'] = ''
        response['logo'] = ''
        response['small_logo'] = ''
        response['status'] = ''
        response['privacy'] = ''
        response['friendlist'] = ''
        response['posts'] = ''

        response['posts'] = ''

        response['user_type'] = user.type
        response['account'] = {}
        return response

    def serialize(self, user=None):
        """Return a json serializable representation of the wish"""
        response = {}

        wish_user = get_user_or_account(self.user_id)
        if hasattr(wish_user, 'admins'):
            response['admins'] = wish_user.admins

        response['user'] = {}
        response['user']['user_id'] = self.user_id
        wish_user = get_user(self.user_id)
        response['user']['username'] = wish_user.username
        response['user']['name'] = wish_user.name
        if hasattr(wish_user, 'first_name'):
            response['user']['first_name'] = wish_user.first_name
        response['user']['picture'] = wish_user.picture
        response['user']['url'] = wish_user.url

        response['user']['first_name'] = self.user_first_name
        response['user']['picture'] = self.user_picture

        response['wish_id'] = self.key().id()
        response['url'] = self.url
        response['title'] = self.title or u''
        response['desc'] = self.desc or u''
        response['logo'] = self.logo or ''
        response['small_logo'] = self.small_logo or ''
        response['status'] = self.status
        response['privacy'] = self.privacy

        response['logoTop'] = self.logo_top
        response['logoLeft'] = self.logo_left

        if (self.privacy == 'friendlist'):
            friendlist = None
            if self.friendlist:
                friendlist = FriendList.get_by_key_name(self.friendlist)
            if friendlist:
                response['friendlist'] = {}
                response['friendlist']['list_id'] = self.friendlist
                response['friendlist']['name'] = friendlist.name


        response['posts'] = self.posts
        response['stats'] = {
                'likes': self.likes,
                'comments': self.comments,
                'shares': self.shares,
                }
        response['total_stats'] = self.total_stats()
        response['step_count'] = self.step_count

        if user:
            if user.key().name() in self.favorited:
                response['favorite'] = True
            else:
                response['favorite'] = False

        response['user_type'] = self.user_type
        if self.user_type == 'account' or self.account_tag:
            account_id = self.account_tag or self.user_id
            account = Account.get_by_key_name(account_id)
            if account:
                response['account'] = {}
                response['account']['id'] = account_id
                response['account']['name'] = account.name
                response['account']['picture'] = account.picture

        return response

    def can_edit(self, user):
        """Returns wether user is allowed to edit this wish"""
        if User.get_association(self.user_id, user) == ASSOC.SELF:
            return True

        if user.key().name() in conf.ADMIN_USER_IDS:
            return True

        return False

    def can_view(self, user=None):
        """Returns wether user is allowed to view this wish"""

        wish_user = get_user_or_account(self.user_id)
        if user and (wish_user.key().name() == user.key().name()):
            return True

        if self.privacy == 'invisible':
            return False

        if self.privacy == 'public':
            return True

        if not user:
            return False

        if self.privacy == 'friendlist':
            # Wish with deleted friendlist
            if not self.friendlist:
                return False

            friendlist = FriendList.get_by_key_name(self.friendlist)
            if user.key().name() in friendlist.friends:
                return True
            else:
                return False
        else:
            # Friends or fof
            assoc = User.get_association(wish_user, user)
            if assoc:
                if self.privacy == 'fof' and assoc in (ASSOC.FOF, ASSOC.FRIEND):
                    return True
                if self.privacy == 'friends' and assoc == ASSOC.FRIEND:
                    return True
                return False
            else:
                return False

        return False

    def refresh_open_graph(self):
        refresh_open_graph(self.url)
    #def refresh_open_graph(self):
    #    """Update wish object open graph data TODO check this out again"""
    #    logging.info('Updating open graph for wish %s', self.key().id())
    #    args = {u'id': self.url,
    #            u'scrape': u'true' }
    #    urlfetch.fetch(
    #        url = u'http://graph.facebook.com?' + urllib.urlencode(args),
    #        method = urlfetch.POST,
    #        deadline = 60,
    #        validate_certificate = True,
    #        )

    def put(self):
        if self.status != 'editing':
            curfriends = Wish.get_friends_from_list(self)
            if (self.isdirty('privacy') or self.isdirty('friendlist')):
                    oldfriends = Wish.get_old_friends(self,
                            self.oldval('privacy'),
                            self.oldval('friendlist'))
                    removed = list(set(oldfriends).difference(curfriends))
            else:
                removed = []

            Wish.update_friends([self], curfriends, removed)
        super(BaseModel, self).put()

    def delete(self):
        """Delete wish callback, deletes indexes."""

        steps = Step.all().filter('wish_id =', self.key().id())
        for step in steps:
            step.delete()

        super(BaseModel, self).delete()

def get_for_user(query_user, user, limit, offset):
    """
    """
    logging.info('user.type: %s', user.type)
    if user.type == 'account':
        logging.debug('account')
        if query_user and query_user.key().name() == user.key().name():
            wishlist = (Wish.all()
                    .filter('user_id =', user.key().name())
                    .order('-updated')
                    .fetch(limit=limit, offset=offset)
                    )
        else:
            wishlist = (Wish.all()
                    .filter('user_id =', user.key().name())
                    .filter('status IN', ('open', 'fulfilled'))
                    .filter('privacy =', 'public')
                    .order('-updated')
                    .fetch(limit=limit+offset, offset=0)
                    )
        rest_wishlist = (Wish.all()
            .filter('account_tag =', user.key().name())
            .filter('status IN', ('open', 'fulfilled'))
            .filter('privacy =', 'public')
            .order('-updated')
            .fetch(limit=limit+offset, offset=0)
            )
        # I need to keep the order (wishes from page first)
        #wishlist.extend(rest_wishlist)
        #wishlist = list(set(wishlist))
        for wish in rest_wishlist:
            if wish not in wishlist:
                wishlist.append(wish)
        wishlist = wishlist[offset:limit+offset]
        #wishlist = sorted(wishlist, key=attrgetter('updated'), reverse=True)
    else:
        logging.debug('user')
        assoc = User.get_association(user, query_user)
        if assoc == ASSOC.SELF:
            wishlist = (Wish.all()
                .filter('user_id =', user.key().name())
                .order('-updated')
                .fetch(limit=limit, offset=offset)
                )
        elif assoc == ASSOC.FRIEND:
            wishlist = (Wish.all()
                    .filter('user_id =', user.key().name())
                    .filter('friends =', query_user.key().name())
                    .filter('status IN', ('open', 'fulfilled'))
                    .order('-updated')
                    .fetch(limit=limit, offset=offset)
                    )
        elif assoc == ASSOC.FOF:
            wishlist = (Wish.all()
                .filter('user_id =', user.key().name())
                .filter('status IN', ('open', 'fulfilled'))
                .filter('privacy IN', ['public', 'fof'])
                .order('-updated')
                .fetch(limit=limit, offset=offset)
                )
        else:
            wishlist = (Wish.all()
                .filter('user_id =', user.key().name())
                .filter('status IN', ('open', 'fulfilled'))
                .filter('privacy =', 'public')
                .order('-updated')
                .fetch(limit=limit, offset=offset)
                )
    return wishlist


class FofImage(BaseModel):
    """Represents an image stored in the datastore"""
    user_id = db.StringProperty(required=True)
    created = db.DateTimeProperty(auto_now_add=True)
    img = blobstore.BlobReferenceProperty(required=True)
    url = db.LinkProperty(required = True)

    def remove(self):
        """Delete image from blobstore"""
        images.delete_serving_url(self.img.key())
        blob_info = blobstore.BlobInfo.get(self.img.key())
        if blob_info:
            blob_info.delete()

    def delete(self):
        self.remove()
        super(BaseModel, self).delete()

    @staticmethod
    def delete_unreferenced_images(user_id):
        """Delete unreferenced images."""
        wish_images = []
        user = get_user_or_account(user_id)
        if user:
            wish_images.append(user.picture)
        for wish in Wish.all().filter('user_id =', user_id):
            wish_images.append(wish.logo)
        for step in Step.all().filter('user_id =', user_id):
            wish_images.append(step.logo)
        account = Account.get_by_key_name(user_id)
        if account:
            wish_images.append(account.wishboard_cover)
        wish_images = set(wish_images)
        for fofimage in FofImage.all().filter('user_id =', user_id):
            if fofimage.url not in wish_images:
                fofimage.delete()


def filter_friends(fb_friends):
    """Get a list of fb user ids and returns those present on mywishguru"""
    fof_friends = []
    fb_friends = set(fb_friends)
    fof_friends = User.get_by_key_name(fb_friends)
    fof_friends = filter(None, fof_friends)
    return [fof_friend.user_id for fof_friend in fof_friends]

class Contact(db.Model):
    user_id = db.StringProperty(required=True)
    name = db.StringProperty()
    picture = db.StringProperty()
    email = db.EmailProperty()

def get_user_or_account(user_id=None):
    user = None
    if not user_id:
        return None

    user = User.get_by_key_name(user_id)
    if not user:
        user = Account.get_by_key_name(user_id)
    return user

def get_user(id_or_username=None):
    user = None
    if not id_or_username:
        return None
    user = User.get_by_key_name(id_or_username)
    if user:
        return user
    user = Account.get_by_key_name(id_or_username)
    if user:
        return user
    user = Account.all().filter('username =', id_or_username).get()
    if user:
        return user
    user = User.all().filter('username =', id_or_username).get()
    if user:
        return user

    return None

def get_user_and_search_parameter(id_or_username=None):
    user = None
    if not id_or_username:
        return None, None

    user = User.get_by_key_name(id_or_username)
    if user:
        return user, 'id'

    user = Account.get_by_key_name(id_or_username)
    if user:
        return user, 'id'

    user = Account.all().filter('username =', id_or_username).get()
    if user:
        return user, 'username'

    user = User.all().filter('username =', id_or_username).get()
    if user:
        return user, 'username'

    return None, None

