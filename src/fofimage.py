#!/usr/bin/env python
# coding: utf-8
"""
Dealing with FOF images
"""
from __future__ import with_statement

#import logging
# Google
from google.appengine.api import images
from django.utils import simplejson as json
# Project
from facebookhandler import BaseHandler
from models import FofImage
#import conf
from aux import user_required
from aux import ImageStoreError
from aux import store_image

#class ImgServer(webapp.RequestHandler):
MAX_SIZE = 600
class ImgServer(BaseHandler):
    """ Image server designed to handle serving images from
    different object properties"""
    csrf_protect = False
    check_access_token = False

    @user_required
    def post(self, image_id=None):
        img=self.request.get(u'img', None)
        iframe = True
        if not img:
            iframe = False
            img = self.request.body

        if not img:
            self.error('500')
            response = json.dumps({'error': 'No image received'})
            if iframe:
                self.response.headers[u'Content-Type'] = u'text/html'
                response = u'<textarea>%s</textarea>' % response
            self.response.out.write(response)
            return

        try:
            image = images.Image(img)
            #logging.info('H: %s, W: %s', image.height, image.width)
            if image.height < 192 or image.width < 289:
                self.error('500')
                response = json.dumps({'error': 'Image must be at least 289 in width and 192 pixels in height'})
                if iframe:
                    self.response.headers[u'Content-Type'] = u'text/html'
                    response = u'<textarea>%s</textarea>' % response
                self.response.out.write(response)
                return
        except images.NotImageError:
            self.error('500')
            response = json.dumps({'error': 'File not image or unrecognized format'})
            if iframe:
                self.response.headers[u'Content-Type'] = u'text/html'
                response = u'<textarea>%s</textarea>' % response
            self.response.out.write(response)
            return

        try:
            (blob_key, url) = store_image(img)
        except ImageStoreError, e:
            self.error('500')
            response = json.dumps({'error': e.message})
            if iframe:
                self.response.headers[u'Content-Type'] = u'text/html'
                response = u'<textarea>%s</textarea>' % response
            self.response.out.write(response)
            return
        except images.TransformationError, e:
            response = json.dumps({'image_id': '', 'url': ''})
            return

        fof_image = FofImage(
                user_id = self.active_user.key().name(),
                img = blob_key,
                url = url)
        fof_image.put()

        response = json.dumps({'image_id': fof_image.key().id(), 'url': url})
        if iframe:
            self.response.headers[u'Content-Type'] = u'text/html'
            response = u'<textarea>%s</textarea>' % response
        self.response.out.write(response)

