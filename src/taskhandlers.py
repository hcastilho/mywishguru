#!/usr/bin/env python
# coding: utf-8
"""Handlers for task operatios"""
import os
import logging
import hmac
import hashlib
import json
import datetime
import httplib

from google.appengine.ext import db
from google.appengine.ext.webapp import template
from google.appengine.ext import  webapp
from google.appengine.api import taskqueue
from google.appengine.api import mail
from google.appengine.api import urlfetch
from google.appengine.ext import deferred
from google.appengine.runtime import DeadlineExceededError
from google.appengine._internal.django.template import TemplateDoesNotExist

import conf
from models import User
from models import Wish
from models import Step
from models import Account
#from models import FriendList
from models import FofImage
from models import get_user_or_account
from facebookhandler import BaseHandler
from facebookhandler import update_user_accounts
import facebook
from facebook import GraphAPIError
from aux import GeneralException


TASKS = []
TASKS.append({
    'title': 'Run conversion',
    'command': '/task/run-conversion',
    })
TASKS.append({
    'title': 'SchemaMigration1',
    'command': '/task/schema-migration1',
    })
TASKS.append({
    'title': 'SchemaMigration2',
    'command': '/task/schema-migration2',
    })
TASKS.append({
    'title': 'SchemaMigration3',
    'command': '/task/schema-migration3',
    })
TASKS.append({
    'title': 'SchemaMigration4',
    'command': '/task/schema-migration4',
    })
TASKS.append({
    'title': 'Set has wishes',
    'command': '/task/set-has-wishes',
    })
TASKS.append({
    'title': 'Set inactivity warning',
    'command': '/task/set-inactivity-warning',
    })
TASKS.append({
    'title': 'Set account tag',
    'command': '/task/set-account-tag',
    })

# https://developers.google.com/appengine/articles/deferred
class Mapper(object):
   # Subclasses should replace this with a model class (eg, model.Person).
   KIND = None

   # Subclasses can replace this with a list of (property, value) tuples to filter by.
   FILTERS = []

   def __init__(self):
       self.to_put = []
       self.to_delete = []

   def map(self, entity):
       """Updates a single entity.

       Implementers should return a tuple containing two iterables (to_update, to_delete).
       """
       return ([], [])

   def finish(self):
       """Called when the mapper has finished, to allow for any final work to be done."""
       pass

   def get_query(self):
       """Returns a query over the specified kind, with any appropriate filters applied."""
       q = self.KIND.all()
       for prop, value in self.FILTERS:
           q.filter("%s" % prop, value)
       q.order("__key__")
       return q

   #def run(self, batch_size=100):
   def run(self, batch_size=50):
       """Starts the mapper running."""
       self._continue(None, batch_size)

   def _batch_write(self):
       """Writes updates and deletes entities in a batch."""
       if self.to_put:
           db.put(self.to_put)
           self.to_put = []
       if self.to_delete:
           db.delete(self.to_delete)
           self.to_delete = []

   def _continue(self, start_key, batch_size):
       q = self.get_query()
       # If we're resuming, pick up where we left off last time.
       if start_key:
           q.filter("__key__ >", start_key)
       # Keep updating records until we run out of time.
       try:
           # Steps over the results, returning each entity and its index.
           for i, entity in enumerate(q):
               map_updates, map_deletes = self.map(entity)
               self.to_put.extend(map_updates)
               self.to_delete.extend(map_deletes)
               # Do updates and deletes in batches.
               if (i + 1) % batch_size == 0:
                   self._batch_write()
               # Record the last entity we processed.
               start_key = entity.key()
           self._batch_write()
       except DeadlineExceededError:
           # Write any unfinished updates to the datastore.
           self._batch_write()
           # Queue a new task to pick up where we left off.
           deferred.defer(self._continue, start_key, batch_size)
           return
       except db.Timeout, e:
           logging.error(e)
           logging.error(type(self))
       except db.BadRequestError, e:
           logging.error(e)
           logging.error(type(self))
       self.finish()


class FbRealtimeHandler(BaseHandler):
    """Handles Facebook Real-time API interactions"""
    csrf_protect = False

    def get(self):
        if (self.request.GET.get(u'setup') == u'1' and
            self.user and conf.ADMIN_USER_IDS.count(self.user.key().id())):
            self.setup_subscription()
            return
        elif (self.request.GET.get(u'setup') == u'2' and
            self.user and conf.ADMIN_USER_IDS.count(self.user.key().id())):
            self.get_subscription()
            return
        elif (self.request.GET.get(u'hub.mode') == u'subscribe' and
              self.request.GET.get(u'hub.verify_token') ==
                  conf.FACEBOOK_REALTIME_VERIFY_TOKEN):
            self.response.out.write(self.request.GET.get(u'hub.challenge'))
            logging.info(
                u'Successful Real-time subscription confirmation ping.')
            return
        self.redirect(u'/')

    def post(self):
        """Facebook tells us that user has changed subscribed fields
           refresh user data"""
        body = self.request.body
        if self.request.headers[u'X-Hub-Signature'] != (u'sha1=' + hmac.new(
            conf.FACEBOOK_APP_SECRET,
            msg=body,
            digestmod=hashlib.sha1).hexdigest()):
            logging.error(
                u'Real-time signature check failed: %s',
                unicode(self.request))
            return
        data = json.loads(body)

        if data[u'object'] == u'user':
            for entry in data[u'entry']:
                user = User.get_by_key_name(entry['id'])
                if user:
                    logging.info('Refresh user %s', user.user_id)
                    deferred.defer(user.refresh)
                logging.info(
                        u'Facebook refresh user data. %s',
                        entry[u'id'])
        else:
            logging.warning(
                    u'Unhandled Real-time ping: %s',
                    body)

    def setup_subscription(self):
        """Setup realtime updates on facebook"""
        access_token = facebook.get_app_access_token(
                conf.FACEBOOK_APP_ID,
                conf.FACEBOOK_APP_SECRET)
        args = {u'access_token': access_token}
        post_args = {
            u'object': u'user',
            u'fields': u'first_name,name,email,friends,picture',
            u'callback_url': conf.EXTERNAL_HREF + u'fbrealtime',
            u'verify_token': conf.FACEBOOK_REALTIME_VERIFY_TOKEN,
        }
        response = facebook.api(
                conf.FACEBOOK_APP_ID + u'/subscriptions',
                args=args,
                post_args=post_args)
        logging.info(
                u'Real-time setup API call response: %s',
                unicode(response))
        self.response.out.write(json.dumps(response))

    def get_subscription(self):
        """Get realtime subscriptions on facebook"""
        access_token = facebook.get_app_access_token(
                conf.FACEBOOK_APP_ID,
                conf.FACEBOOK_APP_SECRET)
        args = {u'access_token': access_token}
        response = facebook.api(
                conf.FACEBOOK_APP_ID + u'/subscriptions',
                args=args)
        logging.info(
                u'Real-time setup API call response: %s',
                unicode(response))
        self.response.out.write(json.dumps(response))


def send_fb_notification(receiver, message, access_token=None):
    if not access_token:
        access_token = facebook.get_app_access_token(
                conf.FACEBOOK_APP_ID,
                conf.FACEBOOK_APP_SECRET)
    try:
        facebook.api(
                receiver.key().name() + u'/notifications',
                args={
                    'access_token': access_token,
                    'href': message['href'],
                    'template': message['template'],
                    },
                method = urlfetch.POST,
                )
    except GraphAPIError, exception:
        logging.warning(exception)

def send_email_notification(receiver, email):
    if not receiver.email:
        return
    mail.send_mail(
            sender = 'MyWishGuru <no-reply@mywishguru.com>',
            to = receiver.email,
            subject = email['subject'],
            body = email['txt'],
            html = email['html'])


def send_notification(
        notification_type,
        receiver,
        transmitter,
        template_data,
        access_token = None,
        ):

    if not receiver:
        logging.warning("Invalid receiver: %s", receiver)
        return

    # If the attr is not defined defaults to true
    #if not hasattr(receiver, notification_type):
    if hasattr(receiver, notification_type) and not getattr(receiver, notification_type):
        logging.info('Notification disabled')
        return

    receivers = []
    if hasattr(receiver, 'admins'):
        receivers = User.get_by_key_name(receiver.admins)
        logging.info('admins: %s', receiver.admins)
    else:
        receivers = [receiver]

    if not receivers:
        return

    if transmitter and transmitter in receivers:
        receivers.remove(transmitter)
        logging.info('receivers no transmitter: %s', receivers)

    if not receivers:
        logging.info('No receivers')
        return

    if receiver.notification_by_facebook:
        try:
            message = render_message(
                    notification_type,
                    template_data)
            for receiver in receivers:
                send_fb_notification(
                        receiver,
                        message,
                        access_token)
        except TemplateDoesNotExist:
            logging.info('%s template does not exist', notification_type)

    if receiver.notification_by_email:
        try:
            email = render_email(
                    notification_type,
                    template_data)
            for receiver in receivers:
                send_email_notification(
                        receiver,
                        email)
        except TemplateDoesNotExist:
            logging.info('%s template does not exist', notification_type)

def render_message(template_name, template_data):
    message = {}
    template_path = os.path.join(
            os.path.dirname(__file__),
            u'notifications',
            '%s.fb' % template_name)
    message['template'] = template.render(template_path, template_data)
    if 'href' in template_data:
        message['href'] = template_data['href']
    else:
        message['href'] = template_data['model'].url_relative
    return message

def render_email(notification_type, template_data):
    message = {}
    subject_path = os.path.join(
            os.path.dirname(__file__),
            u'notifications',
            '%s.email.subject' % notification_type)
    html_path = os.path.join(
            os.path.dirname(__file__),
            u'notifications',
            '%s.email.html' % notification_type)
    txt_path = os.path.join(
            os.path.dirname(__file__),
            u'notifications',
            '%s.email.txt' % notification_type)
    message['subject'] = template.render(subject_path, template_data)
    message['html'] = template.render(html_path, template_data)
    message['txt'] = template.render(txt_path, template_data)
    return message


def notify_wish(request, notification_type):
    transmitter = request.get('transmitter')
    receiver = request.get('receiver')
    wish_id = int(request.get('wish_id'))
    logging.info('notification_type: %s', notification_type)
    logging.info('transmitter: %s', transmitter)
    logging.info('receiver: %s', receiver)
    logging.info('wish_id: %s', wish_id)
    try:
        step_id = int(request.get('step_id'))
        logging.info('step_id: %s', wish_id)
    except (ValueError, TypeError):
        step_id = None
    wish = Wish.get_by_id(wish_id)
    if not wish:
        logging.warning("Invalid Wish: %s", wish_id)
        return

    model = wish
    if step_id:
        step = Step.get_by_id(step_id)
        if not step:
            logging.warning("Invalid step: %s", step_id)
            return
        model = step

    receiver = get_user_or_account(receiver)
    transmitter = get_user_or_account(transmitter)

    send_notification(
            notification_type,
            receiver,
            transmitter,
            {'transmitter': transmitter,
            'receiver': receiver,
            'model': model},
            )


def notify_wish_multiple(request, notification_type):
    transmitter = request.get('transmitter')
    receivers = request.get_all('receivers')
    receivers = list(set(receivers))
    wish_id = int(request.get('wish_id'))
    logging.info('notification_type: %s', notification_type)
    logging.info('transmitter: %s', transmitter)
    logging.info('wish_id: %s', wish_id)
    logging.info('receivers: %s', receivers)
    try:
        step_id = int(request.get('step_id'))
        logging.info('step_id: %s', wish_id)
    except (ValueError, TypeError):
        step_id = None
    wish = Wish.get_by_id(wish_id)
    if not wish:
        logging.warning("Invalid Wish: %s", wish_id)
        return

    model = wish
    if step_id:
        step = Step.get_by_id(step_id)
        if not step:
            logging.warning("Invalid step: %s", step_id)
            return
        model = step

    access_token = facebook.get_app_access_token(
            conf.FACEBOOK_APP_ID,
            conf.FACEBOOK_APP_SECRET)

    transmitter = get_user_or_account(transmitter)
    for receiver_id in receivers:
        receiver = get_user_or_account(receiver_id)
        send_notification(
                notification_type,
                receiver,
                transmitter,
                {'transmitter': transmitter,
                'receiver': receiver,
                'model': model},
                access_token)


TASKS.append(
        {'title': 'Notify Friend Join',
            'command': 'notify-friend-join',
            'params': ('transmitter', 'receivers'),
            },
        )
class NotifyFriendJoin(webapp.RequestHandler):
    def post(self):
        transmitter = self.request.get('transmitter')
        receivers = self.request.get_all('receivers')
        href = 'browse'
        notification_type = 'notification_friend_join'

        transmitter = get_user_or_account(transmitter)
        for receiver_id in receivers:
            receiver = get_user_or_account(receiver_id)
            send_notification(
                    notification_type,
                    receiver,
                    transmitter,
                    {'transmitter': transmitter,
                    'receiver': receiver,
                    'href': href},
                    )

TASKS.append(
        {'title': 'Notify my Join',
            'command': 'notify-my-join',
            'params': ('receiver',),
            },
        )
class NotifyMyJoin(webapp.RequestHandler):
    def post(self):
        receiver = self.request.get('receiver')
        logging.info('receiver %s', receiver)
        receiver = get_user_or_account(receiver)
        notification_type = 'notification_my_join'
        send_notification(
                notification_type,
                receiver,
                None,
                {'receiver': receiver,},
                )


TASKS.append(
        {'title': 'Notify My No Wish',
            'command': 'notify-my-no-wish',
            'params': ('receiver',),
            },
        )
class NotifyMyNoWish(webapp.RequestHandler):
    def post(self):
        receiver = self.request.get('receiver')
        receiver = get_user_or_account(receiver)
        notification_type = 'notification_my_no_wish'
        send_notification(
                notification_type,
                receiver,
                None,
                {'receiver': receiver,},
                )

TASKS.append(
        {'title': 'Notify Tagged Wish',
            'command': 'notify-tagged-wish',
            'params': ('transmitter', 'receiver', 'wish_id'),
            },
        )
class NotifyTaggedWish(webapp.RequestHandler):
    def post(self):
        notify_wish(self.request, 'notification_tagged_wish')

TASKS.append(
        {'title': 'Notify My Wish Share',
            'command': 'notify-my-wish-share',
            'params': ('transmitter', 'receiver', 'wish_id'),
            },
        )
class NotifyMyWishShare(webapp.RequestHandler):
    def post(self):
        notify_wish(self.request, 'notification_my_wish_share')

TASKS.append(
        {'title': 'Notify My Wish Comment',
            'command': 'notify-my-wish-comment',
            'params': ('transmitter', 'receiver', 'wish_id'),
            },
        )
class NotifyMyWishComment(webapp.RequestHandler):
    def post(self):
        logging.info('notify-my-wish-comment')
        notify_wish(self.request, 'notification_my_wish_comment')


TASKS.append(
        {'title': 'Notify My Wish Like',
            'command': 'notify-my-wish-like',
            'params': ('transmitter', 'receiver', 'wish_id'),
            },
        )
class NotifyMyWishLike(webapp.RequestHandler):
    def post(self):
        notify_wish(self.request, 'notification_my_wish_like')

TASKS.append(
        {'title': 'Notify My Wish Fulfill',
            'command': 'notify-my-wish-fulfill',
            'params': ('receiver', 'wish_id'),
            },
        )
class NotifyMyWishFulfill(webapp.RequestHandler):
    def post(self):
        notify_wish(self.request, 'notification_my_wish_fulfill')


TASKS.append(
        {'title': 'Notify My Stopped Wish',
            'command': 'notify-my-stopped-wish',
            'params': ('transmitter', 'receiver', 'wish_id'),
            },
        )
class NotifyMyStoppedWish(webapp.RequestHandler):
    def post(self):
        notify_wish(self.request, 'notification_my_stopped_wish')


TASKS.append(
        {'title': 'Notify Friend Wish Create',
            'command': 'notify-friend-wish-create',
            'params': ('transmitter', 'receivers', 'wish_id'),
            },
        )
class NotifyFriendWishCreate(webapp.RequestHandler):
    def post(self):
        notify_wish_multiple(self.request, 'notification_friend_wish_create')


TASKS.append(
        {'title': 'Notify Friend Wish Fulfill',
            'command': 'notify-friend-wish-fulfill',
            'params': ('transmitter', 'receivers', 'wish_id'),
            },
        )
class NotifyFriendWishFulfill(webapp.RequestHandler):
    def post(self):
        notify_wish_multiple(self.request, 'notification_friend_wish_fulfill')


TASKS.append(
        {'title': 'Notify Favorite Wish Update',
            'command': 'notify-favorite-wish-update',
            'params': ('transmitter', 'receivers', 'wish_id'),
            },
        )
class NotifyFavoriteWishUpdate(webapp.RequestHandler):
    def post(self):
        notify_wish_multiple(self.request, 'notification_favorite_wish_update')


TASKS.append(
        {'title': 'Notify Favorite Wish Fulfill',
            'command': 'notify-favorite-wish-fulfill',
            'params': ('transmitter', 'receivers', 'wish_id'),
            },
        )
class NotifyFavoriteWishFulfill(webapp.RequestHandler):
    def post(self):
        notify_wish_multiple(self.request, 'notification_favorite_wish_fulfill')


TASKS.append(
        {'title': 'Notify Favorite Wish Share',
            'command': 'notify-favorite-wish-share',
            'params': ('transmitter', 'receivers', 'wish_id'),
            },
        )
class NotifyFavoriteWishShare(webapp.RequestHandler):
    def post(self):
        notify_wish_multiple(self.request, 'notification_favorite_wish_share')


TASKS.append(
        {'title': 'Notify Favorite Wish Comment',
            'command': 'notify-favorite-wish-comment',
            'params': ('transmitter', 'receivers', 'wish_id'),
            },
        )
class NotifyFavoriteWishComment(webapp.RequestHandler):
    def post(self):
        notify_wish_multiple(self.request, 'notification_favorite_wish_comment')


TASKS.append(
        {'title': 'Notify Favorite Wish Like',
            'command': 'notify-favorite-wish-like',
            'params': ('transmitter', 'receivers', 'wish_id'),
            },
        )
class NotifyFavoriteWishLike(webapp.RequestHandler):
    def post(self):
        notify_wish_multiple(self.request, 'notification_favorite_wish_like')


TASKS.append(
        {'title': 'Delete wish',
            'command': 'delete-wish',
            'params': ('wish_id',),
            },
        )
class DeleteWish(webapp.RequestHandler):
    def post(self):
        wish_id = int(self.request.get('wish_id'))
        wish = Wish.get_by_id(wish_id)
        if wish:
            wish.delete()
        else:
            logging.error('Invalid id')

TASKS.append(
        {'title': 'Hide wish',
            'command': 'hide-wish',
            'params': ('wish_id',),
            },
        )
class HideWish(webapp.RequestHandler):
    def post(self):
        wish_id = int(self.request.get('wish_id'))
        wish = Wish.get_by_id(wish_id)
        if wish:
            wish.privacy = 'invisible'
            wish.friendlist = None
            wish.favorited = []
            wish.friends = []
            wish.put()
        else:
            logging.error('Invalid id')


TASKS.append(
        {'title': 'Refresh User',
            'command': 'refresh-user',
            'params': ('user_id', ),
            },
        )
class RefreshUserTask(webapp.RequestHandler):
    """Refresh this user's data using the Facebook Graph API"""
    def post(self):
        user_id = self.request.get('user_id')

        if user_id:
            user = User.get_by_key_name(user_id)
            if not user:
                logging.warning(
                        'User not found %s',
                        user_id)
                return
            user.refresh()
        logging.info('Done')


TASKS.append(
        {'title': 'Refresh Account',
            'command': 'refresh-account',
            'params': ('account_id', 'user_id'),
            },
        )
class RefreshAccount(webapp.RequestHandler):
    def post(self):
        account_id = self.request.get('account_id')
        user_id = self.request.get('user_id')

        if account_id and user_id:
            account = Account.get_by_key_name(account_id)
            user = User.get_by_key_name(user_id)
            if not account:
                logging.warning(
                        'Account or user not found %s %s',
                        account_id,
                        user_id)
                return
            update_user_accounts(user, account_id)
        else:
            logging.info('No account or user')
        logging.info('Done')


TASKS.append(
        {'title': 'No wish cron',
            'command': 'cron-no-wish',
            },
        )
class CronNoWish(webapp.RequestHandler):
    def post(self):
        logging.info('created after: %s', (datetime.datetime.now() - datetime.timedelta(days=14)))
        logging.info('created before: %s', (datetime.datetime.now() - datetime.timedelta(days=7)))
        query = (User.all()
                .filter('created <', (datetime.datetime.now() - datetime.timedelta(days=7)))
                .filter('created >', (datetime.datetime.now() - datetime.timedelta(days=14)))
                )
        for user in query:
            if not Wish.all().filter('user_id =', user.key().name()).count(limit=1):
                logging.info('user: %s %s', user.name, user.key().name())
                taskqueue.add(
                    url='/task/notify-my-no-wish',
                    params={'receiver': user.user_id})


TASKS.append(
        {'title': 'Stopped wish cron',
            'command': 'cron-stopped-wish',
            },
        )
class CronStoppedWish(webapp.RequestHandler):
    def post(self):
        logging.info('updated before: %s', (datetime.datetime.now() - datetime.timedelta(days=7)))
        query = (Wish.all()
                .filter('status =', 'open')
                #.filter('privacy !=', 'invisible')
                .filter('received_inactivity_warning =', False)
                .filter('updated <', (datetime.datetime.now() - datetime.timedelta(days=7)))
                #.filter('updated >', (datetime.datetime.now() - datetime.timedelta(days=14)))
                )
        for wish in query:
            taskqueue.add(
                url='/task/notify-my-stopped-wish',
                params={'receiver': wish.user_id,
                    'wish_id': wish.key().id()})
            wish.reveived_inactivity_warning = True
            wish.put()


class DeleteImagesTask(webapp.RequestHandler):
    def get(self):
        taskqueue.add(queue_name='delete-images',
                      url='/task/delete-images')

    def post(self):
        logging.info('Deleting unreferenced images')
        for user in User.all():
            FofImage.delete_unreferenced_images(user.key().name())
        for account in Account.all():
            FofImage.delete_unreferenced_images(account.key().name())
        logging.info('Done')



TASKS.append(
        {'title': 'Log Exception',
            'command': 'log-exception',
            },
        )
class LogException(webapp.RequestHandler):
    def get(self):
        logging.info('log-exception')
        raise GeneralException('Exception message!!!!!')
        #try:
        #    a.users()
        #except Exception, e:
        #    logging.exception('Exception HAHAAHAHA!!')


#TASKS.append({
#    'title': 'Turn On Notifications',
#    'command': 'turn-on-notifications',
#    },
#    )
#class TurnOnNotifications(webapp.RequestHandler):
#    def get(self):
#        taskqueue.add(
#                url=u'/task/turn-on-notifications',
#                )
#
#    def post(self):
#        for user in User.all():
#            user.notifications = True
#            user.put()

TASKS.append({
    'title': 'Remove User',
    'command': '/task/remove-user',
    'params': {'user_id'},
    },
    )
class RemoveUser(webapp.RequestHandler):
    def get(self):
        user_id = self.request.get('user_id')
        taskqueue.add(
                url=u'/task/remove-user',
                params={'user_id': user_id},
                )

    def post(self):
        user_id = self.request.get('user_id')
        user = get_user_or_account(user_id)
        if user:
            logging.info('Deleting user %s %s', user.name, user_id)
            user.delete()
        else:
            logging.info('User not found')


TASKS.append({
    'title': 'Update users',
    'command': '/task/update-users',
    },
    )
class UpdateUsers(webapp.RequestHandler):
    def post(self):
        logging.info('Update Users')
        update_user_mapper = UpdateUserMapper()
        deferred.defer(update_user_mapper.run)
class UpdateUserMapper(Mapper):
    KIND = User
    def map(self, user):
        try:
            user.refresh()
        except DeadlineExceededError:
            raise
        return ([], [])

TASKS.append({
    'title': 'Update accounts',
    'command': '/task/update-accounts',
    },
    )
class UpdateAccounts(webapp.RequestHandler):
    def post(self):
        logging.info('Update Accounts')
        mapper = UpdateAccountsMapper()
        deferred.defer(mapper.run)
class UpdateAccountsMapper(Mapper):
    KIND = Account
    def map(self, model):
        try:
            model.refresh()
        except DeadlineExceededError:
            raise
        return ([], [])


class WishStatsMapper(Mapper):
    KIND = Wish
    def map(self, wish):
        try:
            try:
                user = get_user_or_account(wish.user_id)
                if not user:
                    user = Account.get_by_key_name(wish.user_id)
                    if not user:
                        logging.error('Wish %s has no user', wish.key().id())
                        return ([],[])

                graph = facebook.GraphAPI(user.access_token)
                stats = graph.fql('SELECT url, normalized_url, share_count, like_count, '
                'comment_count, total_count, commentsbox_count, comments_fbid, '
                'click_count FROM link_stat WHERE url="%s"' % wish.url)
                stats = stats[0]
            except Exception:
                stats = {
                        'like_count': 0,
                        'comment_count': 0,
                        'commentsbox_count': 0,
                        'share_count': 0,
                        }
            likes = int(stats['like_count'])
            if likes > wish.likes:
                wish.likes = likes
                wish.updated = datetime.datetime.now()
            comments = int(stats['comment_count']) + int(stats['commentsbox_count'])
            if comments > wish.comments:
                wish.comments = comments
                wish.updated = datetime.datetime.now()
            shares = int(stats['share_count'])
            if shares > wish.shares:
                wish.shares = shares
                wish.updated = datetime.datetime.now()
        except DeadlineExceededError:
            raise

        return ([wish], [])

class StepStatsMapper(Mapper):
    KIND = Step
    def map(self, step):
        user = get_user_or_account(step.user_id)
        if not user:
            logging.info('taskhandlers.py StepStatsMapper.map - Invalid user_id: %s', step.user_id)
        try:
            try:
                graph = facebook.GraphAPI(user.access_token)
                stats = graph.fql('SELECT url, normalized_url, share_count, like_count, '
                'comment_count, total_count, commentsbox_count, comments_fbid, '
                'click_count FROM link_stat WHERE url="%s"' % step.url)
                stats = stats[0]
            except (GraphAPIError, httplib.HTTPException):
                stats = {
                        'like_count': 0,
                        'comment_count': 0,
                        'commentsbox_count': 0,
                        'share_count': 0,
                        }
            likes = int(stats['like_count'])
            update_wish = False
            if likes > step.likes:
                step.likes = likes
                step.updated = datetime.datetime.now()
            comments = int(stats['comment_count']) + int(stats['commentsbox_count'])
            if comments > step.comments:
                step.comments = comments
                step.updated = datetime.datetime.now()
            shares = int(stats['share_count'])
            if shares > step.shares:
                step.shares = shares
                step.updated = datetime.datetime.now()
            if update_wish:
                wish = Wish.get_by_id(step.wish_id)
                wish.updated = datetime.datetime.now()
                wish.put()
        except DeadlineExceededError:
            raise

        return ([step], [])


TASKS.append({
    'title': 'Update Wish Stats',
    'command': '/task/update-wish-stats',
    })
class UpdateWishStats(webapp.RequestHandler):
    def get(self):
        taskqueue.add(url='/task/update-wish-stats')

    def post(self):
        logging.info('Update Wish Stats')
        wish_mapper = WishStatsMapper()
        deferred.defer(wish_mapper.run)

        step_mapper = StepStatsMapper()
        deferred.defer(step_mapper.run)


TASKS.append({
    'title': 'Run Script',
    'command': '/task/run-script',
    })
class RunScript(webapp.RequestHandler):
    def put(self):
        pass


class CommandView(webapp.RequestHandler):
    def get(self):
        data = {'TASKS': TASKS, }
        self.response.out.write(template.render(
            os.path.join(
                os.path.dirname(__file__), u'templates', 'task_admin.html'),
            data))



TASKS.append({
    'title': 'Toggle allowed on frontpage',
    'command': '/task/toggle-allowed',
    'params': {'wish_id'},
    },
    )
class ToggleAllowed(webapp.RequestHandler):
    def post(self):
        wish_id = self.request.get('wish_id')
        wish = Wish.get_by_id(int(wish_id))
        if wish:
            wish.allowed_on_frontpage = not wish.allowed_on_frontpage
            wish.put()
        else:
            logging.info('wish not found')

class UserList(webapp.RequestHandler):
    def get(self):
        cnt = 0
        for user in User.all():
            self.response.out.write('%s\t%s\n' % (user.email, user.name))
            cnt += 1
        self.response.out.write(cnt)

class AccountList(webapp.RequestHandler):
    def get(self):
        cnt = 0
        for user in Account.all():
            self.response.out.write('%s\n' % user.name)
            cnt += 1
        self.response.out.write(cnt)

class UsersWithoutWishes(webapp.RequestHandler):
    def get(self):
        cnt = 0
        for user in User.all():
            if (Wish.all()
                    .filter('user_id =', user.user_id)
                    .filter('privacy !=', 'invisible').count(limit=1)) == 0:
                cnt += 1
                self.response.out.write('%s\t%s\n' % (user.first_name, user.email))
        self.response.out.write(cnt)

class UsersMultipleWishes(webapp.RequestHandler):
    def get(self):
        cnt = 0
        for user in User.all():
            if (Wish.all()
                    .filter('user_id =', user.user_id)
                    .filter('privacy !=', 'invisible').count(limit=2)) == 2:
                cnt += 1
                self.response.out.write(
                        '%s\t%s\n' % (user.first_name, user.email))
        self.response.out.write(cnt)

class WishesInactiveOneWeek(webapp.RequestHandler):
    def get(self):
        cnt = 0
        for wish in (Wish.all()
                .filter('privacy IN', ('friends', 'fof', 'public', 'friendlist',))
                .filter('status =', 'open')
                .filter('updated <', (datetime.datetime.now() - datetime.timedelta(days=7)))):
            user = get_user_or_account(wish.user_id)
            name = user.name
            if hasattr(user, 'first_name'):
                name = user.first_name
            email = ''
            if hasattr(user, 'email'):
                email = user.email
            cnt += 1
            self.response.out.write(
                    '%s\t%s\t%s\t%s\n' % (wish.key().id(), wish.title, name, email))
        self.response.out.write(cnt)


class WishesInactive(webapp.RequestHandler):
    def get(self):
        cnt = 0
        for wish in (Wish.all()
                .filter('privacy IN', ('friends', 'fof', 'public', 'friendlist',))
                .filter('status =', 'open')
                ):
            if wish.updated < (wish.created + datetime.timedelta(hours=1)):
                user = get_user_or_account(wish.user_id)
                name = user.name
                if hasattr(user, 'first_name'):
                    name = user.first_name
                email = None
                if hasattr(user, 'email'):
                    email = user.email
                cnt += 1
                self.response.out.write(
                    '%s\t%s\t%s\t%s\n' % (wish.key().id(), wish.title, name, email))
        self.response.out.write(cnt)

class WishesFulfilled(webapp.RequestHandler):
    def get(self):
        cnt = 0
        for wish in (Wish.all()
                .filter('status =', 'fulfilled')
                ):
            user = get_user_or_account(wish.user_id)
            name = user.name
            email = None
            if hasattr(user, 'email'):
                email = user.email
            cnt += 1
            self.response.out.write(
                '%s\t%s\t%s\t%s\n' % (wish.key().id(), wish.title, name, email))
        self.response.out.write(cnt)

class WishesAll(webapp.RequestHandler):
    def get(self):
        cnt = 0
        for wish in Wish.all():
            user = get_user_or_account(wish.user_id)
            name = user.name
            email = None
            if hasattr(user, 'email'):
                email = user.email
            cnt += 1
            self.response.out.write(
                '%s\t%s\t%s\t%s\n' % (wish.key().id(), wish.title, name, email))
        self.response.out.write(cnt)

class StepsAll(webapp.RequestHandler):
    def get(self):
        cnt = 0
        for step in Step.all():
            user = get_user_or_account(step.user_id)
            name = user.name
            email = None
            if hasattr(user, 'email'):
                email = user.email
            cnt += 1
            self.response.out.write(
                '%s\t%s\t%s\t%s\t%s\n' % (step.wish_id, step.key().id(), step.title, name, email))
        self.response.out.write(cnt)

class AccountWishes(webapp.RequestHandler):
    def get(self):
        cnt = 0
        for account in Account.all():
            for wish in (Wish.all().filter('user_id =', account.key().name())
                .filter('privacy IN', ('friends', 'fof', 'public', 'friendlist',))
                .filter('status =', 'open')):
                user = get_user_or_account(wish.user_id)
                name = user.name
                email = None
                if hasattr(user, 'email'):
                    email = user.email
                cnt += 1
                self.response.out.write(
                    '%s\t%s\t%s\t%s\n' % (wish.key().id(), wish.title, name, email))
        self.response.out.write(cnt)

TASKS.append({
    'title': 'Sync Wish Data',
    'command': '/task/sync-wish-data',
    'params': {'user_id'},
    },
    )
class SyncWishData(webapp.RequestHandler):
    def post(self):
        user_id = self.request.get('user_id')
        user = get_user_or_account(user_id)
        for wish in Wish.all().filter('user_id =', user.key().name()):
            wish.user_name = user.name
            wish.user_picture = user.picture
            wish.put()

TASKS.append({
    'title': 'Get user permissions',
    'command': '/task/get-user-permissions',
    'params': {'user_id'},
    },
    )
class GetUserPermissions(webapp.RequestHandler):
    def post(self):
        user_id = self.request.get('user_id')
        access_token = facebook.get_app_access_token(
                conf.FACEBOOK_APP_ID,
                conf.FACEBOOK_APP_SECRET)
        graph = facebook.GraphAPI(access_token)
        perms = graph.get_object('%s/permissions' % user_id)
        self.response.out.write(perms)
