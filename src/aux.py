#!/usr/bin/env python
# coding: utf-8
"""
Helper functions
"""
import logging
import traceback
from random import randrange
from functools import wraps
from google.appengine.api import urlfetch, taskqueue
from google.appengine.runtime import DeadlineExceededError
from google.appengine.api import images
from google.appengine.api import files

def user_required(fn):
    """Decorator to ensure a user is present"""
    @wraps(fn)
    def wrapper(*args, **kwargs):
        handler = args[0]
        if handler.user:
            return fn(*args, **kwargs)
        handler.render(u'index')
    return wrapper

class CsrfException(Exception):
    """Csrf Exception"""
    pass

class GeneralException(Exception):

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.msg

def log_exception(ex):
    """Internal logging handler to reduce some App Engine noise in errors"""
    msg = ((str(ex) or ex.__class__.__name__) +
            u': \n' + traceback.format_exc() or traceback.print_stack())
    if isinstance(ex, urlfetch.DownloadError) or \
        isinstance(ex, DeadlineExceededError) or \
        isinstance(ex, CsrfException) or \
        isinstance(ex, taskqueue.TransientError):
        logging.warn(msg)
    else:
        logging.error(msg)

def select_random(lst, limit):
    """Select a limited set of random non Falsy values from a list"""
    final = []
    size = len(lst)
    while limit and size:
        index = randrange(min(limit, size))
        size = size - 1
        elem = lst[index]
        lst[index] = lst[size]
        if elem:
            limit = limit - 1
            final.append(elem)
    return final

class ImageStoreError(Exception):
    def __init__(self, message):
        self.message = message
        Exception.__init__(self, self.message)

def store_image(img):
    try:
        image = images.Image(img)
    except images.NotImageError, e:
        logging.warn(e)
        raise
    if image.format == images.JPEG:
        mime_type = 'image/jpeg'
    elif image.format == images.PNG:
        mime_type = 'image/png'
    elif image.format == images.WEBP:
        mime_type = 'image/webp'
    elif image.format == images.BMP:
        mime_type = 'image/bmp'
    elif image.format == images.GIF:
        mime_type = 'image/gif'
    elif image.format == images.ICO:
        mime_type = 'image/tiff'
    elif image.format == images.TIFF:
        mime_type = 'image/jpeg'
    else:
        e = ImageStoreError('File not image or unrecognized format')
        logging.warn(e)
        raise e

    file_name = files.blobstore.create(mime_type=mime_type)
    with files.open(file_name, 'a') as f:
        f.write(img)
    files.finalize(file_name)

    try:
        blob_key = files.blobstore.get_blob_key(file_name)
        blob_url = images.get_serving_url(blob_key, size=0)
    except images.TransformationError, e:
        logging.warn(file_name)
        logging.warn(blob_key)
        logging.warn(e)
        raise

    return (blob_key, blob_url)
