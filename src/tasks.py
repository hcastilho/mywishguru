#!/usr/bin/env python
# coding: utf-8
"""Main dispacher for handlers"""
import logging
from google.appengine.ext import  webapp
from google.appengine.ext.webapp import util
#from google.appengine.ext import ereporter
from libs import ereporter

import conf
from taskhandlers import UserList
from taskhandlers import AccountList
from taskhandlers import UsersWithoutWishes
from taskhandlers import UsersMultipleWishes
from taskhandlers import WishesInactiveOneWeek
from taskhandlers import WishesInactive
from taskhandlers import WishesFulfilled
from taskhandlers import WishesAll
from taskhandlers import StepsAll
from taskhandlers import AccountWishes

from taskhandlers import CommandView
from taskhandlers import DeleteImagesTask
from taskhandlers import RefreshUserTask
from taskhandlers import RefreshAccount
from taskhandlers import UpdateUsers
from taskhandlers import UpdateAccounts
from taskhandlers import RemoveUser
from taskhandlers import RunScript
from taskhandlers import UpdateWishStats
from taskhandlers import ToggleAllowed
from taskhandlers import DeleteWish
from taskhandlers import HideWish

from taskhandlers import CronNoWish
from taskhandlers import CronStoppedWish

from taskhandlers import NotifyTaggedWish
from taskhandlers import NotifyFriendJoin
from taskhandlers import NotifyMyJoin
from taskhandlers import NotifyMyWishShare
from taskhandlers import NotifyMyWishComment
from taskhandlers import NotifyMyWishLike
from taskhandlers import NotifyMyWishFulfill
from taskhandlers import NotifyMyNoWish
from taskhandlers import NotifyMyStoppedWish
from taskhandlers import NotifyFriendWishCreate
from taskhandlers import NotifyFriendWishFulfill
from taskhandlers import NotifyFavoriteWishUpdate
from taskhandlers import NotifyFavoriteWishFulfill
from taskhandlers import NotifyFavoriteWishShare
from taskhandlers import NotifyFavoriteWishComment
from taskhandlers import NotifyFavoriteWishLike

from taskhandlers import SyncWishData
from taskhandlers import GetUserPermissions

from migration import RunConversion
from migration import SchemaMigration1
from migration import SchemaMigration2
from migration import SchemaMigration3
from migration import SetHasWishes
from migration import SetInactivityWarning
from migration import SetAccountTag

#Load custom Django template filters
webapp.template.register_template_library('filters.filters')

ereporter.register_logger()

def main():
    """Dispatcher"""
    routes = [
        (r'/task/delete-images', DeleteImagesTask),
        (r'/task/refresh-user', RefreshUserTask),
        (r'/task/refresh-account', RefreshAccount),
        (r'/task/update-users', UpdateUsers),
        (r'/task/update-accounts', UpdateAccounts),
        (r'/task/remove-user', RemoveUser),
        (r'/task/run-script', RunScript),
        (r'/task/update-wish-stats', UpdateWishStats),
        (r'/task/toggle-allowed', ToggleAllowed),
        (r'/task/get-user-permissions', GetUserPermissions),
        (r'/task/sync-wish-data', SyncWishData),
        (r'/task/delete-wish', DeleteWish),
        (r'/task/hide-wish', HideWish),

        (r'/task/cron-no-wish', CronNoWish),
        (r'/task/cron-stopped-wish', CronStoppedWish),


        (r'/task/notify-tagged-wish', NotifyTaggedWish),
        (r'/task/notify-friend-join', NotifyFriendJoin),
        (r'/task/notify-my-join', NotifyMyJoin),
        (r'/task/notify-my-wish-share', NotifyMyWishShare),
        (r'/task/notify-my-wish-comment', NotifyMyWishComment),
        (r'/task/notify-my-wish-like', NotifyMyWishLike),
        (r'/task/notify-my-wish-fulfill', NotifyMyWishFulfill),
        (r'/task/notify-my-no-wish', NotifyMyNoWish),
        (r'/task/notify-my-stopped-wish', NotifyMyStoppedWish),
        (r'/task/notify-friend-wish-create', NotifyFriendWishCreate),
        (r'/task/notify-friend-wish-fulfill', NotifyFriendWishFulfill),
        (r'/task/notify-favorite-wish-update', NotifyFavoriteWishUpdate),
        (r'/task/notify-favorite-wish-fulfill', NotifyFavoriteWishFulfill),
        (r'/task/notify-favorite-wish-share', NotifyFavoriteWishShare),
        (r'/task/notify-favorite-wish-comment', NotifyFavoriteWishComment),
        (r'/task/notify-favorite-wish-like', NotifyFavoriteWishLike),


        (r'/task/task-admin', CommandView),
        (r'/task/userlist.txt', UserList),
        (r'/task/accountlist.txt', AccountList),
        (r'/task/users_without_wishes.txt', UsersWithoutWishes),
        (r'/task/users_multiple_wishes.txt', UsersMultipleWishes),
        (r'/task/wishes_inactive_one_week.txt', WishesInactiveOneWeek),
        (r'/task/wishes_inactive.txt', WishesInactive),
        (r'/task/wishes_fulfilled.txt', WishesFulfilled),
        (r'/task/wishes_all.txt', WishesAll),
        (r'/task/steps_all.txt', StepsAll),
        (r'/task/account_wishes.txt', AccountWishes),
        # Page not found
        #(r'/(.*)', PageNotFoundHandler),

        (r'/task/run-conversion', RunConversion),
        (r'/task/schema-migration1', SchemaMigration1),
        (r'/task/schema-migration2', SchemaMigration2),
        (r'/task/schema-migration3', SchemaMigration3),
        (r'/task/set-has-wishes', SetHasWishes),
        (r'/task/set-inactivity-warning', SetInactivityWarning),
        (r'/task/set-account-tag', SetAccountTag),
    ]
    application = webapp.WSGIApplication(routes, debug=conf.DEBUG)
    util.run_wsgi_app(application)


if __name__ == u'__main__':
    if conf.DEBUG:
        logging.getLogger().setLevel(logging.DEBUG)
    else:
        logging.getLogger().setLevel(logging.INFO)
    main()
