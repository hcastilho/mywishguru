#!/usr/bin/env python
# coding: utf-8
"""
RESTful server
"""

# TODO Content-Range
# http://dojotoolkit.org/reference-guide/1.7/dojo/store/JsonRest.html

import logging
import re
import datetime
from operator import attrgetter
#import urlparse
#from posixpath import basename, dirname
from google.appengine.api import taskqueue
from google.appengine.api import memcache
import json
import conf
from models import User
from models import Account
from models import Wish
from models import Step
from models import FriendList
from models import get_for_user
from models import get_user_or_account
from models import get_user
from aux import user_required
from facebookhandler import BaseHandler
from facebook import refresh_open_graph

MEMCACHE_SHORT_TIMEOUT = 1200 # 20 minutes
MEMCACHE_LONG_TIMEOUT = 86400 # 1 day

MSG_ERROR_AUTH = {u'error': u'Fatal', u'msg': u'Unauthorized query'}
MSG_ERROR_SUP = {u'error': u'Fatal', u'msg': u'Unsuported query'}

def jsonify(func):
    def wrapper(self, *args, **kwargs):
        result = func(self, *args, **kwargs)
        self.response.out.write(json.dumps(result))
    return wrapper

class PropertyException(Exception):
    def __init__(self, sev, msg):
        self.sev = sev
        self.msg = msg
    def __str__(self):
        return u'%s: %s' % self.sev, self.msg

class RestBaseHandler(BaseHandler):
    def initialize(self, request, response):
        """General initialization for every request"""
        super(RestBaseHandler, self).initialize(request, response)

        # TODO remove this
        self.fields = []
        if self.request.get('fields', None):
            self.fields = self.request.get('fields').split(',')

        self.offset = 0
        self.limit = conf.PAGE_SIZE

        # TODO and probably this too
        # From query
        if self.request.get('offset'):
            self.offset = int(self.request.get('offset'))
        if self.request.get('limit'):
            self.limit = int(self.request.get('limit'))

        # From html Range header
        if self.request.headers.get(u'Range'):
            regex = re.compile(r'(?P<key>\w+)=(?P<start>\d+)-(?P<end>\d+)')
            rang = regex.search(self.request.headers.get('Range'))
            if rang:
                rang = rang.groupdict()
                self.offset = int(rang[u'start'])
                self.limit = int(rang[u'end'])-int(rang[u'start'])+1


class RestUserHandler(RestBaseHandler):
    @jsonify
    def get(self, id_or_username):
        user = get_user(id_or_username)
        if not user:
            #self.response.out.write(json.dumps({'error': 'Fatal'}))
            return {'error': 'Fatal'}

        if (not self.active_user or self.active_user.key().name() != user.key().name()):
            user_data = {
                'name': user.name,
                'picture': user.picture,
                'user_id': user.key().name(),
                'username': user.username,
                'url': user.url,
            }
            if hasattr(user, 'first_name'):
                user_data['first_name'] = user.first_name
            if hasattr(user, 'wishboard_cover'):
                user_data['wishboard_cover'] = user.wishboard_cover

        else:
            user_data = {
                'name': self.active_user.name,
                'picture': self.active_user.picture,
                'user_id': self.active_user.key().name(),
                'username': self.active_user.username,
                'url': user.url,
                'notification_by_facebook':
                    self.active_user.notification_by_facebook,
                'notification_by_email':
                    self.active_user.notification_by_email,
                'notification_my_wish_share':
                    self.active_user.notification_my_wish_share,
                'notification_my_wish_comment':
                    self.active_user.notification_my_wish_comment,
                'notification_my_wish_like':
                    self.active_user.notification_my_wish_like,
                'notification_favorite_wish_update':
                    self.active_user.notification_favorite_wish_update,
                'notification_favorite_wish_fulfill':
                    self.active_user.notification_favorite_wish_fulfill,
                'notification_favorite_wish_share':
                    self.active_user.notification_favorite_wish_share,
                'notification_favorite_wish_comment':
                    self.active_user.notification_favorite_wish_comment,
                'notification_favorite_wish_like':
                    self.active_user.notification_favorite_wish_like,
                }
            if hasattr(self.active_user, 'notification_friend_join'):
                user_data['notification_friend_join'] = \
                        self.active_user.notification_friend_join,
            if hasattr(self.active_user, 'notification_friend_wish_create'):
                user_data['notification_friend_wish_create'] = \
                        self.active_user.notification_friend_wish_create,
            if hasattr(self.active_user, 'notification_friend_wish_fulfill'):
                user_data['notification_friend_wish_fulfill'] = \
                        self.active_user.notification_friend_wish_fulfill,
            if hasattr(self.active_user, 'first_name'):
                user_data['first_name'] = self.active_user.first_name
            if hasattr(self.active_user, 'wishboard_cover'):
                user_data['wishboard_cover'] = self.active_user.wishboard_cover
        #self.response.out.write(json.dumps(user_data))
        return user_data

    @user_required
    def put(self, user_id):
        body = json.loads(self.request.body)

        if user_id == self.active_user.key().name():
            if body.get('wishboard_cover'):
                self.active_user.wishboard_cover = body.get('wishboard_cover')

            if body.get('notification_by_facebook') != None:
                self.active_user.notification_by_facebook = \
                    body.get('notification_by_facebook')
            if body.get('notification_by_email') != None:
                self.active_user.notification_by_email = \
                    body.get('notification_by_email')

            if body.get('notification_friend_join') != None:
                self.active_user.notification_friend_join = \
                    body.get('notification_friend_join')

            if body.get('notification_my_wish_share') != None:
                self.active_user.notification_my_wish_share = \
                    body.get('notification_my_wish_share')
            if body.get('notification_my_wish_comment') != None:
                self.active_user.notification_my_wish_comment = \
                    body.get('notification_my_wish_comment')
            if body.get('notification_my_wish_like') != None:
                self.active_user.notification_my_wish_like = \
                    body.get('notification_my_wish_like')

            if body.get('notification_friend_wish_create') != None:
                self.active_user.notification_friend_wish_create = \
                    body.get('notification_friend_wish_create')
            if body.get('notification_friend_wish_fulfill') != None:
                self.active_user.notification_friend_wish_fulfill = \
                    body.get('notification_friend_wish_fulfill')

            if body.get('notification_favorite_wish_update') != None:
                self.active_user.notification_favorite_wish_update = \
                    body.get('notification_favorite_wish_update')
            if body.get('notification_favorite_wish_fulfill') != None:
                self.active_user.notification_favorite_wish_fulfill = \
                    body.get('notification_favorite_wish_fulfill')
            if body.get('notification_favorite_wish_share') != None:
                self.active_user.notification_favorite_wish_share = \
                    body.get('notification_favorite_wish_share')
            if body.get('notification_favorite_wish_comment') != None:
                self.active_user.notification_favorite_wish_comment = \
                    body.get('notification_favorite_wish_comment')
            if body.get('notification_favorite_wish_like') != None:
                self.active_user.notification_favorite_wish_like = \
                    body.get('notification_favorite_wish_like')

            self.active_user.put()
            self.response.out.write(json.dumps({'user_id': self.active_user.key().name()}))
            return

        self.response.out.write(json.dumps({'error': 'Fatal'}))

class StepHandler(RestBaseHandler):
    def get(self, step_id):
        if step_id:
            step_id = int(step_id)
            step = Step.get_by_id(step_id)
            wish = Wish.get_by_id(step.wish_id)
            if step:
                if (self.active_user and wish.can_view(self.active_user)):
                    self.response.out.write(
                            json.dumps(step.serialize()))
                    return
                elif wish.can_view():
                    self.response.out.write(
                            json.dumps(step.serialize()))
                    return
                else:
                    self.response.out.write(json.dumps(MSG_ERROR_AUTH))
                    return
            else:
                self.response.out.write(json.dumps(MSG_ERROR_AUTH))
                return

    # Update
    @user_required
    def put(self, step_id):
        body = json.loads(self.request.body)
        updated = False

        step = Step.get_by_id(body.get('step_id'))
        if not step:
            self.response.out.write(json.dumps(
                {'error': 'Fatal',
                'msg': 'Invalid Step',
                }))
            return
        wish = Wish.get_by_id(step.wish_id)
        if not wish:
            self.response.out.write(json.dumps(
                {'error': 'Fatal',
                'msg': 'Invalid wish',
                }))
            return

        # This has to be here to allow anyone to add posts
        if body.get(u'post'):
            if body.get(u'post') not in step.posts:
                step.posts.insert(0, body.get(u'post'))
                step.updated = datetime.datetime.now()
                step.put()
                wish.updated = datetime.datetime.now()
                wish.put()

                # When step is shared by an unregistered user
                active_user = None
                if self.active_user:
                    active_user = self.active_user.key().name()

                # Share Notifications
                #if self.active_user and self.active_user.key().name() != wish.user_id:
                wish_user = get_user_or_account(wish.user_id)
                taskqueue.add(
                    url='/task/notify-my-wish-share',
                    params={
                        'transmitter': active_user,
                        'receiver': wish_user.key().name(),
                        'wish_id': wish.key().id(),
                        'step_id': step.key().id()})

                #receivers = set(wish.favorited)
                #if self.active_user and self.active_user.key().name() in receivers:
                #    receivers.remove(self.active_user.key().name())
                taskqueue.add(
                    url='/task/notify-favorite-wish-share',
                    params={
                        'transmitter': active_user,
                        'receivers': wish.favorited,
                        'wish_id': wish.key().id(),
                        'step_id': step.key().id()})

                return self.response.out.write(
                        json.dumps({
                            'wish_id': step.wish_id,
                            'step_id': step.key().id()
                            }))

        if not wish.can_edit(self.active_user):
            self.response.out.write(json.dumps(
                {'error': 'Fatal',
                'msg': 'Permission Error',
                }))
            return

        if body.get(u'status'):
            if (step.status != body.get('status') and
                body.get('status') == 'fulfilled' and
                wish.privacy != 'invisible'):
                logging.info('notify-favorite-wish-fulfill')
                taskqueue.add(
                    url='/task/notify-favorite-wish-fulfill',
                    params={
                        'transmitter': wish.user_id,
                        'receivers': wish.favorited,
                        'wish_id': wish.key().id(),
                        'step_id': step.key().id()})

                logging.info('notify-friend-wish-fulfill')
                friends = set(wish.get_friends()).difference(wish.favorited)
                taskqueue.add(
                    url='/task/notify-friend-wish-fulfill',
                    params={
                        'transmitter': wish.user_id,
                        'receivers': friends,
                        'wish_id': wish.key().id(),
                        'step_id': step.key().id()})
            step.status = body.get(u'status')

        if (body.get(u'logo') and
        body.get(u'logo') == conf.DEFAULT_IMAGE and
        body.get(u'logo') == conf.DEFAULT_THUMB):
            del body['logo']

        for key,value in body.iteritems():
            if key in ['_csrf_token', 'wish_id', 'step_id', 'status', 'logoTop', 'logoLeft']:
                continue
            updated = True
            setattr(step, key, value)

        if body.get(u'logoTop') == 0 or body.get(u'logoTop'):
            step.logo_top = float(body.get(u'logoTop'))
        if body.get(u'logoLeft') == 0 or body.get(u'logoLeft'):
            step.logo_left = float(body.get(u'logoLeft'))

        wish.updated = datetime.datetime.now()
        wish.put()
        step.updated = datetime.datetime.now()
        step.put()
        self.response.out.write(json.dumps({u'wish_id': wish.key().id(),
                                            u'step_id': step.key().id()}))


        if updated:
            refresh_open_graph(step.url)
            taskqueue.add(
                url='/task/notify-favorite-wish-update',
                params={
                    'transmitter': wish.user_id,
                    'receivers': wish.favorited,
                    'wish_id': wish.key().id(),
                    'step_id': step.key().id()})

        return

    # New
    @user_required
    def post(self, wish_id=None):
        body = json.loads(self.request.body)

        wish = Wish.get_by_id(int(body['wish_id']))
        if not wish:
            self.response.out.write(json.dumps(
                {'error': 'Fatal',
                'msg': 'Invalid Wish',
                }))
            return
        if not wish.can_edit(self.active_user):
            self.response.out.write(json.dumps(
                {'error': 'Fatal',
                'msg': 'Permission Error',
                }))
            return

        if (body.get(u'logo') == conf.DEFAULT_IMAGE or
            body.get(u'logo') == conf.DEFAULT_THUMB):
            body[u'logo'] = ''

        step = Step(
            user_id = self.active_user.key().name(),
            wish_id = body.get('wish_id'),
            index = body.get('index'),
            title = body.get(u'title'),
            logo = body.get(u'logo', ''),
            desc = body.get(u'desc', ''),
            content = body.get(u'content', ''),
            status = body.get('status') or 'open')

        if body.get(u'logoTop') == 0 or body.get(u'logoTop'):
            step.logo_top = float(body.get(u'logoTop'))
        if body.get(u'logoLeft') == 0 or body.get(u'logoLeft'):
            step.logo_left = float(body.get(u'logoLeft'))
        step.put()

        wish.step_count = wish.step_count + 1

        wish.updated = datetime.datetime.now()
        wish.put()

        if (wish.status != 'editing' and wish.privacy != 'invisible' and
            (wish.created < (datetime.datetime.now() - datetime.timedelta(minutes = 5)))):
            taskqueue.add(
                url='/task/notify-friend-wish-create',
                params={
                    'transmitter': wish.user_id,
                    'receivers': wish.favorited,
                    'wish_id': wish.key().id(),
                    'step_id': step.key().id()})

            friends = set(wish.get_friends()).difference(wish.favorited)
            taskqueue.add(
                url='/task/notify-friend-wish-create',
                params={
                    'transmitter': wish.user_id,
                    'receivers': friends,
                    'wish_id': wish.key().id(),
                    'step_id': step.key().id()})

        self.response.out.write(json.dumps({u'wish_id': wish.key().id(),
                                            u'step_id': step.key().id()}))
        return

    @user_required
    def delete(self, step_id):
        if not step_id:
            self.response.out.write(json.dumps(
                {'error': 'Fatal',
                'msg': 'Invalid Step',
                }))
            return

        step = Step.get_by_id(int(step_id))
        if not step:
            self.response.out.write(json.dumps(
                {'error': 'Fatal',
                'msg': 'Invalid Step',
                }))
            return

        wish = Wish.get_by_id(step.wish_id)
        if not wish:
            self.response.out.write(json.dumps(
                {'error': 'Fatal',
                'msg': 'Invalid wish',
                }))
            return
        if not wish.can_edit(self.active_user):
            self.response.out.write(json.dumps(
                {'error': 'Fatal',
                'msg': 'Permission Error',
                }))
            return

        logging.debug('Deleting wish %s %s', step.wish_id, wish.title)
        step.delete()

        wish.step_count = wish.step_count - 1
        wish.updated = datetime.datetime.now()
        wish.put()

        self.response.out.write(json.dumps({
            u'wish_id': step.wish_id,
            'step_id': step_id
            }))
        return


class RestFavoritesHandler(RestBaseHandler):
    @user_required
    def put(self, wish_id):
        wish_id = int(wish_id)
        body = json.loads(self.request.body)

        wish = Wish.get_by_id(wish_id)
        if not wish:
            logging.warning('Tried to (un)favorite undefined wish %s' % wish_id)
            return
        if body['favorite'] == False:
            if self.active_user.key().name() in wish.favorited:
                wish.favorited.remove(self.active_user.key().name())
                wish.put()

        if body['favorite'] == True:
            if wish and wish.can_view(self.active_user):
                if self.active_user.key().name() not in wish.favorited:
                    wish.favorited.append(self.active_user.key().name())
                    wish.put()

        self.response.out.write(json.dumps({u'wish_id': wish.key().id()}))
        return


class FriendsHandler(RestBaseHandler):
    @user_required
    def get(self):
        """Returns a list of the querying user friends user_id's filtering
        users without wishes"""
        friends = memcache.get('%s::friends' % self.active_user.key().name())
        if not friends:
            friendlist = FriendList.get_by_key_name(self.active_user.key().name())
            if not friendlist:
                self.response.out.write(json.dumps({'error': 'No friend list'}))
                return

            friends = []
            for friend_id in friendlist.friends:
                friend = User.get_by_key_name(friend_id)
                has_wish = (Wish.all()
                        .filter('user_id =', friend_id)
                        .filter('friends =', self.active_user.key().name())
                        .filter('status IN', ('open', 'fulfilled'))
                        .order('-updated')
                        .count(limit=1)
                        )
                logging.debug('has wish: %s', has_wish)
                if friend:
                    friends.append({
                        'user_id': friend.key().name(),
                        'first_name': friend.first_name,
                        'name': friend.name,
                        'picture': friend.picture,
                        'username': friend.username,
                        'url': friend.url,
                        'hasWish': True if has_wish else False,
                        })
            friends = json.dumps(friends)
            if not memcache.add('%s::friends' % self.active_user.key().name(),
                    friends,
                    MEMCACHE_SHORT_TIMEOUT):
                logging.error('Memcache set failed friends %s.', self.active_user.key().name())
        self.response.out.write(friends)

class FriendlistHandler(RestBaseHandler):
    @user_required
    def get(self, list_id=None):
        friendlists = memcache.get('%s::friendlists' % self.active_user.key().name())
        if not friendlists:
            friendlists = FriendList.all().filter('user_id =', self.active_user.key().name())
            #self.active_user.refresh_friendlists()
            fdict = []
            for friendlist in friendlists:
                fdict.append({
                        'list_id': friendlist.list_id,
                        'name': friendlist.name,
                    })
            friendlists = json.dumps(fdict)
            if not memcache.add('%s::friendlists' % self.active_user.key().name(),
                    friendlists,
                    MEMCACHE_SHORT_TIMEOUT):
                logging.error('Memcache set failed friendlists %s.', self.active_user.key().name())
        self.response.out.write(friendlists)

class AccountHandler(RestBaseHandler):
    def get(self, query=None):
        wish = None
        if query:
            wish_id = query.split('=')[1]
            wish = Wish.get_by_id(int(wish_id))
            accounts = (Account.all()
                    .filter('has_wishes =', True)
                    )
            accounts_list = []
            for account in accounts:
                if wish and account.key().name() in wish.forbidden_tags:
                    continue
                accounts_list.append({
                        'id': account.key().name(),
                        'name': account.name,
                        'picture': account.picture,
                        'wishboard_cover': account.wishboard_cover,
                        'username': account.username,
                        'url': account.url,
                    })
            self.response.out.write(json.dumps(accounts_list))
            return
        else:
            accounts = memcache.get('accounts')
            if not accounts:
                accounts = (Account.all()
                        .filter('has_wishes =', True)
                        )
                accounts_list = []
                for account in accounts:
                    accounts_list.append({
                            'id': account.key().name(),
                            'name': account.name,
                            'picture': account.picture,
                            'wishboard_cover': account.wishboard_cover,
                            'username': account.username,
                            'url': account.url,
                        })
                accounts = json.dumps(accounts_list)
                if not memcache.add('accounts', accounts):
                    logging.error('Memcache set failed accounts.')
            self.response.out.write(accounts)
            return


    @user_required
    def delete(self, account_id):
        account = Account.get_by_key_name(account_id)
        if not account:
            self.response.out.write(json.dumps(MSG_ERROR_AUTH))
            return
        if self.user.key().name() in account.admins:
            account.delete()



class LikeStore(RestBaseHandler):
    csrf_protect = False
    def put(self, wish_id):
        body = json.loads(self.request.body)
        user_id = body.get('user_id')
        try:
            wish_id = int(body.get('wish_id'))
        except (ValueError, TypeError):
            return
        wish = Wish.get_by_id(wish_id)
        if wish:
            wish.updated = datetime.datetime.now()
            wish.put()
        if not wish:
            return
        try:
            step_id =  int(body.get('step_id'))
            step = Step.get_by_id(wish_id)
            if step:
                step.updated = datetime.datetime.now()
                step.put()
        except (ValueError, TypeError):
            step_id = None


        taskqueue.add(
            url='/task/notify-my-wish-like',
            params={'transmitter': user_id,
                    'receiver': wish.user_id,
                    'wish_id': wish_id,
                    'step_id': step_id})

        taskqueue.add(
            url='/task/notify-favorite-wish-like',
            params={'transmitter': user_id,
                    'receivers': wish.favorited,
                    'wish_id': wish_id,
                    'step_id': step_id})

        self.response.out.write(json.dumps({u'wish_id': wish_id}))
        return


class WishStore(RestBaseHandler):
    def get(self, wish_id=None):

        # Request a particular wish
        if wish_id:
            wish = Wish.get_by_id(int(wish_id))
            if wish and wish.can_view(self.active_user):
                self.response.out.write(json.dumps(wish.serialize(self.active_user)))
                return

        wishlist = []
        # Query by user_id
        if self.request.get('user'):
            id_or_username = self.request.get('user')
            wishlist = []
            user = get_user(id_or_username)
            wishlist = get_for_user(
                self.active_user,
                user,
                self.limit,
                self.offset,
                )
            wishlist = [_wish.serialize(self.active_user) for _wish in wishlist]
        # Other queries
        elif self.request.get('type'):
            query_type = self.request.get('type')

            if query_type == 'favorites':
                wishlist = (Wish.all().filter('favorited =', self.active_user.key().name())
                        .order('-updated')
                        .fetch(limit=self.limit, offset=self.offset)
                        )
                wishlist = [_wish.serialize(self.active_user) for _wish in wishlist]

            if query_type == 'everyone':
                if not self.active_user or self.active_user.type == 'account':
                    wishlist = (Wish.all()
                        .filter('status IN', ('open', 'fulfilled'))
                        .filter('privacy =', 'public')
                        .order('-updated')
                        .fetch(limit=self.limit, offset=self.offset))
                else:
                    wishlist = (Wish.all()
                            .filter('friends =', self.active_user.key().name())
                            .filter('status IN', ('open', 'fulfilled'))
                            .order('-updated')
                            .fetch(limit=self.limit, offset=self.offset))

            if query_type == 'public':
                wishlist = (Wish.all()
                    .filter('status IN', ('open', 'fulfilled'))
                    .filter('privacy =', 'public')
                    .order('-updated')
                    .fetch(limit=self.limit, offset=self.offset)
                    )
                wishlist = [_wish.serialize(self.active_user) for _wish in wishlist]

            if query_type == 'friends':
                wishlist = (Wish.all()
                        .filter('friends =', self.active_user.key().name())
                        .filter('status IN', ('open', 'fulfilled'))
                        .order('-updated')
                        .fetch(limit=self.limit, offset=self.offset)
                        )
                wishlist = [_wish.serialize(self.active_user) for _wish in wishlist]

            if query_type == 'random_public':
                wishlist = Wish.get_random_wishes(9)
                wishlist = [_wish.serialize(self.active_user) for _wish in wishlist]

            if query_type == 'pages':
                wishlist = (Wish.all()
                        .filter('user_type =', 'account')
                        .filter('status IN', ('open', 'fulfilled'))
                        .order('-updated')
                        .fetch(limit=self.limit, offset=self.offset)
                        )
                wishlist = [_wish.serialize(self.active_user) for _wish in wishlist]
        else:
            if self.active_user:
                wishlist = get_for_user(
                    self.active_user,
                    self.active_user,
                    self.limit,
                    self.offset,
                    )
                wishlist = [_wish.serialize(self.active_user) for _wish in wishlist]
            else:
                logging.warning('Wishlist without user')
                wishlist = []

        self.response.out.write(json.dumps(wishlist))

    # Update
    def put(self, wish_id):
        body = json.loads(self.request.body)
        updated = False

        if not wish_id:
            self.response.out.write(json.dumps(MSG_ERROR_SUP))
            return

        wish_id = int(wish_id)
        wish = Wish.get_by_id(wish_id)
        if not wish:
            self.error(404)

        # This has to be here to allow anyone to add posts
        if body.get(u'post'):
            if body.get(u'post') not in wish.posts:
                wish.posts.insert(0, body.get(u'post'))
                wish.updated = datetime.datetime.now()
                wish.put()

                # When wish is shared by an unregistered user
                active_user = None
                if self.active_user:
                    active_user = self.active_user.key().name()

                # Share Notifications
                taskqueue.add(
                    url='/task/notify-my-wish-share',
                    params={
                        'transmitter': active_user,
                        'receiver': wish.user_id,
                        'wish_id': wish.key().id()})

                taskqueue.add(
                    url='/task/notify-favorite-wish-share',
                    params={
                        'transmitter': active_user,
                        'receivers': wish.favorited,
                        'wish_id': wish.key().id()})

                return self.response.out.write(
                        json.dumps({'wish_id': wish.key().id()}))

        if (self.active_user.key().name() == wish.account_tag):
            if 'remove_account' in body:
                wish.forbidden_tags.append(wish.account_tag)
                wish.account_tag = None
                wish.put()
                return self.response.out.write(
                        json.dumps({'wish_id': wish.key().id()}))

        if (not self.active_user) or (not wish.can_edit(self.active_user)):
            self.response.out.write(json.dumps(MSG_ERROR_AUTH))
            return

        #refresh_open_graph = False

        if body.get(u'title') and (wish.title != body.get(u'title')):
            wish.title = body.get(u'title')[:500]
            #refresh_open_graph = True
            updated = True
        if body.get(u'desc') and (wish.desc != body.get(u'desc')):
            wish.desc = body.get(u'desc')[:2000]
            #refresh_open_graph = True
            updated = True

        if (body.get(u'logo') and
        body.get(u'logo') != conf.DEFAULT_IMAGE and
        body.get(u'logo') != conf.DEFAULT_IMAGE):
            wish.logo = body.get(u'logo')
            #refresh_open_graph = True
            updated = True
        if body.get(u'logoTop') == 0 or body.get(u'logoTop'):
            wish.logo_top = float(body.get(u'logoTop'))
        if body.get(u'logoLeft') == 0 or body.get(u'logoLeft'):
            wish.logo_left = float(body.get(u'logoLeft'))

        if body.get(u'privacy'):
            wish.privacy = body.get(u'privacy')
            if wish.privacy in ('invisible'):
                wish.friendlist = None
                # TODO we should never touch friends directly
                # and remove friends from favorited should also be automatic
                # remobe this after testing
                #wish.friends = []
                #wish.favorited = []
            if wish.privacy in ('public', 'fof', 'friends'):
                wish.friendlist = self.active_user.key().name()
                # TODO
                #wish.friends = wish.get_friends()
            elif wish.privacy in ('friendlist'):
                wish.friendlist = body.get('friendlist')
                # TODO
                #wish.friends = wish.get_friends()

        if body.get(u'status'):
            if (wish.status != body.get('status') and
                    body.get('status') == 'fulfilled' and
                    wish.privacy != 'invisible'):
                taskqueue.add(
                    url='/task/notify-favorite-wish-fulfill',
                    params={
                        'transmitter': wish.user_id,
                        'receivers': wish.favorited,
                        'wish_id': wish.key().id()})

                friends = set(wish.get_friends()).difference(wish.favorited)
                taskqueue.add(
                    url='/task/notify-friend-wish-fulfill',
                    params={
                        'transmitter': wish.user_id,
                        'receivers': friends,
                        'wish_id': wish.key().id()})

                taskqueue.add(
                    url='/task/notify-my-wish-fulfill',
                    params={
                        'receiver': wish.user_id,
                        'wish_id': wish.key().id()})
            wish.status = body.get(u'status')

        if wish.privacy == 'public' and body.get('account'):
            if body.get('account').get('id') not in wish.forbidden_tags:
                wish.account_tag = body.get('account').get('id')
                taskqueue.add(
                    url='/task/notify-tagged-wish',
                    params={
                        'transmitter': wish.user_id,
                        'receiver': wish.account_tag,
                        'wish_id': wish.key().id()})
        if body.get('account') == {}:
            wish.account_tag = None

        #if body.get(u'deleted_post'):
        #    post_id = body.get(u'deleted_post')
        #    if post_id in wish.posts:
        #        wish.deleted_posts.insert(0, post_id)
        #        wish.posts.remove(post_id)
        #        logging.info('Deleting post %s' % post_id)

        wish.updated = datetime.datetime.now()
        wish.put()

        # TODO this should also be in the on something?
        #if refresh_open_graph:
        #    wish.refresh_open_graph()

        if updated:
            refresh_open_graph(wish.url)
            taskqueue.add(
                url='/task/notify-favorite-wish-update',
                params={
                    'transmitter': wish.user_id,
                    'receivers': wish.favorited,
                    'wish_id': wish.key().id()})

        self.response.out.write(json.dumps({u'wish_id': wish.key().id(),
                                            u'url': wish.url}))
        return

    # New
    # Should never receive wish_id
    @user_required
    def post(self, wish_id=None):
        body = json.loads(self.request.body)

        if (body.get(u'logo') == conf.DEFAULT_IMAGE or
            body.get(u'logo') == conf.DEFAULT_THUMB):
            body[u'logo'] = ''

        wish = Wish(
            user_id = self.active_user.key().name(),
            user_type = self.active_user_type,
            user_name = self.active_user.name,
            user_picture = self.active_user.picture,
            title = body.get(u'title'),
            logo = body.get(u'logo'),
            desc = body.get(u'desc'),
            content = body.get(u'content'),
            status = body.get('status') or 'open')
        if hasattr(self.active_user, 'first_name'):
            wish.first_name = self.active_user.first_name,

        if body.get(u'logoTop') == 0 or body.get(u'logoTop'):
            wish.logo_top = float(body.get(u'logoTop'))
        if body.get(u'logoLeft') == 0 or body.get(u'logoLeft'):
            wish.logo_left = float(body.get(u'logoLeft'))

        if body.get(u'privacy'):
            wish.privacy = body.get(u'privacy')
            if wish.privacy in ('invisible'):
                wish.friendlist = None
            if wish.privacy in ('public', 'fof', 'friends'):
                wish.friendlist = self.active_user.key().name()
            elif body.get('privacy') in ('friendlist'):
                wish.friendlist = body.get('friendlist')

        if self.active_user.type == 'account':
            wish.account_tag = self.active_user.key().name()
        elif body.get('account'):
            wish.account_tag = body.get('account').get('id')

        wish.put()

        if self.active_user.has_wishes == False:
            self.active_user.has_wishes = True
            self.active_user.put()
            if self.active_user.type == 'account':
                memcache.delete('accounts')

        if self.active_user.type == 'user' and body.get('account'):
            taskqueue.add(
                url='/task/notify-tagged-wish',
                params={
                    'transmitter': wish.user_id,
                    'receiver': wish.account_tag,
                    'wish_id': wish.key().id()})

        if wish.status != 'editing' and wish.privacy != 'invisible':
            taskqueue.add(
                url='/task/notify-friend-wish-create',
                params={
                    'transmitter': wish.user_id,
                    'receivers': wish.get_friends(),
                    'wish_id': wish.key().id()})


        self.response.out.write(json.dumps({u'wish_id': wish.key().id(),
                                            u'url': wish.url}))
        return

    @user_required
    def delete(self, wish_id):
        if not wish_id:
            self.response.out.write(json.dumps(MSG_ERROR_SUP))
            return

        wish = Wish.get_by_id(int(wish_id))
        if not wish.can_edit(self.active_user):
            logging.error('User %s %s cannot edit this wish',
                    self.active_user.key().name(), self.active_user.name)
            self.response.out.write(json.dumps(MSG_ERROR_AUTH))
            return
        else:
            logging.debug('Deleting wish %s %s', wish_id, wish.title)
            wish.delete()
            self.response.out.write(json.dumps({u'wish_id': wish.key().id()}))
            return


class CommentStore(RestBaseHandler):
    @user_required
    def put(self, comment_id):
        # Check if comment exists
        logging.info('comment_id %s', comment_id)

        # TODO was getting Unsupported get request, removing all verifications
        # for now
        #try:
        #    response = facebook.api(
        #            comment_id,
        #            args={
        #                'access_token': self.active_user.access_token,
        #                },
        #            )
        #except GraphAPIError, exception:
        #    logging.warning(exception)
        #    # Comment doesn't exist or smth
        #    return

        #if (not response) or (not 'from' in response) or (not 'id' in response['from']):
        #    logging.warning('response: %s', response)
        #    return

        # TODO ID in comment_id from comment.create and in graph api
        # are different!!
        ## Get comments
        #try:
        #    access_token = facebook.get_app_access_token(
        #            conf.FACEBOOK_APP_ID,
        #            conf.FACEBOOK_APP_SECRET)
        #except httplib.HTTPException, e:
        #    logging.warning(e)
        #logging.info('comment_id %s', comment_id)
        #try:
        #    response = facebook.api(
        #            ('comments/%s' % comment_id),
        #            args={
        #                'ids': href,
        #                'access_token': self.active_user.access_token,
        #                },
        #            )
        #except GraphAPIError, exception:
        #    logging.warning(exception)
        #    return
        #logging.info(response)

        #body = json.loads(self.request.body)
        #href = body.get('href')
        #regex = re.compile(r'/wish/(?P<wish_id>\d*)(/step/(?P<step_num>\d*))?')
        #r = regex.search(href)
        #wish_step = r.groupdict()

        #wish_step['wish_id'] = int(wish_step['wish_id'])
        #wish = Wish.get_by_id(int(wish_step['wish_id']))
        #wish.updated = datetime.datetime.now()
        #wish.put()

        #step_id = None
        #if 'step_num' in wish_step and wish_step['step_num']:
        #    wish_step['step_num'] = int(wish_step['step_num'])
        #    step = (Step.all()
        #        .filter('wish_id =', wish_step['wish_id'])
        #        .filter('index =', wish_step['step_num']).get())
        #    if not step:
        #        logging.warning('Invalid step in CommentStore')
        #        return
        #    step.updated = datetime.datetime.now()
        #    step.put()
        #    step_id = step.key().id()

        body = json.loads(self.request.body)
        #user_id = body.get('user_id')
        try:
            wish_id = int(body.get('wish_id'))
        except (ValueError, TypeError):
            return
        wish = Wish.get_by_id(wish_id)
        if wish:
            wish.updated = datetime.datetime.now()
            wish.put()
        if not wish:
            return

        try:
            step_id =  int(body.get('step_id'))
            step = Step.get_by_id(wish_id)
            if step:
                step.updated = datetime.datetime.now()
                step.put()
        except (ValueError, TypeError):
            step_id = None

        logging.info('notify-my-wish-comment')
        taskqueue.add(
            url='/task/notify-my-wish-comment',
            params={'transmitter': self.active_user.key().name(),
                    'receiver': wish.user_id,
                    'wish_id': wish_id,
                    'step_id': step_id})

        logging.info('notify-favorite-wish-comment')
        taskqueue.add(
            url='/task/notify-favorite-wish-comment',
            params={'transmitter': self.active_user.key().name(),
                    'receivers': wish.favorited,
                    'wish_id': wish_id,
                    'step_id': step_id})

