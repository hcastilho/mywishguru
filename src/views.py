#!/usr/bin/env python
# coding: utf-8
"""
Views file
"""
import os
import logging
from operator import attrgetter #, itemgetter

from google.appengine.ext.webapp import template

from facebookhandler import BaseHandler
#from models import User
from models import Wish
from models import Step
#from models import FriendList
#from models import WishIndex
#from models import FavoriteIndex
from models import get_user_or_account
#from models import get_user
from models import get_user_and_search_parameter
from aux import user_required
import conf

class PageNotFoundHandler(BaseHandler):
    """Generall 404 page handler"""
    def get(self):
        self.error(404)

    def put(self):
        self.error(404)

class TosHandler(BaseHandler):
    def get(self):
        self.render(u'tos')

class IndexHandler(BaseHandler):
    """Access index page when the user is logged in"""
    def get(self):
        self.render(u'index')

class WishboardHandler(BaseHandler):
    """Browse my wishes"""
    @user_required
    def get(self):
        #self.render(u'wishboard')
        self.render(u'browse',
                browse_user = self.active_user,
                wishboard = True)

class BrowseHandler(BaseHandler):
    """Browse other users wishes"""
    def facebook_get(self, browse_user):
        # It's facebook requesting for page data
        template_path = os.path.join( os.path.dirname(__file__), u'templates', u'facebookexternalhit_browse.html')
        template_data = {
            'browse_user': browse_user,
            'request_url': self.request.url,
            u'site_name': conf.SITE_NAME,
            u'app_id': conf.FACEBOOK_APP_ID,
            u'app_namespace': conf.FACEBOOK_NAMESPACE,
            'conf': conf,
            }
        template_rendered = template.render(template_path, template_data)
        self.response.out.write(template_rendered)
        return

    def get(self, id_or_username=None):
        user = None
        if id_or_username:
            user, search_param = get_user_and_search_parameter(id_or_username)
            if not user:
                self.error(404)
                return
            if search_param == 'id' and user.username:
                self.redirect(user.url)
                return

        if self.request.headers['User-Agent'].startswith('facebookexternalhit'):
            self.facebook_get(browse_user = user)
            return

        if not user and not self.user:
            self.render('index')
            return

        self.render(u'browse', browse_user = user)


class WishHandler(BaseHandler):
    """Browse one wish"""
    def facebook_get(self, wish, step):
        # It's facebook requesting for page data
        if wish.privacy != 'invisible':
            template_path = os.path.join( os.path.dirname(__file__), u'templates', u'facebookexternalhit.html')
            template_data = {
                'selected_step': step,
                'wish_user': get_user_or_account(wish.user_id),
                u'site_name': conf.SITE_NAME,
                u'app_id': conf.FACEBOOK_APP_ID,
                u'app_namespace': conf.FACEBOOK_NAMESPACE,
                'conf': conf,
                }
            template_rendered = template.render(template_path, template_data)
            self.response.out.write(template_rendered)
            return
        else:
            self.error(404)
            return

    def get(self, wish_id=None, step_index=None):
        if not wish_id:
            # TODO is this realy necessary? returning an empty wish
            if self.active_user:
                context = {
                    u'wish': Wish.empty(self.active_user),
                    u'is_owner': True,
                    'steps': [],
                    }
            else:
                self.error(404)
                return
        else:
            try:
                wish_id = int(wish_id)
            except ValueError:
                self.error(404)
                return

            wish = Wish.get_by_id(wish_id)
            if not wish:
                self.error(404)
                return

            selected_step = wish.serialize(self.active_user)
            selected_index = 0
            if step_index:
                try:
                    step_index = int(step_index)
                except ValueError:
                    self.error(404)
                    return

                step = (Step.all().filter('wish_id =', wish_id)
                        .filter('index =', step_index).get())
                if step:
                    selected_step = step.serialize(self.active_user)
                    selected_index = step_index
                else:
                    self.error(404)
                    return


            if self.request.headers['User-Agent'].startswith('facebookexternalhit'):
                self.facebook_get(wish, selected_step)
                return

            if not wish.can_view(self.active_user):
                if self.active_user:
                    # Not authorized page
                    self.render(u'notauth', wish=wish)
                    return
                else:
                    # User is not logged in
                    self.render(
                        u'offline_wish',
                        wish_user_name = wish.user_first_name,
                        wish_privacy = wish.privacy,
                        )
                    return

            steps = Step.all().filter('wish_id =', wish_id)
            steps = sorted(
                steps,
                key=attrgetter('index'),
                )
            steps = [s.serialize(self.active_user) for s in steps]


            is_owner = False
            if self.account:
                if self.account.key().name() == wish.user_id:
                    is_owner = True
            else:
                if self.active_user and self.active_user.key().name() == wish.user_id:
                    is_owner = True

            context = {u'wish': wish.serialize(self.active_user),
                'wish_user': get_user_or_account(wish.user_id),
                u'is_owner': is_owner,
                'steps': steps,
                'selected_index': selected_index,
                'selected_step': selected_step,
                }
        self.render(u'wish', **context)

class WishCard(BaseHandler):
    def get(self, wish_id=None):
        if not wish_id:
            self.error(404)
            return

        try:
            wish_id = int(wish_id)
        except ValueError:
            self.error(404)
            return

        wish = Wish.get_by_id(wish_id)
        if not wish:
            self.error(404)
            return

        # It's facebook requesting for page data
        if self.request.headers['User-Agent'].startswith('facebookexternalhit'):
            template_path = os.path.join( os.path.dirname(__file__), u'templates', u'facebookexternalhit.html')
            template_data = {
                'wish': wish,
                'wish_user': get_user_or_account(wish.user_id),
                u'site_name': conf.SITE_NAME,
                u'app_id': conf.FACEBOOK_APP_ID,
                u'app_namespace': conf.FACEBOOK_NAMESPACE,
                'conf': conf,
                }
            template_rendered = template.render( template_path, template_data)
            self.response.out.write(template_rendered)
            return

        if not wish.can_view(self.active_user):
            if self.active_user:
                # Not authorized page
                self.render(u'notauth', wish=wish)
                return
            else:
                # User is not logged in
                self.render(
                    u'offline_wish',
                    wish_user_name = wish.user_first_name,
                    wish_privacy = wish.privacy,
                    )
                return

        context = {u'wish': wish.serialize(self.active_user),
                'wish_user': get_user_or_account(wish.user_id),
                }
        self.render(u'wishcard', **context)


class EditWishHandler(BaseHandler):
    """Edit one wish"""
    @user_required
    def get(self, wish_id=None):
        #self.redirect("wish/")
        wish = None
        if wish_id:
            if not wish_id:
                self.error(404)
                return

            try:
                wish_id = int(wish_id)
            except ValueError:
                self.error(404)
                return

            wish = Wish.get_by_id(wish_id)
            if not wish:
                self.error(404)
                return

            if not wish.can_edit(self.active_user):
                logging.debug('401')
                self.error(401)
                return

        if wish:
            wish = wish.serialize(self.active_user)
        else:
            wish = Wish.empty(self.active_user)

        steps = Step.all().filter('wish_id =', wish_id)
        steps = sorted(
            steps,
            key=attrgetter('index'),
            )
        steps = [step.serialize(self.active_user) for step in steps]
        context = {
                u'wish': wish,
                'wish_user': get_user_or_account(wish['user']['user_id']),
                'steps': steps,
                }
        self.render(u'newwish', **context)

