#import factory
#import time
#import logging
import unittest
#import base64
#from google.appengine.ext import db
from google.appengine.ext import testbed
#from google.appengine.api import taskqueue
from .. import models

def get_user_fixture(**kwargs):
    me = {
        'id': '1',
        'name': 'John Doe',
        'first_name': 'John',
        'picture': {'data': {'url': 'http://localhost:8080/user_picture'}},
        'email': 'JohnDoe@testmail.com',
        'friends': {'data': [{'id': '2'}]},
        }
    for key in kwargs:
        me[key] = kwargs[key]
    return me

#class WishFactory(factory.Factory):
def wish_factory(user, **kwargs):
    #FACTORY_FOR = models.Wish
    wish_data = {
        'user': user,
        'user_id': user.user_id,
        'user_name': user.name,
        'user_first_name': user.first_name,
        'user_picture': user.picture,
        'friendlist': user.user_id,
        'title': 'Test Wish',
        'desc': 'Test desc',
        'logo': 'http://localhost:8080/logo',
        'status': 'open',
        'privacy': 'fof',
        }
    for key in kwargs:
        wish_data[key] = kwargs[key]

    return models.Wish(**wish_data)

class TestCreateUser(unittest.TestCase):
    def setUp(self):
        self.testbed = testbed.Testbed()
        self.testbed.activate()
        self.testbed.init_datastore_v3_stub()

        self.me = get_user_fixture()
        self.access_token = {
                'access_token': 'abcdef',
                'expires': 674000,
                }
        models.User.create(self.me, self.access_token)

    def tearDown(self):
        self.testbed.deactivate()

    def test_user_created(self):
        user = models.User.get_by_key_name(self.me['id'])
        self.assertTrue(user)
        self.assertEqual(user.user_id, '1')

    def test_friendlist(self):
        friendlist = models.FriendList.get_by_key_name(self.me['id'])
        self.assertTrue(friendlist)
        self.assertEqual(friendlist.fb_friends, ['2'])
        self.assertEqual(friendlist.friends, [])

class TestBase(unittest.TestCase):
    def setUp(self):
        self.testbed = testbed.Testbed()
        self.testbed.activate()
        self.testbed.init_datastore_v3_stub()
        #self.testbed.init_taskqueue_stub()
        #self.taskq = self.testbed.get_stub(testbed.TASKQUEUE_SERVICE_NAME)

        self.access_token = {
                'access_token': 'abcdef',
                'expires': 674000,
                }

        #self.taskq.GetTasks('default')
        #tasks = self.taskq.GetTasks('default')
        #for task in tasks:
        #    print task.get('url')
        #self.taskq.FlushQueue('default')

    def tearDown(self):
        self.testbed.deactivate()

class TestFriendships(TestBase):
    def test_create_users(self):
        self.create_users()
    def create_users(self):
        self.create_user1()
        self.create_user2()
        self.create_user3()

    def create_user1(self):
        # First user has no fof friends
        me = get_user_fixture()
        user1 = models.User.create(me, self.access_token)
        friendlist1 = models.FriendList.get_by_key_name('1')
        self.assertTrue('2' in friendlist1.fb_friends)
        self.assertFalse('3' in friendlist1.fb_friends)
        self.assertFalse('2' in friendlist1.friends)
        self.assertFalse('3' in friendlist1.friends)
        return user1

    def create_user2(self):
        # When the second user is created
        # it should be included in user1.friends
        me = get_user_fixture(id='2',
            friends = {'data': [{'id': '1'}, {'id': '3'}]},
            )
        user2 = models.User.create(me, self.access_token)
        friendlist2 = models.FriendList.get_by_key_name('2')
        self.assertTrue('1' in friendlist2.fb_friends)
        self.assertTrue('3' in friendlist2.fb_friends)
        self.assertTrue('1' in friendlist2.friends)
        self.assertFalse('3' in friendlist2.friends)
        # Test if user1 is updated
        friendlist1 = models.FriendList.get_by_key_name('1')
        self.assertTrue('2' in friendlist1.friends)
        self.assertFalse('3' in friendlist1.friends)
        return user2

    def create_user3(self):
        me = get_user_fixture(id='3',
            friends = {'data': [{'id': '2'}]},
            )
        user3 = models.User.create(me, self.access_token)
        friendlist3 = models.FriendList.get_by_key_name('3')
        self.assertFalse('1' in friendlist3.fb_friends)
        self.assertTrue('2' in friendlist3.fb_friends)
        self.assertFalse('1' in friendlist3.friends)
        self.assertTrue('2' in friendlist3.friends)
        friendlist2 = models.FriendList.get_by_key_name('2')
        self.assertTrue('1' in friendlist2.friends)
        self.assertTrue('3' in friendlist2.friends)
        friendlist1 = models.FriendList.get_by_key_name('1')
        self.assertTrue('2' in friendlist1.friends)
        self.assertFalse('3' in friendlist1.friends)
        return user3


    def test_add_friend(self):
        self.create_users()
        self.add_friend('1', '3')

    def add_friend(self, user1, user2):
        # Facebook realtime updates each one independently
        # add 2 as friend of 1
        friendlist1 = models.FriendList.get_by_key_name(user1)
        self.assertFalse(user2 in friendlist1.fb_friends)
        self.assertFalse(user2 in friendlist1.friends)
        curfriends = list(friendlist1.fb_friends)
        curfriends.append(user2)
        friendlist1.fb_friends = curfriends
        friendlist1.put()
        friendlist1 = models.FriendList.get_by_key_name(user1)
        self.assertTrue(user2 in friendlist1.fb_friends)
        self.assertTrue(user2 in friendlist1.friends)

        # add 1 as friend of 2
        friendlist2 = models.FriendList.get_by_key_name(user2)
        self.assertFalse(user1 in friendlist2.fb_friends)
        self.assertFalse(user1 in friendlist2.friends)
        curfriends = list(friendlist2.fb_friends)
        curfriends.append(user1)
        friendlist2.fb_friends = curfriends
        friendlist2.put()
        friendlist2 = models.FriendList.get_by_key_name(user2)
        self.assertTrue(user1 in friendlist2.fb_friends)
        self.assertTrue(user1 in friendlist2.friends)
    # TODO test delete users/friends

    #def test_trans(self):
    #    friendlist1 = models.FriendList.get_by_key_name('1')
    #    # Just to check the changes we did before were contained
    #    # It checked out
    #    self.assertFalse('3' in friendlist1.friends)

    def test_add_wish(self):
        self.create_users()
        user1 = models.User.get_by_key_name('1')
        wish_id = self.add_wish(user1)
        self.check_wish('2', wish_id)
        self.check_no_wish('3', wish_id)
        self.add_friend('1', '3')
        self.check_wish('3', wish_id)

    def add_wish(self, user):
        #user1 = models.User.get_by_key_name('1')
        wish = wish_factory(user)
        wish.put()
        return wish.key().id()

    def check_wish(self, user_id, wish_id):
        wish = models.Wish.get_by_id(wish_id)
        self.assertTrue(user_id in wish.friends)

    def check_no_wish(self, user_id, wish_id):
        wish = models.Wish.get_by_id(wish_id)
        self.assertFalse(user_id in wish.friends)

    def test_add_wish_user_created(self):
        user1 = self.create_user1()
        wish_id = self.add_wish(user1)
        self.create_user2()
        self.check_wish('2', wish_id)
        self.check_no_wish('3', wish_id)
