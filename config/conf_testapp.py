#!/usr/bin/env python
# coding: utf-8

from private import *

DEBUG = True

SITE_NAME=u'FuzzyMonkey'

PAGE_SIZE=8

FACEBOOK_NAMESPACE = 'testapp-fuzzymonkey'
FACEBOOK_APP_ID = "133708810062561"
FACEBOOK_CANVAS_NAME = 'testapp-fuzzymonkey'
FACEBOOK_REDIRECT_URI = u''

EXTERNAL_HREF = 'http://testapp-fuzzymonkey.appspot.com/'
HOSTNAME = 'testapp-fuzzymonkey.appspot.com'

DEFAULT_IMAGE = EXTERNAL_HREF + 'images/default.png'
DEFAULT_THUMB = EXTERNAL_HREF + 'images/thumb_lamp.png'
DEFAULT_COVER = EXTERNAL_HREF + 'images/genie_head_700x700.png'

