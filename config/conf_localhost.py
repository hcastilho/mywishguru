#!/usr/bin/env python
# coding: utf-8

from private import *

DEBUG = True

SITE_NAME=u'Localhost'

PAGE_SIZE=8

FACEBOOK_NAMESPACE = 'localhost'
FACEBOOK_APP_ID = "261983243864633"
FACEBOOK_CANVAS_NAME = 'testapp-fuzzymonkey'
FACEBOOK_REDIRECT_URI = u''

EXTERNAL_HREF = 'http://localhost:8080/'
HOSTNAME = 'localhost'

DEFAULT_IMAGE = EXTERNAL_HREF + 'images/default.png'
DEFAULT_THUMB = EXTERNAL_HREF + 'images/thumb_lamp.png'
DEFAULT_COVER = EXTERNAL_HREF + 'images/genie_head_700x700.png'

