#!/usr/bin/env python
# coding: utf-8

from private import *

DEBUG = False

SITE_NAME=u'Mywishguru'

PAGE_SIZE=16

FACEBOOK_NAMESPACE = 'mywishguru'
FACEBOOK_APP_ID = '387714221299242'
FACEBOOK_CANVAS_NAME = ''
FACEBOOK_REDIRECT_URI = u''

EXTERNAL_HREF = 'http://www.mywishguru.com/'
HOSTNAME = 'www.mywishguru.com'

DEFAULT_IMAGE = EXTERNAL_HREF + 'images/default.png'
DEFAULT_THUMB = EXTERNAL_HREF + 'images/thumb_lamp.png'
DEFAULT_COVER = EXTERNAL_HREF + 'images/genie_head_700x700.png'

