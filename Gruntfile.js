path = require('path');

module.exports = function(grunt) {

  // CHANGE JS VERSION
  // src/static/src/mwg/package.json

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('src/static/js/mwg/package.json'),

    copy: {
      dev_src: {
        files: [
          {
            expand: true,
            flatten: true,
            src: ['src/*'],
            dest: 'build/dev/',
            filter: 'isFile'
          },
        ]
      },
      dev_templates: {
        files: [
          {
            expand: true,
            flatten: true,
            src: ['src/templates/*'],
            dest: 'build/dev/templates/',
            filter: 'isFile'
          },
        ]
      },
      dist_src: {
        files: [
          {
            expand: true,
            flatten: true,
            src: ['src/*'],
            dest: 'build/dist/',
            filter: 'isFile'
          },
        ]
      },
      dist_templates: {
        files: [
          {
            expand: true,
            flatten: true,
            src: ['src/templates/*'],
            dest: 'build/dist/templates/',
            filter: 'isFile'
          },
        ]
      },
      dist_masonry: {
        files: [
          {
            src: ['src/static/js/jquery.masonry.min.js'],
            dest: 'build/dist/static/js/',
          },
        ]
      }
    },

    preprocess: {
      dev: {
        files: [
          {src: 'src/templates/skeleton.html', dest: 'build/dev/templates/skeleton.html'}
        ],
        options: {
          inline: true,
          context: {
            RELEASE_TAG: '<%= pkg.version %>',
            NODE_ENV: 'local'
          }
        }
      },
      dist: {
        files: [
          {src: 'src/templates/skeleton.html', dest: 'build/dist/templates/skeleton.html'}
        ],
        options: {
          inline: true,
          context: {
            RELEASE_TAG: '<%= pkg.version %>',
            NODE_ENV: 'dist'
          }
        }
      }
    },

    watch: {
      files: ['src/static/sass/**/*.scss',
              'Gruntfile.js',
              'src/static/js/mwg/**/*.js',
              'src/**/*.py'],
      tasks: ['build:dev']
    },

    jshint: {
      all: ['Gruntfile.js',
            'src/static/js/mwg/**/*.js',
            'src/static/js/profiles/**/*.js']
    },

    compass: {
      dist: {
        options: {
          sassDir: 'src/static/sass',
          cssDir: 'build/dist/static/css',
          environment: 'production'
        }
      },
      dev: {
        options: {
          sassDir: 'src/static/sass',
          cssDir: 'build/dev/static/css'
        }
      }
    },

    dojo: {
      dist: {
        options: {
          dojo: 'src/static/js/dojo/dojo.js',
          load: 'build',
          profile: 'src/static/js/mwg.profile.js'
        }
      }
    }

  });


  grunt.registerTask('pyflakes', function() {
    var done = this.async();
    grunt.util.spawn({
      cmd: 'pyflakes',
      args: ['src/'],
      opts: {'stdio': 'inherit'}
      }, function () {
        done();
      });
  });


  // TODO clear css directory
  //grunt.registerTask('clearBuildDir', 'Clean the whole build directory.', function(env) {
  //  var done = this.async();
  //  if (env === 'dev') {
  //    grunt.util.spawn({
  //      cmd: 'rm',
  //      args: ['-rdf', 'src/static/sass/dev'],
  //      opts: {'stdio': 'inherit'}
  //    },
  //    function() {
  //        done();
  //    });
  //  }
  //  else {
  //    grunt.util.spawn({
  //      cmd: 'rm',
  //      args: ['-rdf', 'src/static/js/dist', 'src/static/sass/dist'],
  //      opts: {'stdio': 'inherit'}
  //    },
  //    function() {
  //        done();
  //    });
  //  }
  //});


  grunt.registerTask('setAppengineConf', 'Copy configs.', function(conf, env) {
      if (conf === 'testapp') {
        grunt.file.copy('config/app_testapp.yaml', 'build/' + env + '/app.yaml');
        grunt.file.copy('config/conf_testapp.py', 'build/' + env + '/conf.py');
        grunt.file.copy('config/private_testapp.py', 'build/' + env + '/private.py');
      }
      else if (conf === 'localhost') {
        grunt.file.copy('config/app_localhost.yaml', 'build/' + env + '/app.yaml');
        grunt.file.copy('config/conf_localhost.py', 'build/' + env + '/conf.py');
        grunt.file.copy('config/private_localhost.py', 'build/' + env + '/private.py');
      }
      else {
        grunt.file.copy('config/app_mywishguru.yaml', 'build/' + env + '/app.yaml');
        grunt.file.copy('config/conf_mywishguru.py', 'build/' + env + '/conf.py');
        grunt.file.copy('config/private_mywishguru.py', 'build/' + env + '/private.py');
      }
  });


  grunt.registerTask('appengineUpdate', 'Upload to App Engine.', function(env) {
      grunt.log.subhead('Uploading to App Engine');

      var done = this.async();
      grunt.util.spawn({
        cmd: 'appcfg.py',
        args: ['update', 'build/' + env],
        opts: {'stdio': 'inherit'}
      },
      function() {
          grunt.log.success('Done');
          done();
      });
  });

  grunt.registerTask('run', 'Run app server.', function(env) {
      grunt.log.subhead('Starting App Server');

      var done = this.async();
      grunt.util.spawn({
        cmd: 'dev_appserver.py',
        //cmd: 'devappserver2.py',
        args: ['build/' + env],
        opts: {'stdio': 'inherit'}
      },
      function() {
        grunt.log.writeln('Done');
        done();
      });

  });

  //grunt.registerTask('vaccuumIndexes', function() {
  //    grunt.log.subhead('Uploading to App Engine');

  //    var done = this.async();
  //    grunt.util.spawn({
  //      cmd: 'appcfg.py',
  //      args: ['vacuum_indexes', 'src/'],
  //      opts: {'stdio': 'inherit'}
  //    },
  //    function() {
  //        grunt.log.success('Done');
  //        done();
  //    });
  //});

  grunt.loadNpmTasks(path.relative(
       path.resolve(__dirname, 'node_modules'),
       path.resolve(process.env.NODE_PATH, 'grunt-contrib-watch')));
  grunt.loadNpmTasks(path.relative(
       path.resolve(__dirname, 'node_modules'),
       path.resolve(process.env.NODE_PATH, 'grunt-contrib-compass')));
  grunt.loadNpmTasks(path.relative(
       path.resolve(__dirname, 'node_modules'),
       path.resolve(process.env.NODE_PATH, 'grunt-contrib-jshint')));
  grunt.loadNpmTasks(path.relative(
       path.resolve(__dirname, 'node_modules'),
       path.resolve(process.env.NODE_PATH, 'grunt-contrib-copy')));
  grunt.loadNpmTasks(path.relative(
       path.resolve(__dirname, 'node_modules'),
       path.resolve(process.env.NODE_PATH, 'grunt-preprocess')));
  grunt.loadNpmTasks(path.relative(
       path.resolve(__dirname, 'node_modules'),
       path.resolve(process.env.NODE_PATH, 'grunt-dojo')));
  //grunt.loadNpmTasks('grunt-contrib-watch');
  //grunt.loadNpmTasks('grunt-contrib-compass');
  //grunt.loadNpmTasks('grunt-contrib-jshint');
  //grunt.loadNpmTasks('grunt-preprocess');
  //grunt.loadNpmTasks('grunt-dojo');

  grunt.registerTask('lint', ['jshint', 'pyflakes']);

  grunt.registerTask('build', function(env) {
    if (env === 'dev') {
      grunt.task.run([
        'compass:dev',
        'copy:dev_src',
        'copy:dev_templates',
        'preprocess:dev',
        'lint']);
    }
    else if (env === 'dist') {
      grunt.task.run([
        'compass:dist',
        'dojo:dist',
        'clearUncompressed',
        'copy:dist_src',
        'copy:dist_templates',
        'copy:dist_masonry',
        'preprocess:dist']);
    }
  });

  grunt.registerTask('runLocal', [
      'setAppengineConf:localhost:dev',
      'run:dev']);
  grunt.registerTask('runLocalDist', [
      'setAppengineConf:localhost:dist',
      'setConf:localhost:dist',
      'run:dist']);
  grunt.registerTask('updateTestappDev', [
      'setAppengineConf:testapp:dev',
      'appengineUpdate:dev']);
  grunt.registerTask('updateTestapp', [
      'setAppengineConf:testapp:dist',
      'appengineUpdate:dist']);
  grunt.registerTask('updateMywishguru', [
      'setAppengineConf:mywishguru:dist',
      'appengineUpdate:dist']);

  grunt.registerTask('clearUncompressed', 'Clean the whole build directory.', function() {
      var done = this.async();
      grunt.util.spawn({
        cmd: 'find',
        args: ['build/dist/static/js',
          '-name', '*.uncompressed.js',
          '-type', 'f',
          '-delete'],
        opts: {'stdio': 'inherit'}
      },
      function() {
        grunt.util.spawn({
          cmd: 'find',
          args: ['build/dist/static/js',
            '-name', '*.consoleStripped.js',
            '-type', 'f',
            '-delete'],
          opts: {'stdio': 'inherit'}
        },
        function() {
          grunt.util.spawn({
            cmd: 'find',
            args: ['build/dist/static/js',
              '-name', '*.less',
              '-type', 'f',
              '-delete'],
            opts: {'stdio': 'inherit'}
          },
          function() {
            grunt.util.spawn({
              cmd: 'find',
              args: ['build/dist/static/js',
                '-name', 'README',
                '-type', 'f',
                '-delete'],
              opts: {'stdio': 'inherit'}
            },
            function() {
              grunt.util.spawn({
                cmd: 'find',
                args: ['build/dist/static/js',
                  '-name', 'LICENSE',
                  '-type', 'f',
                  '-delete'],
                opts: {'stdio': 'inherit'}
              },
              function() {
                grunt.util.spawn({
                  cmd: 'find',
                  args: ['build/dist/static/js',
                    '-name', '*.js.map',
                    '-type', 'f',
                    '-delete'],
                  opts: {'stdio': 'inherit'}
                },
                function() {
                  done();
                });
              });
            });
          });
        });
      });
  });
};



